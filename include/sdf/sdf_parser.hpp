#pragma once

#include "sdf_node.hpp"

#include "util/logger.hpp"

#include <vector>
#include <string>
#include <regex>

namespace sdf
{

/**
 * @brief A vector of SDF_Nodes.
 */
using SDF_Branch = std::vector<SDF_Node>;

/**
 * @brief A parsing class capable of reading from or writing to SDF files. 
 */
class SDF_Parser
{
public:
    /**
     * @brief Standard, default constructor. Generates log and prepares for operations.
     */
    SDF_Parser();

    /**
     * @brief Saves to the provided filename, return true on success.
     */
    auto load_file(std::string filename) -> bool;

    /**
     * @brief Loads from the provided filename, return true on success.
     */
    auto save_file(std::string filename) -> bool;

    /**
     * @brief Sets the internal table for the parser to that SDF_Branch provided.
     */
    auto set_table(const SDF_Branch table) -> void;

    /**
     * @brief Returns a copy of the parser's internal table. 
     */
    auto table() const -> SDF_Branch;

    /**
     * @brief Pushes the branch to the back the parser's top-level internal Branch.
     */
    auto operator << (const SDF_Branch table) -> SDF_Parser&; 

private:
    /**
     * @brief Given the input, construct a branch of all valid SDF_Nodes.
     * 
     * @note This function will recurse with construct_node.
     * @see construct_node 
     */
    auto construct_tree(std::string input) -> SDF_Branch;

    /**
     * @brief Givesn the name, tag, and value, create an appropriate SDF_Node.
     * 
     * @note This function will recurse with construct_tree.
     * @see construct_tree
     */
    auto construct_node(std::string name, std::string tag, std::string value) -> SDF_Node;

    /**
     * @brief Generates string output for the given node, at the provided node depth.
     *
     * @param depth Base-1 depth of node graph. This tabulates our output appropriately. 
     */
    auto construct_node_output(SDF_Node node, int depth = 1) -> std::string;

    SDF_Branch _tree; ///< List of nodes at the top-level of the file.

    std::string _log_file = std::string("parser.log"); ///< Relative filepath for event log.
    util::Logger _log;
};

}
