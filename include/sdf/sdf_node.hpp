#pragma once

#include "util/maybe.hpp"

#include <string>
#include <vector>
#include <regex>

namespace sdf
{

class SDF_Node; // Forward Declaration

using SDF_Branch = std::vector<SDF_Node>; // See sdf_parser.hpp for documentation.

/**
 * @brief The type of our Node instance.
 */
enum class SDF_Node_Type {
    Value,
    Branch
};

/**
 * @brief
 */
class SDF_Node
{
public:
    /**
     * @brief Construct a node from the given name and string value.
     */
    SDF_Node(std::string name, std::string value);

    /**
     * @brief Construct a node from the given name and string value, along with a string tag.
     */
    SDF_Node(std::string name, std::string tag, std::string value);

    /**
     * @brief Construct a node from the given name and ndoe branch.
     */
    SDF_Node(std::string name, SDF_Branch branch);

    /**
     * @brief Construct a node from the given name and node branch, along with a string tag.
     */
    SDF_Node(std::string name, std::string tag, SDF_Branch branch);

    /**
     * @brief Return the name of our node.
     */
    auto name() const -> std::string;

    /**
     * @brief Return the tags for our node, or an empty string.
     */
    auto tag() const -> std::string;

    /**
     * @brief Set the tags for our node.
     */
    auto tag(std::string tag) -> void;

    /**
     * @brief Returns true if this node has a tag (not an empty string).
     */
    auto has_tag() const -> bool;

    /**
     * @brief Returns what node type this node is.
     */
    auto kind() const -> SDF_Node_Type;

    /**
     * @brief Returns a Maybe type containing our value string, or nothing if this is a branch.
     */
    auto value() const -> util::Maybe<std::string>;

    /**
     * @brief Returns a Maybe type containing our value string, or nothing if this is a branch.
     */
    auto as_string() const -> util::Maybe<std::string>;

    /**
     * @brief Returns a Maybe type containing our value as an int, or nothing if this is a branch.
     */
    auto as_int() const -> util::Maybe<int>;

    /**
     * @brief Returns a Maybe type containing our value as a float, or nothing if this is a branch.
     */
    auto as_float() const -> util::Maybe<float>;

    /**
     * @brief Returns a Maybe type containing our value as a bool, or nothing if this is a branch.
     */
    auto as_bool() const -> util::Maybe<bool>;

    /**
     * @brief Returns a Maybe type containing the node's branch, or nothing if this is a value node.
     */
    auto branch() const -> util::Maybe<SDF_Branch>;

    /**
     * @brief Returns true if this node contains a subnode with the given name.
     */
    auto contains(std::string node_name) const -> bool;

    /**
     * @brief Returns the number of subnodes with the given name.
     */
    auto count(std::string node_name) const -> int; 

private:
    std::string _name;  ///< Name of this node. 
    std::string _tag;   ///< Any node tags or an empty string.
    SDF_Node_Type _kind;///< What node type is this?
    std::string _value; ///< The value of the node, or an empty string.
    SDF_Branch _branch; ///< The node branch value, or an empty branch.
};

}
