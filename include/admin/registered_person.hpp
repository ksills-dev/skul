#pragma once

#include "id.hpp"
#include "person.hpp"

#include "sdf/sdf_node.hpp"
#include "util/maybe.hpp"

namespace admin
{

class Admin_DB;   // Forward declare for cyclic dependency
class Department; // Forward declare for cyclic dependency

/**
 * @brief A Person that has been registered at a university.
 */
class Registered_Person : public Person
{
friend Department;
public:
    Registered_Person() : _adminsys(nullptr)  {};

    /**
     * @brief Constructs a Registered person from the given parameters.
     */
    Registered_Person(Admin_DB* const adminsys, Person base, UID id, 
                      DID department, bool active = true)
        : Person(base), _adminsys(adminsys), _id(id), _department(department), _active(active) {};

    virtual ~Registered_Person() = default;

    /**
     * @brief Returns a Maybe type with a Registered Person from the SDF_Branch, or empty on failure.
     */
    static auto from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Registered_Person>;

    /**
     * @brief Returns an SDF_Node made from the given registered person.
     */
    static auto to_sdf(const Registered_Person& person) -> sdf::SDF_Node;

    /**
     * @brief Returns the UID for this person.
     */
    auto id() const -> UID;

    /**
     * @brief Returns true if this person is actively enrolled.
     */
    auto active() const -> bool;
    
    /**
     * @brief Returns the DID for the enrolled department.
     */
    auto department() const -> DID;

    /**
     * @brief Re-activates the enrollment for the given person.
     */
    virtual auto activate() -> void;

    /**
     * @brief Deactivates the enrollment for the given person.
     */
    virtual auto deactivate() -> void;

protected:
    /**
     * @brief Changes the department of our person. Restricted access.
     */
    auto change_department(DID department) -> void;

    Admin_DB* const _adminsys; ///< Backreference to the database.
    
    UID _id;         ///< Person's assigned UID     
    DID _department; ///< Department's DID
    bool _active;    ///< Is this person actively enrolled?
};

}
