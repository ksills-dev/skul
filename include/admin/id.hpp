#pragma once

#include "util/unique_id_factory.hpp"

#include <regex>
#include <string>
#include <vector>

namespace admin 
{

struct DID_Tag {}; ///< Ignore these tag types. 
struct UID_Tag {}; ///< Ignore these tag types. 
struct RID_Tag {}; ///< Ignore these tag types. 
struct CID_Tag {}; ///< Ignore these tag types. 
struct SID_Tag {}; ///< Ignore these tag types. 
struct AID_Tag {}; ///< Ignore these tag types. 

using DID = util::ID_Base<DID_Tag>; ///< Unique ID for departments. 
using UID = util::ID_Base<UID_Tag>; ///< Unique ID for registered persons.
using RID = util::ID_Base<RID_Tag>; ///< Unique ID for research projects.
using CID = util::ID_Base<CID_Tag>; ///< Unique ID for course definitions. 
using SID = util::ID_Base<SID_Tag>; ///< Unique ID for course sections. 
using AID = util::ID_Base<AID_Tag>; ///< Unique ID for assignments. 

using DID_Factory = util::Unique_ID_Factory<DID, DID_Tag>; ///< A factory to generate DIDs
using UID_Factory = util::Unique_ID_Factory<UID, UID_Tag>; ///< A factory to generate UIDs
using RID_Factory = util::Unique_ID_Factory<RID, RID_Tag>; ///< A factory to generate RIDs
using CID_Factory = util::Unique_ID_Factory<CID, CID_Tag>; ///< A factory to generate CIDs
using SID_Factory = util::Unique_ID_Factory<SID, SID_Tag>; ///< A factory to generate SIDs
using AID_Factory = util::Unique_ID_Factory<AID, AID_Tag>; ///< A factory to generate AIDs

const std::regex id_regex = std::regex("(\\d+)[, ]?"); ///< A regex to parse IDs. 

/**
 * @brief Generates a vectors of the given ID type from the string of comma or space delimited ids.
 */
template <typename ID>
auto parse_id_list(std::string from) -> std::vector<ID> 
{
    if (from == "") {
        return std::vector<ID>();
    }

    std::vector<ID> result;
    std::smatch match;
    while (std::regex_search(from, match, id_regex)) {
        result.push_back(ID(std::stoul(match.str(1))));
        from = match.suffix();
    }
    return result;
}

/**
 * @brief Writes a vector of IDs to a string, comma delimited.
 */
template <typename ID>
auto write_id_list(std::vector<ID> list) -> std::string
{
    std::string output = "";
    for (const auto& id : list) {
        output += std::to_string(id.raw());
        output += ",";
    }
    return output;
}

}