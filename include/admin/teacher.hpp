#pragma once

#include "id.hpp"
#include "registered_person.hpp"

#include "sdf/sdf_node.hpp"

#include <vector>

namespace admin
{

/**
 * @brief The kind of teacher employment. Lecturers may not conduct research.
 */
enum class Teacher_Kind {
    Lecturer,
    Adjunct,
    Professor
};

class Course_Section;   // Forward declares for cyclic dependencies
class Research_Project; // Forward declares for cyclic dependencies

/**
 * @brief A teacher at a university.
 */
class Teacher : public Registered_Person
{
friend Course_Section;
friend Research_Project;

public:
    Teacher() = default;

    /**
     * @brief Base constructor. Constructs an empty teacher from the given parameters.
     */
    Teacher(Registered_Person person, Teacher_Kind employment_kind);

    /**
     * @brief Full constructor. Constructs a teacher from the given parameters.
     */
    Teacher(Registered_Person person, Teacher_Kind employment_kind,
            std::vector<SID> current_courses, std::vector<SID> completed_courses,
            std::vector<RID> current_research, std::vector<RID> completed_research);

    virtual ~Teacher() = default;

    /**
     * @brief Returns a Maybe type with a Teacher from the SDF_Branch, or empty on failure.
     */
    static auto from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Teacher>;

    /**
     * @brief Returns an SDF_Node made from the given course teacher.
     */
    static auto to_sdf(const Teacher& teacher) -> sdf::SDF_Node;

    /**
     * @brief Returns current employment kind.
     *
     * @see Teacher_Kind
     */
    auto kind() const -> Teacher_Kind;
    
    /**
     * @brief Changes the teacher's employment kind.
     */
    auto change_kind(Teacher_Kind employment_kind) -> void;

    /**
     * @brief Attempt to assign self to the section. Return true on success.
     */
    auto add_course(SID section) -> bool;

    /**
     * @brief Remove self from the course section.
     */
    auto quit_course(SID section) -> void;

    /**
     * @brief Finalize the section, if we have permission to do so.
     */
    auto finalize_course(SID section) -> void;
    
    /**
     * @brief Attempt to assign self to the project. Return true on success.
     */
    auto add_research(RID project) -> bool;

    /**
     * @brief Remove self from the research project.
     */
    auto quit_research(RID project) -> void;

    /**
     * @brief Finalize the research project as complete, if we have permission to do so.
     */
    auto finalize_research(RID project) -> void;

    /**
     * @brief Returns true if the teacher can research.
     */
    auto can_research() const -> bool;

    /**
     * @brief Returns true if the teacher can research this section.
     */
    auto can_research(RID research) const -> bool;

    /**
     * @brief Returns a vector of currently taught course SIDs.
     */
    auto current_courses() const -> std::vector<SID>;

    /**
     * @brief Returns a vector of previously taught course SIDs.
     */
    auto completed_courses() const -> std::vector<SID>;

    /**
     * @brief Returns a vector of currently assigned research RIDs.
     */
    auto current_research() const -> std::vector<RID>;

    /**
     * @brief Returns a vector of previously assigned research RIDs.
     */
    auto completed_research() const -> std::vector<RID>;

    /**
     * @brief Returns true if currently teaching the section.
     */
    auto is_teaching(SID section) const -> bool;

    /**
     * @brief Returns true if currently teaching sections of the definition.
     */
    auto is_teaching(CID course) const -> bool;

    /**
     * @brief Returns true if previously taught the section.
     */
    auto has_taught(SID section) const -> bool;

    /**
     * @brief Returns true if previously taught sections of the definition.
     */
    auto has_taught(CID course) const -> bool;

    /**
     * @brief Returns true if currently working on the project.
     */
    auto is_researching(RID project) const -> bool;

    /**
     * @brief Returns true if previously worked on the project.
     */
    auto has_researched(RID project) const -> bool;

    /**
     * @brief Deactivates the teacher. Withdrawing from current assignment and preventing future ones.
     */
    virtual auto deactivate() -> void override final;

protected:
    /**
     * @brief Adds section to records. Restricted Access. 
     */
    auto assign_course(SID section) -> void;

    /**
     * @brief Removes section from records. Restricted Access.
     */
    auto remove_course(SID section) -> void;

    /**
     * @brief Adds research to records. Restricted Access.
     */
    auto assign_research(RID project) -> void;

    /**
     * @brief Remove research from records. Restricted Access.
     */
    auto remove_research(RID project) -> void;

    /**
     * @brief Archive course to completed courses. Restricted Access.
     */
    auto archive_course(SID section) -> void;

    /**
     * @brief Archives research to completed research. Restricted Access.
     */
    auto archive_research(RID project) -> void;

private:
    Teacher_Kind _kind;                  ///< Current employment kind for teacher.
    std::vector<SID> _current_courses;   ///< Vector of current course CIDs
    std::vector<SID> _completed_courses; ///< Vector of completed course CIDs
    std::vector<RID> _current_research;  ///< Vector of current research RIDs
    std::vector<RID> _completed_research;///< Vector of completed research RIDs
};

}
 