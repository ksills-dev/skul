#pragma once

#include "id.hpp"

#include "sdf/sdf_node.hpp"
#include "util/maybe.hpp"

#include <string>
#include <vector>

namespace admin
{

class Admin_DB; // Forward declare for cyclic dependency.

/**
 * @brief A specific Department in a university.
 */
class Department
{
public:
    Department() : _adminsys(nullptr) {};

    /**
     * @brief Base constructor. Constructs an empty department from the given parameters.
     */
    Department(Admin_DB* const adminsys, DID id, std::string name, std::string prefix);

    /**
     * @brief Full constructor. Constructs a department from the given parameters.
     */
    Department(Admin_DB* const adminsys, DID id, std::string name, std::string prefix,
               std::vector<UID> students, std::vector<UID> teachers,
               std::vector<CID> courses, std::vector<RID> research, bool archived);

    /**
     * @brief Returns a Maybe type with a Department from the SDF_Branch, or empty on failure.
     */
    static auto from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Department>;

    /**
     * @brief Returns an SDF_Node made from the given department.
     */
    static auto to_sdf(const Department& department) -> sdf::SDF_Node;

    /**
     * @brief Returns the DID for this department.
     */
    auto id() const -> DID;

    /**
     * @brief Returns the name for this department.
     */
    auto name() const -> std::string;

    /**
     * @brief Returns the course prefix for this department.
     */
    auto prefix() const -> std::string;

    /**
     * @brief Returns true if this department is archived.
     */
    auto archived() const -> bool;

    /**
     * @brief Archives the department, preventing new enrollements or course/project assignment.
     */
    auto archive() -> void;

    /**
     * @brief Returns a vector of teacher UIDs.
     */
    auto teachers() const -> std::vector<UID>;

    /**
     * @brief Returns a vector of student UIDs.
     */
    auto students() const -> std::vector<UID>;

    /**
     * @brief Returns a vector of course definition CIDs.
     */
    auto courses() const -> std::vector<CID>;

    /**
     * @brief Returns a vector of research project RIDs.
     */
    auto research() const -> std::vector<RID>;

    /**
     * @brief Attempts to enroll the given student to the department. Returns true on success.
     */
    auto enroll_student(UID student) -> bool;

    /**
     * @brief Removes the student with the given UID from the department and assigns them to the new department.
     *
     * @param new_department New department to assign to (as everyone must be assigned to 
     * a department at any time). Defaults to the general undergraduate department = 0. 
     * @return Returns true on success.
     */
    auto remove_student(UID student, DID new_department = DID(0)) -> bool;

    /**
     * @brief Attempts to assign the teacher to the department. Returns true on success.
     */
    auto assign_teacher(UID teacher) -> bool;

    /**
     * @brief Removes the teacher with the given UID from the department and assigns them to the new department.
     *
     * @param new_department New department to assign to (as everyone must be assigned to 
     * a department at any time). Defaults to the general undergraduate department = 0. 
     * @return Returns true on success.
     */
    auto remove_teacher(UID teacher, DID new_department = DID(0)) -> bool;

    /**
     * @brief Attempts to assign the course to the department. Returns true on success.
     */
    auto add_course(CID course) -> bool;

    /**
     * @brief Removes the course with the given UID from the department and assigns it to the new department.
     *
     * @param new_department New department to assign to (as courses must be assigned to 
     * a department at any time). Defaults to the general undergraduate department = 0. 
     * @return Returns true on success.
     */
    auto remove_course(CID course, DID new_department = DID(0)) -> bool;

    /**
     * @brief Attempts to assign the research to the department. Returns true on success.
     */
    auto add_research(RID project) -> bool;

    /**
     * @brief Removes the research with the given UID from the department and assigns it to the new department.
     *
     * @param new_department New department to assign to (as research must be assigned to 
     * a department at any time). Defaults to the general undergraduate department = 0. 
     * @return Returns true on success.
     */
    auto remove_research(RID project, DID new_department = DID(0)) -> bool;

private:
    Admin_DB* const _adminsys; ///< Backreference to the database.

    DID _id;             ///< DID for the department.
    std::string _name;   ///< Name for the department.
    std::string _prefix; ///< Course prefix for assigned courses.
    bool _archived;      ///< Is this department archived?

    std::vector<UID> _teachers; ///< Vector of UIDs for assigned teachers.
    std::vector<UID> _students; ///< Vector of UIDs for enrolled students.
    std::vector<CID> _courses;  ///< Vector of CIDs for assigned courses.
    std::vector<RID> _research; ///< Vector of RIDs for assigned research.

};

}
