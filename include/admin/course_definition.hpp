#pragma once

#include "id.hpp"

#include "sdf/sdf_node.hpp"

#include <string>
#include <vector>

namespace admin
{

class Admin_DB;   // Forward declaration for cyclic dependency
class Department; // Forward declaration for cyclic dependency

/**
 * @brief A definition for a course.
 */
class Course_Definition
{
friend Department;
public:
    Course_Definition(Admin_DB* const adminsys = nullptr) : _adminsys(adminsys) {};

    /**
     * @brief Base constructor for a course definition. 
     * Constructs a course definition from the given parameters. No prereqs, coreqs, or 
     * equivalencies will be defined.
     */
    Course_Definition(Admin_DB* const adminsys, CID id, DID department,
                      std::string name, std::string number, unsigned int credits);

    /**
     * @brief Full constructor for a course definition.
     * Constructs a course definition from the given parameters. 
     */
    Course_Definition(Admin_DB* const adminsys, CID id,  DID department,
                      std::string name, std::string number, unsigned int credits,
                      std::vector<CID> prereqs, std::vector<CID> coreqs, 
                      std::vector<CID> equivalencies, bool retired);

    /**
     * @brief Returns a Maybe type with a Course Definition from the SDF_Branch, or empty on failure.
     */
    static auto from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Course_Definition>;

    /**
     * @brief Returns an SDF_Node made from the given course definition.
     */
    static auto to_sdf(const Course_Definition& course) -> sdf::SDF_Node;

    /**
     * @brief Returns the course definition CID.
     */
    auto id() const -> CID;

    /**
     * @brief Returns the course definition's name.
     */
    auto name() const -> std::string;

    /**
     * @brief Returns the number for the course definition.
     */
    auto number() const -> std::string;

    /**
     * @brief Returns the assigned department DID.
     */
    auto department() const -> DID;

    /**
     * @brief Returns a vector of the CID for prerequisite courses.
     */
    auto prereqs() const -> std::vector<CID>;

    /**
     * @brief Returns a vector of the CID for corequisite courses.
     */
    auto coreqs() const -> std::vector<CID>;

    /**
     * @brief Returns a vector of the CID for equivalent courses.
     */
    auto equivalencies() const -> std::vector<CID>;

    /**
     * @brief returns the number of credits this course is worth. 
     */
    auto credits() const -> unsigned int;

    /**
     * @brief Returns true if this course has been retired and sections may not longer be made. 
     */
    auto retired() const -> bool;

    /**
     * @brief Returns true if this course is equivalent to the given CID. 
     */
    auto is_equivalent(CID course_def) const -> bool;

    /**
     * @brief Returns true if the given teacher can teach this course.
     */
    auto can_teach(UID teacher) const -> bool;

    /**
     * @brief Returns true if the given student may assist with this course. 
     */
    auto can_assist(UID student) const -> bool;

    /**
     * @brief Returns true if the given student may enroll in this course. 
     */
    auto can_enroll(UID student) const -> bool;

    /**
     * @brief Add's the specified course equivalency.
     * 
     * This is not a bidirectional operation, you must explicitly add equivalence
     * from the other course to this if you desire such a relationship.
     *
     * @see remove_equivalency
     * @see equivalencies
     * @see is_equivalent
     */
    auto add_equivalency(CID course_def) -> void;

    /**
     * @brief Removes equivalency with the specified course.
     * 
     * This is not a bidirectional operation. If the other course is also equivalent
     * to this course, you must remove it from that object explicitly.
     * 
     * @see add_equivalency
     * @see equivalencies
     * @see is_equivalent
     */
    auto remove_equivalency(CID course_def) -> void;

    /**
     * @brief Retire this course, blocking new sections from being created..
     */
    auto retire() -> void;

protected:
    /**
     * @brief Changes the department to the given department DID. Protected for restricted access.
     */
    auto change_department(DID new_department) -> void;

private:
    Admin_DB* const _adminsys; ///< Back-reference to database.

    CID _id;               ///< Course CID.
    DID _department;       ///< Department's DID.
    std::string _name;     ///< Name of the course
    std::string _number;   ///< Course number.
    unsigned int _credits; ///< Credit value.
    bool _retired;         ///< Is this course definition retired?
 
    std::vector<CID> _prereqs;       ///< Prerequisite course CIDs.
    std::vector<CID> _coreqs;        ///< Corequisite course CIDs.
    std::vector<CID> _equivalencies; ///< Equivalent course CIDs.
};

}
