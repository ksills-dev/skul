#pragma once

#include <string>
#include <unordered_map>
#include <vector>
#include <memory>

#include "sdf/sdf_parser.hpp"
#include "util/maybe.hpp"
#include "util/logger.hpp"

#include "id.hpp"
#include "department.hpp"
#include "person.hpp"
#include "student.hpp"
#include "teacher.hpp"
#include "course_definition.hpp"
#include "course_section.hpp"
#include "research_project.hpp"

using namespace util;

namespace admin
{

/**
 * @brief An top-level school adminstration database.
 *
 * The premise of this class is not to work directly through this handle, but rather
 * utilize an access-filter-request model. Using this you grab weak references to objects,
 * filter the provided list down to what you want, and then request operations directly
 * on those objects.
 */
class Admin_DB
{
public:
    /**
     * @brief Constructs the database in the provided directly. Does not save or load 
     * on construct/destruct.
     */
    Admin_DB(std::string dirpath);

    /**
     * @brief Loads in the database files, returns true on success.
     */
    auto load() -> bool;

    /**
     * @brief Saves our database files, returns true on success.
     */
    auto save() -> bool;

    /**
     * @brief Logs the given message at Log_Level::Info.
     */
    auto log(std::string message) const -> void;

    /**
     * @brief Logs the given message at the given log level, with an identifier for the 
     * passed ID.
     */
    template<typename ID>
    auto log(ID id, util::Log_Level level, std::string message) const -> void
    {
        std::string full_message = "";
        if (std::is_same<ID, DID>::value) {
            full_message += "Department " + id.as_string() + " -> ";
        }
        else if (std::is_same<ID, UID>::value) {
            full_message += "Person " + id.as_string() + " -> ";
        }
        else if (std::is_same<ID, RID>::value) {
            full_message += "Research " + id.as_string() + " -> ";
        }
        else if (std::is_same<ID, CID>::value) {
            full_message += "Course " + id.as_string() + " -> ";
        }
        else if (std::is_same<ID, SID>::value) {
            full_message += "Section " + id.as_string() + " -> ";
        }
        full_message += message;

        log(level, full_message);
    }

    /**
     * @brief Log the given message at the provided logging level.
     */
    auto log(util::Log_Level level, std::string message) const -> void;

    /**
     * @brief Logs the given message with an identifier for the passed ID.
     */
    template<typename ID>
    auto log(ID id, std::string message) const -> void 
    {
        log<ID>(id, Log_Level::Info, message);
    }

    /**
     * @brief Enroll the person as a student into the given department.
     *
     * @returns A Maybe type with the generated UID or nothing on failure.
     */
    auto enroll_student(Person person, DID department) -> Maybe<UID>;

    /**
     * @brief Remove the student from active enrollment.
     * The student will no longer be able to be registered or assigned to courses and
     * all existing courses will be archived.
     */
    auto unenroll_student(UID student) -> void;

    /**
     * @brief Return an unordered map of students with UIDs as keys and weak references as values.
     */
    auto student_map() const -> std::unordered_map<UID, std::weak_ptr<Student>, UID::hash>;

    /**
     * @brief Return a vector of weak references to all our registered students.
     */
    auto students() const -> std::vector<std::weak_ptr<Student>>;

    /**
     * @brief Return a Maybe type containing a weak reference to the Student of that UID. 
     */
    auto student(UID student) const -> Maybe<std::weak_ptr<Student>>;

    /**
     * @brief Hire the person as a student into the given department with that level of employment.
     *
     * @returns A Maybe type with the generated UID or nothing on failure.
     */
    auto hire_teacher(Person person, DID department, Teacher_Kind kind) -> Maybe<UID>;

    /**
     * @brief Remove the teacher from active employment.
     * The teacher will no longer be able to be registered or assigned to courses and
     * all existing courses will be archived.
     */
    auto fire_teacher(UID teacher) -> void;

    /**
     * @brief Return an unordered map of teachers with UIDs as keys and weak references as values.
     */
    auto teacher_map() const -> std::unordered_map<UID, std::weak_ptr<Teacher>, UID::hash>;

    /**
     * @brief Return a vector of weak references to all our registered teachers.
     */
    auto teachers() const -> std::vector<std::weak_ptr<Teacher>>;

    /**
     * @brief Return a Maybe type containing a weak reference to the Teacher of that UID. 
     */
    auto teacher(UID teacher) const -> Maybe<std::weak_ptr<Teacher>>;

    /**
     * @brief Begin a research projects with the given name and department. 
     *
     * @return A Maybe type with the RID on success, or empty on failure.
     */
    auto start_research(std::string name, DID department) -> Maybe<RID>;

    /**
     * @brief Cancel the research project with the given RID. 
     */
    auto cancel_research(RID project) -> void;

    /**
     * @brief Return an unordered map of research with RIDs as keys and weak references as values.
     */
    auto research_map() const -> std::unordered_map<RID, std::weak_ptr<Research_Project>, RID::hash>;

    /**
     * @brief Return a vector of weak references to all our registered research projects.
     */
    auto research() const -> std::vector<std::weak_ptr<Research_Project>>;

    /**
     * @brief Return a Maybe type containing a weak reference to the Research of that RID. 
     */
    auto research(RID project) const -> Maybe<std::weak_ptr<Research_Project>>;

    /**
     * @brief Define a course with the given parameters, returning a Maybe type with the CID on success.
     */
    auto define_course(std::string name, std::string number, DID department, int credits) -> Maybe<CID>;

    /**
     * @brief Define a course with the given parameters, returning a Maybe type with the CID on success.
     */
    auto define_course(std::string name, std::string number, DID department, int credits,
                       std::vector<CID> prereqs, std::vector<CID> coreqs,
                       std::vector<CID> equivalencies) -> Maybe<CID>;

    /**
     * @brief Retire the course definition with the given CID. Preventing new sections from being made.
     */
    auto retire_course(CID course_def) -> void;

    /**
     * @brief Return an unordered map of course definitions with CIDs as keys and weak references as values.
     */
    auto course_def_map() const -> std::unordered_map<CID, std::weak_ptr<Course_Definition>,CID::hash>;

    /**
     * @brief Return a vector of weak references to all our registered course definitions.
     */
    auto course_defs() const -> std::vector<std::weak_ptr<Course_Definition>>;

    /**
     * @brief Return a Maybe type containing a weak reference to the Course Definition of that CID. 
     */
    auto course_def(CID course) const -> Maybe<std::weak_ptr<Course_Definition>>;

    /**
     * @brief Begin a research course section with the definition and seat count. 
     *
     * @return A Maybe type with the SID on success, or empty on failure.
     */
    auto create_course(CID course_def, int seats) -> Maybe<SID>;

    /**
     * @brief Cancel the course with the given SID.
     */
    auto cancel_course(SID course_section) -> void;

    /**
     * @brief Return an unordered map of course sections with SIDs as keys and weak references as values.
     */
    auto course_map() const -> std::unordered_map<SID, std::weak_ptr<Course_Section>, SID::hash>;

    /**
     * @brief Return a vector of weak references to all our registered course sections.
     */
    auto courses() const -> std::vector<std::weak_ptr<Course_Section>>;

    /**
     * @brief Return a Maybe type containing a weak reference to the Course Section of that SID. 
     */
    auto course(SID section) const -> Maybe<std::weak_ptr<Course_Section>>;

    /**
     * @brief Create a department with the given name and course prefix. 
     *
     * @return A Maybe type with the DID on success, or empty on failure.
     */
    auto create_department(std::string name, std::string prefix) -> Maybe<DID>;

    /**
     * @brief Archive the department with this ID.
     */
    auto archive_department(DID department) -> void;

    /**
     * @brief Return an unordered map of departments with DIDs as keys and weak references as values.
     */
    auto department_map() const -> std::unordered_map<DID, std::weak_ptr<Department>, DID::hash>;

    /**
     * @brief Return a vector of weak references to all our registered departments.
     */
    auto departments() const -> std::vector<std::weak_ptr<Department>>;

    /**
     * @brief Return a Maybe type containing a weak reference to the Deparmtne of that DID. 
     */
    auto department(DID department) const -> Maybe<std::weak_ptr<Department>>;

private:
    std::string _dirpath;         ///< The directory path for the database files.
    mutable util::Logger _logger; ///< A logger to write all our events.

    UID_Factory _uid_factory; ///< A factory to create UIDs 
    DID_Factory _did_factory; ///< A factory to create DIDs
    RID_Factory _rid_factory; ///< A factory to create RIDs
    CID_Factory _cid_factory; ///< A factory to create CIDs
    SID_Factory _sid_factory; ///< A factory to create SIDs

    std::unordered_map<UID, std::shared_ptr<Student>, UID::hash> _students;
    std::unordered_map<UID, std::shared_ptr<Teacher>, UID::hash> _teachers;
    std::unordered_map<DID, std::shared_ptr<Department>, DID::hash> _departments;
    std::unordered_map<RID, std::shared_ptr<Research_Project>, RID::hash> _research;
    std::unordered_map<CID, std::shared_ptr<Course_Definition>, CID::hash> _course_defs;
    std::unordered_map<SID, std::shared_ptr<Course_Section>, SID::hash> _course_secs;
};

}
