#pragma once

#include "id.hpp"
#include "assignment.hpp"

#include "sdf/sdf_node.hpp"
#include "util/maybe.hpp"

#include <vector>
#include <unordered_map>
#include <string>

namespace admin
{

class Admin_DB;

/**
 * @brief An unordered map relating assignment grades to an student UID and assignment AID.
 */
using Grade_Book = std::unordered_map<UID, 
                                      std::unordered_map<AID, Assignment_Grade, 
                                                         AID::hash>,
                                      UID::hash>;

/**
 * @brief A specific instance (section) for a given course definition.
 */
class Course_Section
{
public:
    Course_Section(Admin_DB* const adminsys = nullptr) : _adminsys(adminsys) {};

    /**
     * @brief Base constructor. Creates an empty course section with the given parameters.
     */
    Course_Section(Admin_DB* const adminsys, SID id, CID definition, unsigned int seats);

    /**
     * @brief Full constructor. Creates a course section with the given parameters.
     */
    Course_Section(Admin_DB* const adminsys, SID id, CID definition, unsigned int seats,
                   std::vector<UID> teachers, std::vector<UID> students, std::vector<UID> assistants,
                   std::vector<Assignment> assignments, Grade_Book grade_book,
                   bool active);

    /**
     * @brief Returns a Maybe type with a Course Section from the SDF_Branch, or empty on failure.
     */
    static auto from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Course_Section>;

    /**
     * @brief Returns an SDF_Node made from the given course section.
     */
    static auto to_sdf(const Course_Section& section) -> sdf::SDF_Node;

    /**
     * @brief Returns the SID for this section.
     */
    auto id() const -> SID;

    /**
     * @brief Returns the CID for this sections' definition.
     */
    auto definition() const -> CID;

    /**
     * @brief Changes the number of seats offered by this course, returns true on success.
     * @note Will fail if the number of students already enrolled is greater.
     */
    auto set_seats(const int number) -> bool;

    /**
     * @brief Returns the total number of seats in this section.
     */
    auto total_seats() const -> unsigned int;

    /**
     * @brief Returns the number of free seats in this section.
     */
    auto free_seats() const -> unsigned int;

    /**
     * @brief Returns the number of students in this section.
     */
    auto student_count() const -> unsigned int;

    /**
     * @brief Returns true if this course is active.
     */
    auto is_active() const -> bool;

    /**
     * @brief Finalizes all grades in the course and archives in student & teacher records.
     */
    auto finalize_course() -> void;

    /**
     * @brief Returns true if the teacher can teach this course.
     */
    auto can_teach(UID teacher) const -> bool;

    /**
     * @brief Returns true if the student can assist in this course.
     */
    auto can_assist(UID student) const -> bool;

    /**
     * @brief Returns true if the student can enroll in this course.
     */
    auto can_enroll(UID student) const -> bool;

    /**
     * @brief Try to assign the given teacher UID to this course. Return true on success.
     */
    auto assign_teacher(UID teacher) -> bool;

    /**
     * @brief Remove the teacher with the given UID.
     */
    auto remove_teacher(UID teacher) -> void;

    /**
     * @brief Try to assign the given student UID to this course as an assistant. Return true on success.
     */
    auto assign_assistant(UID student) -> bool;

    /**
     * @brief Remove the assistant with the given UID.
     */
    auto remove_assistant(UID student) -> void;

    /**
     * @brief Try to enroll the given student UID to this course. Return true on success.
     */
    auto enroll_student(UID student) -> bool;

    /**
     * @brief Remove the student with the given UID.
     */
    auto remove_student(UID student) -> bool;

    /**
     * @brief Returns a vector of UIDs for this course's assigned teachers.
     */
    auto teachers() const -> std::vector<UID>;

    /**
     * @brief Returns a vector of UIDs for this course's assigned assistants.
     */
    auto assistants() const -> std::vector<UID>;

    /**
     * @brief Returns a vector of UIDs for this course's enrolled students.
     */
    auto students() const -> std::vector<UID>;

    /**
     * @brief Returns true if the given teacher UID is teaching this course. 
     */
    auto is_teaching(UID teacher) const -> bool;
    
    /**
     * @brief Returns true if the given student UID is assisting with this course.
     */
    auto is_assisting(UID student) const -> bool;

    /**
     * @brief Returns true if the given student UID is enrolled in this course.
     */
    auto is_enrolled(UID student) const -> bool;

    /**
     * @brief Returns the list of Assignments in this section.
     */
    auto assignments() const -> std::vector<Assignment>;

    /**
     * @brief Returns a Maybe type containing the Assignment with the given AID, or empty on failure.
     */
    auto assignment(AID assignment) const -> util::Maybe<Assignment>;

    /**
     * @brief Creates an assignment with the given parameter. 
     * 
     * @return A Maybe type containing the generated AID on success, or empty on failure. 
     */
    auto create_assignment(std::string name, float max_score, float weight) -> util::Maybe<AID>;

    /**
     * @brief Remove the Assignment (and associated grades) with the given AID.
     */
    auto remove_assignment(AID assignment) -> void;
    
    /**
     * @brief Calculates and returns the GPA (0.00-4.00) of the desired student.
     *
     * Will return an empty Maybe type if no such student is enrolled or if the course has
     * an invalid number of points allotted.
     */
    auto gpa(UID student) const -> util::Maybe<float>;

    /**
     * @brief Returns a copy of the course Grade Book.
     */
    auto grade_book() const -> Grade_Book;

    /**
     * @brief Assigns the grade to the assignment AID to the student UID. Returns true on success.
     */
    auto grade_assignment(UID student, AID assignment, float score) -> bool;

    /**
     * @brief Returns a Maybe type containing the grade for the student's assignment, or empty if no such exists..
     */
    auto assignment_grade(UID student, AID assignment) const -> util::Maybe<Assignment_Grade>;

private:
    Admin_DB* const _adminsys; ///< Backreference to database.

    SID _id;             ///< SID for the section.
    CID _definition;     ///< CID for the definition.
    bool _active;        ///< Is this section in progress?
    unsigned int _seats; ///< How many seats are allowed in this section?

    std::vector<UID> _teachers;   ///< Vector of teacher UIDs.
    std::vector<UID> _students;   ///< Vector of student UIDs.
    std::vector<UID> _assistants; ///< Vector of assistant UIDs.

    std::unordered_map<AID, Assignment, AID::hash> _assignments;
    Grade_Book _grade_book;

    AID_Factory _aid_factory; ///< Factory to generate assignment AIDs.
};

}
 