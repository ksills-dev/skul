#pragma once

#include "id.hpp"

#include "sdf/sdf_node.hpp"

#include <string>

namespace admin
{

class Admin_DB; // Forward declare for cyclic dependencies.

/**
 * @brief The grade for an assignment.
 */
struct Assignment_Grade
{
    Assignment_Grade() = default;
    Assignment_Grade(bool graded, float earned_score, float max_score)
        : graded(graded), earned_score(earned_score), max_score(max_score) {};

    /**
     * @brief Generate an Assignment Grade from an SDF_Branch, if the branch is valid.
     */
    static auto from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) 
        -> util::Maybe<Assignment_Grade>;

    /**
     * @brief Generate an SDF_Node from a grade.
     */
    static auto to_sdf(Admin_DB* const adminsys, const Assignment_Grade& grade) -> sdf::SDF_Node;

    bool graded = false;    ///< Has this assignment been graded?
    float earned_score = 0; ///< The score earned by the student
    float max_score = 100;  ///< The max possible score
};

/**
 * @brief The definition for a course assignment.
 */
struct Assignment
{
    Assignment() = default;
    Assignment(AID id, std::string name, float max_score, float weight) 
        : id(id), name(name), max_score(max_score), weight(weight) {}; 

    /**
     * @brief Generate an Assignment definition from an SDF_Branch, if the branch is valid.
     */
    static auto from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Assignment>;

    /**
     * @brief Generate an SDF_Node from an assignment definition.
     */
    static auto to_sdf(Admin_DB* const adminsys, const Assignment& assignment) -> sdf::SDF_Node;

    AID id;                  ///< The AID for the assignment
    std::string name;        ///< The assignment name
    float max_score = 100.0; ///< The max score someone may earn.
    float weight = 1.0;      ///< The weight (multiplier) of this assignment.
};

}
