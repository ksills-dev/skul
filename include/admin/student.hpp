#pragma once

#include "id.hpp"
#include "registered_person.hpp"

#include "sdf/sdf_node.hpp"
#include "util/maybe.hpp"

#include <string>
#include <vector>

namespace admin
{

/**
 * @brief The current grade level for a student.
 */
enum class Grade_Level {
    Freshman,
    Sophmore,
    Junior,
    Senior,
    Masters,
    Doctorate
};

class Research_Project; // Forward declaration for cyclic dependency
class Course_Section;   // Forward declaration for cyclic dependency

/**
 * @brief A registered student at a university.
 */
class Student : public Registered_Person
{
friend Research_Project;
friend Course_Section;

public:
    Student() = default;

    /**
     * @brief Base constructor. Constructs an empty student from the given parameters.
     */
    Student(const Registered_Person person, Grade_Level grade = Grade_Level::Freshman);

    /**
     * @brief Full constructor. Constructs a student from the given parameters.
     */
    Student(const Registered_Person person, Grade_Level grade,
            std::vector<RID> current_research, std::vector<RID> completed_research,
            std::vector<SID> current_ta_courses, std::vector<SID> completed_ta_courses,
            std::vector<SID> enrolled_courses, std::vector<SID> completed_courses);
    virtual ~Student() = default;

    /**
     * @brief Returns a Maybe type with a Student from the SDF_Branch, or empty on failure.
     */
    static auto from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Student>;

    /**
     * @brief Returns an SDF_Node made from the given student.
     */
    static auto to_sdf(const Student& student) -> sdf::SDF_Node; 

    /**
     * @brief Returns the grade level of this student.
     */
    auto grade_level() const -> Grade_Level;

    /**
     * @brief Returns true if this student is an assistant.
     */
    auto is_assistant() const -> bool;

    /**
     * @brief Returns true if this student is a teaching assistant.
     */
    auto is_teaching_assistant() const -> bool;

    /**
     * @brief Returns true if this student is a research assistant.
     */
    auto is_research_assistant() const -> bool;

    /**
     * @brief Changes the student's grade level.
     */
    auto change_grade_level(const Grade_Level grade) -> void;

    /**
     * @brief Attempt to enroll student in the given course section. Return true on success.
     */
    auto enroll_course(SID section) -> bool;

    /**
     * @brief Attempt to withdraw student from the course section. Return true on success.
     */
    auto withdraw_course(SID section) -> bool;

    /**
     * @brief Returns the SIDs for currently enrolled courses. 
     */
    auto enrolled_courses() const -> std::vector<SID>;

    /**
     * @brief Returns SIDs for previously enrolled courses.
     */
    auto course_history() const -> std::vector<SID>;

    /**
     * @brief Returns true if student is currently enrolled in the section.
     */
    auto is_enrolled_in(SID section) const -> bool;

    /**
     * @brief Returns true if the student has ever previously taken the section.
     */
    auto has_taken(SID section) const -> bool;

    /**
     * @brief Returns true if the student is currently taking a section of the given definition.
     */
    auto is_enrolled_in(CID section) const -> bool;

    /**
     * @brief Returns true if the student has ever taken a section of the given definition.
     */
    auto has_taken(CID section) const -> bool;

    /**
     * @brief Returns a vector of SIDs for currently assisting courses.
     */
    auto assisting_courses() const -> std::vector<SID>;

    /**
     * @brief Returns a vector of SIDs for previously assisted courses.
     */
    auto past_assisting_courses() const -> std::vector<SID>;

    /**
     * @brief Returns a vector of RIDs for currently assisting research.
     */
    auto assisting_research() const -> std::vector<RID>;

    /**
     * @brief Returns a vector of RIDs for previously assisted research.
     */
    auto past_assisting_research() const -> std::vector<RID>;

    /**
     * @brief Returns true if the student is currently assisting in the section.
     */
    auto is_assisting(SID section) const -> bool;

    /**
     * @brief Returns true if the student is currently assisting in the research project.
     */
    auto is_assisting(RID project) const -> bool;

    /**
     * @brief Returns true if the student can assist projects or courses.
     */
    auto can_assist() const -> bool;

    /**
     * @brief Returns true if the student can assist the given section.
     */
    auto can_assist(SID section) const -> bool;

    /**
     * @brief Returns true if the student can assist the given project.
     */
    auto can_assist(RID project) const -> bool;

    /**
     * @brief Attempt to enroll in the section as an assistant. Return true on success.
     */
    auto enroll_as_assistant(SID section) -> bool;

    /**
     * @brief Attempt to enroll in the project as an assistant. Return true on success.
     */
    auto enroll_as_assistant(RID project) -> bool;

    /**
     * @brief Attempt to withdraw from assisting the section. Return true on success.
     */
    auto withdraw_from_assisting(SID section) -> bool;

    /**
     * @brief Attempt to withdraw from assisting the project. Return true on success.
     */
    auto withdraw_from_assisting(RID project) -> bool;

    /**
     * @brief Deactivate the student, withdrawing them from all their course and preventing future enrollment.
     */
    virtual auto deactivate() -> void override final;

protected:
    /**
     * @brief Adds the course section to the student. Restricted Access.
     */
    auto add_course(SID section) -> void;

    /**
     * @brief Adds the course section to assisting courses. Restricted Access.
     */
    auto assign_as_assistant(SID section) -> void;

    /**
     * @brief Adds the project to assisting courses. Restricted Access.
     */
    auto assign_as_assistant(RID project) -> void;

    /**
     * @brief Removes the section from course history. Restricted Access.
     */
    auto remove_course(SID section) -> void;

    /**
     * @brief Removes the section from assisted courses. Restricted Access.
     */
    auto remove_from_assisting(SID section) -> void;
    
    /**
     * @brief Removes the project from assisted proejcts. Restricted Access.
     */
    auto remove_from_assisting(RID project) -> void;

    /**
     * @brief Archives the course section from enrollment. Restricted Access.
     */
    auto finalize_course(SID section) -> void;

    /**
     * @brief Archives the assisting course section. Restricted Access.
     */
    auto finalize_assisting_course(SID section) -> void;

    /**
     * @brief Archives the assisting research project. Restricted Access.
     */
    auto finalize_assisting_research(RID project) -> void;

private:
    Grade_Level _grade; ///< Student's current grade level.

    std::vector<RID> _current_research;     ///< Vector of current research RIDs
    std::vector<RID> _completed_research;   ///< Vector of completed research RIDs
    std::vector<SID> _current_ta_courses;   ///< Vector of current ta course SIDs
    std::vector<SID> _completed_ta_courses; ///< Vector of completed ta course SIDs
    std::vector<SID> _enrolled_courses;     ///< Vector of enrolled course SIDs
    std::vector<SID> _completed_courses;    ///< Vector of completed course SIDs
};

}
