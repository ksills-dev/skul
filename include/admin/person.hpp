#pragma once

#include "date.hpp"
#include "sdf/sdf_node.hpp"

#include <string>

namespace admin
{

/**
 * @brief The gender of a person.
 */
enum class Gender {
    Male, 
    Female
};

class Admin_DB; // Forward declare for cyclic dependency.

/**
 * @brief A generic Person, not necessarily associated with a university.
 */
class Person
{
public:
    Person() {};
    Person(std::string first_name, std::string last_name, Date birth_date, Gender gender)
        : _first_name(first_name), _last_name(last_name), _birth_date(birth_date), _gender(gender) {};
    virtual ~Person() = default;

    static auto from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Person>;
    static auto to_sdf(Admin_DB* const adminsys, const Person& person) -> sdf::SDF_Node;

    auto first_name() const -> std::string { return _first_name; };
    auto last_name() const -> std::string { return _last_name; };
    auto full_name() const -> std::string { return (_first_name + _last_name); };
    auto birth_date() const -> Date { return _birth_date; };
    auto gender() const -> Gender { return _gender; };

protected:
    std::string _first_name;
    std::string _last_name;
    Date _birth_date;
    Gender _gender;
};

}
