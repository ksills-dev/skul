#pragma once

#include <string>
#include <exception>

#include "sdf/sdf_node.hpp"
#include "util/maybe.hpp"

namespace admin
{

class Admin_DB; // Forward declare for cyclic dependency.

/**
 * @brief An exception type for a date with invalid ranges.
 */
class Invalid_Date : std::exception
{
public:
    /**
     * @brief Constructor with explicit date values.
     */
    Invalid_Date(int year,      int month,      int day) 
        : _year(year), _month(month), _day(day) {};

    /**
     * @brief Constructor from another exception.
     */
    Invalid_Date(const Invalid_Date& other)
        : _year(other._year), _month(other._month), _day(other._day) {};

    /**
     * @brief Exception assignment operator.
     */
    auto operator =(const Invalid_Date& other) -> Invalid_Date&
    {
        this->_year = other._year;
        this->_month = other._month;
        this->_day = other._day;
        return *this;
    }

    /**
     * @brief Returns a string describing our exceptional case.
     */
    auto what() const noexcept -> const char* 
    {
        std::string reply = "Supplied invalid date: ";
        reply += std::to_string(_day);
        reply += "/";
        reply += std::to_string(_month);
        reply += "/";
        reply += std::to_string(_year);
        reply += "\n";
        
        return reply.data();
    }

private:
    int _year;  ///< Year of the date.
    int _month; ///< Month of the date.
    int _day;   ///< Day of the date.
};

/**
 * @brief A date of year, month, and day.
 */
class Date
{
public:
    Date() {};

    /**
     * @brief Constructor a date from the given values.
     */
    Date(int year, int month, int day);

    /**
     * @brief Copy constructor for Date.
     */
    Date(const Date& other);

    /**
     * @brief Returns a Maybe type containing a Date parsed from the given SDF_Branch, or empty on failure.
     */
    static auto from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Date>;

    /**
     * @brief Returns an SDF_Node constructed from the given date.
     */
    static auto to_sdf(Admin_DB* const adminsys, const Date& date) -> sdf::SDF_Node;

    /**
     * @brief Sets the year of our date.
     */
    auto year(const int year) -> void;

    /**
     * @brief Returns the year of our date.
     */
    auto year() const -> int;

    /**
     * @brief Sets the month of our date.
     */
    auto month(const int month) -> void;

    /**
     * @brief Returns the month of our date.
     */
    auto month() const -> int;

    /**
     * @brief Sets the day of the date.
     */
    auto day(const int day) -> void;

    /**
     * @brief Returns the day of the date.
     */
    auto day() const -> int;

    /**
     * @brief Returns the distances between two dates, in days.
     * @see operator-
     */
    auto distance(const Date& other) const -> int;

    /**
     * @brief Returns true if this date is on a leap year.
     */
    auto is_leap_year() const -> bool;

    /**
     * @brief Iterates the date by a day.
     */
    auto operator ++() -> Date&;

    /**
     * @brief Iterates the date by a day.
     */
    auto operator ++(int) -> Date; // Postfix

    /**
     * @brief Reduces the date by a day.
     */
    auto operator --() -> Date&;

    /**
     * @brief Reduces the date by a day.
     */
    auto operator --(int) -> Date; // Postfix

    /**
     * @brief Adds the provided number of days to the date.
     */
    auto operator +(const int days) -> Date;

    /**
     * @brief Removes the provided number of days to the date.
     */
    auto operator -(const int days) -> Date;

    /**
     * @brief Adds the provided number of days to the date in-place.
     */
    auto operator +=(const int days) -> Date&;

    /**
     * @brief Removes the provided number of days to the date in-place.
     */
    auto operator -=(const int days) -> Date&;

    /**
     * @brief Returns the difference between two dates, in days.
     *
     * @see distance
     */
    auto operator -(const Date& other) -> int;

    /**
     * @brief Returns true if the left operand is a later date than the right.
     */
    auto operator >(const Date& other) const -> bool;

    /**
     * @brief Returns true if the left operand is a later date or equal to the right.
     */
    auto operator >=(const Date& other) const -> bool;

    /**
     * @brief Returns true if the left operand is an earlier date than the right.
     */
    auto operator <(const Date& other) const -> bool;

    /**
     * @brief Returns true if the left operand is a earlier date or equal to the right.
     */
    auto operator <=(const Date& other) const -> bool;

    /**
     * @brief Returns true if the dates are the same.
     */
    auto operator ==(const Date& other) const -> bool;

    /**
     * @brief Returns false if the dates are the same.
     */
    auto operator !=(const Date& other) const -> bool;

private:
    int _year;  ///< The date's year.
    int _month; ///< The date's month.
    int _day;   ///< The date's day.
};

}
