#pragma once

#include "id.hpp"

#include "sdf/sdf_node.hpp"
#include "admin/student.hpp"
#include "admin/teacher.hpp"

#include <string>
#include <vector> 

namespace admin
{

class Admin_DB;   // Forward declare for cyclic dependency
class Department; // Forward declare for cyclic dependency

/**
 * @brief The status of a research project.
 */
enum class Research_Status {
    Ongoing,
    Complete,
    Cancelled
};

/**
 * @brief A research project conducted by a university.
 */
class Research_Project
{
friend Department;
public:
    Research_Project() : _adminsys(nullptr) {};

    /**
     * @brief Base constructor. Constructs an empty research project from the given parameters.
     */
    Research_Project(Admin_DB* const adminsys, RID id, DID department, std::string name);

    /**
     * @brief Full constructor. Constructs a research project from the given parameters.
     */
    Research_Project(Admin_DB* const adminsys, RID id, DID department, std::string name,
               std::vector<UID> teachers, std::vector<UID> assistants,
               Research_Status status);

    /**
     * @brief Returns a Maybe type with a Research Project from the SDF_Branch, or empty on failure.
     */
    static auto from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Research_Project>;

    /**
     * @brief Returns an SDF_Node made from the given research project.
     */
    static auto to_sdf(const Research_Project& research) -> sdf::SDF_Node;

    /**
     * @brief Returns the RID for the project.
     */
    auto id() const -> RID;

    /**
     * @brief Returns the department DID for the project.
     */
    auto department() const -> DID;

    /**
     * @brief Returns the project name.
     */
    auto name() const -> std::string;

    /**
     * @brief Returns true if this project has been completed.
     */
    auto completed() const -> bool;

    /**
     * @brief Returns true if this project has been cancelled.
     */
    auto cancelled() const -> bool;

    /**
     * @brief Returns the status of our project.
     * 
     * @see Research_Status
     */
    auto status() const -> Research_Status;

    /**
     * @brief Returns a vector of assigned teacher UIDs.
     */
    auto teachers() const -> std::vector<UID>;

    /**
     * @brief Returns a vector of assigned assistant UIDs.
     */
    auto assistants() const -> std::vector<UID>;

    /**
     * @brief Return true if the person can be assigned to this research project.
     */
    auto can_research(UID person) const -> bool;

    /**
     * @brief Returns true if this person has already been assigned to this research project.
     */
    auto is_assigned(UID person) const -> bool;

    /**
     * @brief Attempts to assign the teacher to the project. Returns true on success.
     */
    auto assign_teacher(UID teacher) -> bool;

    /**
     * @brief Removes the teacher with the given UID from the project.
     */
    auto remove_teacher(UID teacher) -> void;

    /**
     * @brief Attempts to assign the student to the project as an assistant. Returns true on success.
     */
    auto assign_assistant(UID student) -> bool;

    /**
     * @brief Removes the assistant with the given UID from the project.
     */
    auto remove_assistant(UID student) -> void;

    /**
     * @brief Finalizes this project as complete.
     */
    auto complete() -> void;

    /**
     * @brief Finalizes this project as cancelled.
     */
    auto cancel() -> void;

protected:
    /**
     * @brief Changes department to the new department. Restricted Access.
     */
    auto change_department(DID new_department) -> void;

private:
    Admin_DB* const _adminsys; ///< Backreference to database.
    
    RID _id;           ///< RID of the project.
    DID _department;   ///< DID of the department.
    std::string _name; ///< Name of the project.

    std::vector<UID> _teachers;   ///< Vector of UIDs for assigned teachers.
    std::vector<UID> _assistants; ///< Vector of UIDs for assigned assistants.

    Research_Status _status; ///< Current status of the project.
};

}
