#pragma once

#include <unordered_map>
#include <string>
#include <iostream>
#include <mutex>

namespace tester 
{

/**
 * @brief A function pointer for tests. No parameters, returns a bool.
 */
using Test = auto(*)() -> bool;

/**
 * @brief Hash map containing all our sections and associated tests.
 * Section Map:
 * |-> Test Map:
 *     |-> Test Name
 *     |-> Test Function Pointer 
 */
using Test_Map   = std::unordered_map<std::string, std::unordered_map<std::string, Test>>;

/**
 * @brief Hash map containing all our sections and associated test results.
 * Section Map:
 * |-> Test Map:
 *     |-> Test Name
 *     |-> Test result bool
 */
using Result_Map = std::unordered_map<std::string, std::unordered_map<std::string, bool>>;

/**
 * @brief A quick and dirty unit testing class.
 * A fairly simple Unit Testing utility class to run parameter-less functions and
 * report successes based on boolean returns. Provides pretty printing for results
 * and cout output redirection.
 * 
 * Does not allowed shared state between tests.
 * 
 * # Example  
 *     Unit_Tester tester{"Example Program"};
 *     tester.add_test("section_name", "test_name", []() -> bool {
 *         /// ...
 *     })
 *     tester.run_tests();
 * 
 * @note This class is **NOT** thread-safe. It freely modifies the global state of 
 * std::cout during unit tests. 
 * 
 * @note The current iteration was designed to run very small
 * tests sequentally, but it shouldn't be too hard to implement a parallel worker
 * task queue. A mutex for Test_Map grabbing, a mutex for Result_Map emplacement,
 * and breaking `run_test` into a two-phase run/display setup. Not sure how cout 
 * capture would work, but I'll hit that roadblock when I hit it.
 */
class Unit_Tester {
public:
    /**
     * @brief Construct a Unit_Tester with the given suite name.
     */
    Unit_Tester(std::string test_suite_name);
    Unit_Tester() = delete; // Do not allow empty constructor.
    
    /**
     * @brief Default constructor for Unit_Tester.
     * Resets terminal to default settings, closes any cout redirect files, resets cout.
     */
    ~Unit_Tester();

    /**
     * @brief Add a unit test to our suite in the given section, with the given name.
     */
    auto add_test(std::string section, std::string name, Test test) -> void;

    /**
     * @brief Run all unit tests within our suite. Returns a Result_Map of which tests passed/failed.
     */
    auto run_tests() -> Result_Map;
    
    /**
     * @brief Enabled cout redirection to the given fie during tests.
     * The file will be in standard Markdown format with the ".md" extension.
     * Each test will have it's output contained in a code block section under a level-2
     * header with the name of the specific test, under a level-1 header with the name
     * of the given section.
     * 
     * `""` will send output to the void.
     * 
     * # Example File  
     *     # Section_Name
     *     ## Test_Name
     *     ```
     *     Hello world!
     *     ```
     * 
     * @param filename Filename to redirect our cout output to, relative to executable.
     */
    auto set_cout_target(std::string filename) -> bool;
    
    /**
     * @brief Clear the cout redirection target.
     */
    auto clear_cout_target() -> void;

private:    
    /**
     * @brief Redirects future output for cout to our `_cout_target`.
     */
    auto redirect_cout() -> void;
    
    /**
     * @brief Resets our cout target to the terminal.
     */
    auto reset_cout() -> void;
    
    std::string _name; ///< Name of the suite we're testing.
    Test_Map _tests;   ///< A map containing all of our tests.
    int _longest_name; ///< The longest test name, so we can line up all our Pass/Fails. 
    int _count;        ///< The total number of tests.
    
    std::string _cout_target;   ///< Filename for cout redirection during tests.
    std::ofstream* _out_stream; ///< Outbound filestream for our cout redirect file.
};

}