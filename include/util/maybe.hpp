#pragma once

#include <exception>
#include <string>

namespace util
{

/**
 * @brief Small exception type for invalid access to a Maybe type with no valid state.
 */
class Invalid_Maybe_Access : std::exception 
{
public:
    /**
     * @brief Basic default constructor.
     */
    Invalid_Maybe_Access() {};
    
    /**
     * @brief Return a C-string describing our exceptional case.
     */
    virtual auto what() const noexcept -> const char* {
        return std::string("Cannot access empty Maybe type.").data();
    };
};

/**
 * @brief Very small, generic class to represent an object that may or may not exist.
 */
template <typename T>
class Maybe
{
public:
    /**
     * @brief Constructs an empty, non-existant Maybe using default constructors.
     * @note This default constructor prevents value types from being used.
     */
    Maybe() : _exists(false) {};

    /**
     * @brief Constructs Maybe from the given value, guaranteed existence.
     */
    Maybe(T value) : _exists(true), _value(value) {};

    /**
     * @brief Constructs Maybe from the given value, but with conditional existence.
     */
    Maybe(bool exists, T value) : _exists(exists), _value(value) {};

    /**
     * @brief Returns whether or not the Maybe type contains a value.
     */
    auto exists() -> bool { return _exists; };


    /**
     * @brief Return our contained value.
     * @throws Invalid_Maybe_Access when no value exists.
     */
    auto access() -> T {
        if (_exists) { return _value; }
        else         { throw Invalid_Maybe_Access(); }

    }; 

private:
    bool _exists;
    T _value;
};

}