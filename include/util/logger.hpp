#pragma once

#include <fstream>
#include <string>

namespace util
{

/**
 * @brief The level of importance for a given logging message.
 */
enum class Log_Level {
    Info,
    Warning,
    Error
};

/**
 * @brief A very basic logging class, giving the time of event, the level of importance, and a message.
 */
class Logger
{
public:
    /**
     * @brief Construct the logger pointing at the given logfile name.
     */
    Logger(std::string logfile);
    ~Logger();

    /**
     * @brief Sets the tag for information posts to the given string.
     */
    auto set_info_post(std::string post) -> void;

    /**
     * @brief Sets the tag for warning posts to the given string.
     */
    auto set_warning_post(std::string post) -> void;

    /**
     * @brief Sets the tag for error posts to the given string.
     */
    auto set_error_post(std::string post) -> void;

    /**
     * @brief Writes the given message with the desired logging level.
     */
    auto log(Log_Level level, std::string message) -> void;
    
    /**
     * @brief Force all previously written logging data to file now.
     */
    auto flush() -> void;

    /**
     * @brief Stream the message into the log file at the current logging level.
     */
    auto operator <<(std::string message) -> Logger&;

    /**
     * @brief Stream a new logging level, changing the current level.
     */
    auto operator <<(Log_Level level) -> Logger&;

private:
    std::fstream* _handle;    ///< Pointer to logfile handle.
    Log_Level _current_level; ///< Current logging level. 

    std::string _info_post = "INFO";    ///< Tag for information posts.
    std::string _warning_post = "WARN"; ///< Tag for warning posts.
    std::string _error_post = "ERROR";  ///< Tag for error posts. 
};

}
