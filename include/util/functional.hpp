#pragma once

#include <vector>
#include <functional>

namespace func
{

/**
 * @brief Apply the provided function on all elements in the vector, then return that vector.
 */
template<typename T>
auto apply(std::function<void(T&)> function, std::vector<T> list) -> std::vector<T> 
{
    for (auto& element : list) {
        function(element);
    }
    return list;
}

/**
 * @brief Return a vector of elements that pass the provided filter function.
 */
template<typename T>
auto filter(std::function<bool(const T&)> function, std::vector<T> list) -> std::vector<T>
{
    std::vector<T> filtered;
    for (const auto& element : list) {
        if (function(element)) {
            filtered.push_back(element);
        }
    }
    return filtered;
}

/**
 * @brief Generate a 1-1 map of objects from a vector of similarly typed objects. 
 */
template<typename T>
auto map(std::function<T(const T&)> function, std::vector<T> list) -> std::vector<T>
{
    std::vector<T> mapped;
    for (const auto& element : list) {
        mapped.push_back(function(element));
    }
    return mapped;
}

/**
 * @brief  Generates a 1-1 map of objects from a vector of unlike typed objects.
 */
template<typename In, typename Out>
auto map(std::function<Out(const In&)> function, std::vector<In> list) -> std::vector<Out>
{
    std::vector<Out> mapped;
    for (const auto& element : list) {
        mapped.push_back(function(element));
    }
    return mapped;
}

// What's the real name for something like this?
/**
 * @brief Generates a 1-* map of objects from a vector of similarly typed objects.
 */
template <typename T>
auto remap(std::function<std::vector<T>(const T&)> function, std::vector<T> list) 
        -> std::vector<T>
{
    std::vector<T> mapped;
    for (const auto& element : list) {
        for (const auto& result : function(element)) {
            mapped.push_back(result);
        }
    }
    return mapped;
}

/**
 * @brief Generates a 1-* map of objects from a vector of unlike typed objects. 
 */
template<typename In, typename Out>
auto remap(std::function<std::vector<Out>(const In&)> function, std::vector<In> list) 
        -> std::vector<Out>
{
    std::vector<Out> mapped;
    for (const auto& element : list) {
        for (const auto& result : function(element)) {
            mapped.push_back(result);
        }
    }
    return mapped;
}

/**
 * @brief Perform the given function on each element of the vector from left to right and return the result.
 * Starts from the given initial value.
 */
template<typename T>
auto foldr(const T& initial, std::function<T(const T&, const T&)> function, 
           std::vector<T> list) -> T
{
    T result = initial;
    for (const auto& element : list) {
        result = result + function(result, element);
    }
    return result;
}

/**
 * @brief Perform the given function on each element of the vector from right to left and return the result.
 * Starts from the given initial value.
 */
template<typename T>
auto foldl(const T& initial, std::function<T(const T&, const T&)> function, 
           std::vector<T> list) -> T
{
    T result = initial;
    for (auto i = list.rbegin(); i != list.rend(); ++i) {
        result = result + function(result, list[i]);
    }
    return result;
}

/**
 * @brief Apply foldr on the vector, with the first element as our initial value.
 */
template<typename T>
auto reduce(std::function<T(const T&, const T&)> function, std::vector<T> list) -> T
{
    return foldr(list[0], function, list);
}

}