#pragma once

#include <vector>
#include <type_traits>
#include <functional>
#include <string>

namespace util
{

// Forward declare
template <typename Strict_ID, typename Strict_ID_Tag>
class Unique_ID_Factory; 

/**
 * @brief Base ID Type to be used for simple, strongly typed IDs.
 *
 * To create a new ID type, create an empty Tag struct and alias your specialization.
 *
 * # Example
 * ```
 * struct UID_Tag;
 * using UID = ID_Base<UID_Tag>;
 * ```
 *
 * In this way, we guarantee each ID may only interact those of an equal type, while 
 * disallowing overrides that may interfere with the intended design.
 *
 * If you need something else, use something else. 
 */
template <typename Tag>
class ID_Base final
{

friend Unique_ID_Factory<ID_Base<Tag>, Tag>; // Allow access to protected functions.

public: 
    /// Default constructor (value 0)
    ID_Base() : _raw(0) {};

    /// ID Constructor from fundamental integer.
    explicit ID_Base(unsigned int raw) : _raw(raw) {};

    auto raw() const -> unsigned int { return _raw; };
    auto as_string() const -> std::string { return std::to_string(_raw); };

    /**
     * @brief Basic quality operator. Returns true if the IDs are identical.
     */
    auto operator == (const ID_Base<Tag> other) const -> bool 
    {
        return (_raw == other._raw);
    }

    /**
     * @brief Basic inquality operator. Returns false if the IDs are identical.
     */
    auto operator != (const ID_Base<Tag> other) const -> bool 
    {
        return (_raw == other._raw);
    } 

    struct hash 
    {
        auto operator ()(const ID_Base<Tag>& id) const -> std::size_t
        {
            return std::hash<unsigned int>{}(id._raw);
        }
    };     

protected:
    /**
     * @brief
     *
     * This function is protected as it has absolutely no meaning outside of the ID Factory.
     */
    auto operator < (const ID_Base<Tag> other) -> bool {
        return _raw < other._raw;
    };

private:
    unsigned int _raw; ///< Underlying value of ID.
};

/**
 * @brief A generic Factory class to generate specialized ID_Base IDs.
 */
template <typename Strict_ID, typename Strict_ID_Tag>
class Unique_ID_Factory
{
public:
    /**
     * @brief Create an empty factory, starting all IDs from 0.
     */
    Unique_ID_Factory() : _max_id(0) {
        static_assert(std::is_same<Strict_ID, ID_Base<Strict_ID_Tag>>::value, 
                      "Strict_ID is not an alias for the given ID_Base specialization.");
    };

    /**
     * @brief Create a factory from an existing sparsity list and max id.  
     *
     * The sparsity_list provided need not be sorted, and may even contain elements above
     * the max_id; it will be optimized internally.
     */
    Unique_ID_Factory(Strict_ID max_id, std::vector<Strict_ID> sparsity_list)
            : _max_id(max_id) {
        static_assert(std::is_same<Strict_ID, ID_Base<Strict_ID_Tag>>::value, 
                      "Strict_ID is not an alias for the given ID_Base specialization.");
        // Sort, trim duplicates, trim erraneous.
        sparsity_list.sort();
        sparsity_list.unique();
        sparsity_list.remove_if( [max_id](const Strict_ID& n) { return (n.raw() > max_id); } );
    };

    /**
     * @brief Create a factory from a list of used IDs. 
     */
    Unique_ID_Factory(std::vector<Strict_ID> utilized_list) {
        static_assert(std::is_same<Strict_ID, ID_Base<Strict_ID_Tag>>::value, 
                      "Strict_ID is not an alias for the given ID_Base specialization.");
        // Find our highest used id.
        unsigned int max_id = 0;
        for (const auto& id : utilized_list) {
            if (id.raw() > max_id) {
                max_id = id.raw();
            } 
        }

        // For each id up to the max, add a non-existant ID to the sparsity list.
        for (unsigned int id = 0; id < max_id; ++id) {
            bool found = false;
            for (auto it = utilized_list.begin(); it != utilized_list.end(); ++it) {
                if ((*it).raw() == id) {
                    found = true;
                    utilized_list.erase(it);
                    break;
                }
            }

            if (!found) {
                _sparsity_list.emplace_back(id);
            }
        }
    };

    /**
     * @brief Generates the next available unique ID. 
     */
    auto generate_id() -> Strict_ID 
    {
        if (!_sparsity_list.empty()) {
            auto back = *_sparsity_list.end();
            _sparsity_list.pop_back();
            return Strict_ID(back);
        } else {
            return Strict_ID(_max_id++);
        }
    }

    /**
     * @brief Frees the given ID, allowing it to be generated again in the future.
     */
    auto free_id(Strict_ID id) -> void
    {
        auto raw = id.raw();
        if (raw > _max_id) {
            return;
        }
    
        bool found_spot = false;
        for (auto i = _sparsity_list.begin(); i != _sparsity_list.end(); ++i) {
            auto free_id = *i;

            if        (free_id == raw) {
                return;
            } else if (free_id < raw) {
                _sparsity_list.insert(--i, raw);
                found_spot = true;
            }
        }
        if (!found_spot) {
            _sparsity_list.push_back(raw);
        }
    }

    /**
     * @brief Returns a list of free IDs lower than our max id.
     */
    auto sparsity_list() const -> std::vector<Strict_ID> { return _sparsity_list; }

    /**
     * @brief Returns the highest assigned ID so far.
     */
    auto max_id() const -> Strict_ID { return _max_id; }

private:
    std::vector<unsigned int> _sparsity_list; ///< Sorted list of free IDs up to our _max_id.
    unsigned int _max_id;                   ///< Highest used ID.
};

}