#include "tester/unit-tester.hpp"

#include <iostream>
#include <fstream>

namespace // Compilation unit internal namespace.
{
const char separator = '.';                  ///< Character that separates test name from result. 
const std::string color_code_prefix = "\e["; ///< Prefix for ASCII term color code.
const std::string color_code_suffix = "m";   ///< Suffix for ASCII term color code.
const std::string color_green = "32";        ///< Term color code for green.
const std::string color_red   = "31";        ///< Term color code for red.
const std::string color_default = "39";      ///< Term color code to reset to terminal default.

/**
 * @brief Print test section name.
 */
auto print_section(std::string name) -> void
{
    std::cout << name << std::endl;
}

/**
 * @brief Print test section name in Markdown format.
 */
auto print_md_section(std::string name) -> void
{
    std::cout << "# " << name << std::endl;
}

/**
 * @brief Print test name.
 */
auto print_test(std::string name, int longest_name) -> void
{
    std::cout << "\t" << name << std::string(longest_name - name.length() + 3, separator);
}

/**
 * @brief Print test name in Markdown format.
 */
auto print_md_test(std::string name) -> void
{
    std::cout << "## " << name << std::endl;
}

/**
 * @brief Print result of our test.
 */
auto print_result(bool success) -> void
{
    std::cout << (color_code_prefix + 
                  ((success) ? color_green : color_red) + 
                  color_code_suffix);
    std::cout << ((success) ? "Passed" : "Failed") << std::endl;
    std::cout << (color_code_prefix + 
                  color_default + 
                  color_code_suffix);
}

/**
 * @brief Print Markdown code block delimiters.
 * 
 * @param exit True if this is an exiting delimiter, so we may ensure a newline.
 */
auto print_md_block_delimiter(bool exit = false) -> void
{
    std::cout << ((exit) ? "\n" : "") << "..." << std::endl;
}

const auto std_out = std::cout.rdbuf(); ///< Terminal output's std::streambuf object.
}

namespace tester
{

Unit_Tester::Unit_Tester(std::string test_suite_name)
    : _name(test_suite_name)
{
    _longest_name = 0;
    _count = 0;
    _cout_target = "";
    _out_stream = nullptr;
}


Unit_Tester::~Unit_Tester()
{
    if (_out_stream != nullptr) {
        _out_stream->close();
        delete _out_stream;
        _out_stream = nullptr;
    }
}


auto Unit_Tester::add_test(std::string section, std::string name, Test test) -> void
{
    // Check for any overwriting.
    if (_tests.count(section) > 0 && _tests[section].count(name) > 0) {
        std::cout << "Uh oh! You're overwriting a previous test: [" 
                  << section << ":" << name << "]" << std::endl;
    } else {
        _longest_name = (static_cast<int>(name.length()) > _longest_name) 
        ? name.length() : _longest_name;
        _count += 1;
    }
    _tests[section][name] = test;
}

auto Unit_Tester::run_tests() -> Result_Map
{
    Result_Map results{};
    auto passed = 0;

    std::cout << std::string(17 + _name.length(), '*') << std::endl;
    std::cout << std::string(3, '*');
    std::cout << _name << " Unit Tests";
    std::cout << std::string(3, '*') << std::endl;
    std::cout << std::string(17 + _name.length(), '*') << std::endl;
    std::cout << "Running " << _count << " tests:" << std::endl << std::endl;

    for (auto&& section_pair : _tests) {           // For each test section 
        auto section_name = section_pair.first;
        auto section = section_pair.second;

        print_section(section_name);               // Print which section we're in
        redirect_cout();                           // And once more for possible cout file target.
        print_md_section(section_name);
        reset_cout();
        for (auto&& test_pair : section) {         // And for each test within this section
            auto test_name = test_pair.first;
            auto test = test_pair.second;

            print_test(test_name, _longest_name);  // Print the test name
            redirect_cout();                       // Set up our redirect.
            print_md_test(test_name);
            print_md_block_delimiter();            // Place test output within code block.
            auto success = test();                 // Run our test.
            print_md_block_delimiter(true);        // Escape code block.
            std::cout << "Test " << ((success) ? "Passed" : "Failed" ) << "\n\n";
            std::cout.flush(); 
            reset_cout();                          // And turn cout back on.
            results[section_name][test_name] = success;
            if (success) passed++;                 // Keep track of how many tests passed.
            print_result(success);                 // And print the results out.
        }
    }

    std::cout << std::endl << "Finished!" << std::endl;
    std::cout << passed << "/" << _count << " \e[32mPassed.\e[39m" << std::endl;
    std::cout << (_count - passed) << "/" << _count << " \e[31mFailed.\e[39m" << std::endl; 
    
    if (_cout_target != "") {
        std::cout << "Output written to " << _cout_target << std::endl;
    }
    
    return results;
}

auto Unit_Tester::set_cout_target(std::string filename) -> bool
{
    _cout_target = filename + ".md";
    if (_out_stream != nullptr) {
            _out_stream->close();
            delete _out_stream;
        }
    if (!filename.empty()) {
        _out_stream = new std::ofstream(_cout_target);
        _out_stream->copyfmt(std::cout); // Copy cout formatting rules.
        if (!_out_stream->is_open()) {
            _cout_target = "";   // Empty our target, for consistency's sake.
            _out_stream->close();
            delete _out_stream;
            return false;
        }
    }
    return true;
}

auto Unit_Tester::clear_cout_target() -> void
{
    _cout_target = "";
    if (_out_stream != nullptr) {
        _out_stream->close();
        delete _out_stream;
        _out_stream = nullptr;
    }
}

auto Unit_Tester::redirect_cout() -> void
{
    if (_cout_target.empty()) {
        std::cout.setstate(std::ios::failbit);
    } else {
        std::cout.rdbuf(_out_stream->rdbuf());
    }
}

auto Unit_Tester::reset_cout() -> void
{
    std::cout.setstate(std::ios::goodbit);
    std::cout.rdbuf(std_out);
}

}
