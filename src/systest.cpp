#include "util/logger.hpp"
#include "admin/admin_db.hpp"
#include "util/functional.hpp"

#include <iostream>
#include <memory>

auto main() -> int {
    std::cout << "Howdy!" << std::endl;

    util::Logger log("systest.log");

    log << "Initializing Administration Database";
    admin::Admin_DB adminsys("Database");
    log << "Loading contents from database files.";
    if (!adminsys.load()) {
        log << Log_Level::Error << "Failed to load database, exiting!";
        exit(EXIT_FAILURE);
    }

    // Print information for a Student - Kenneth Sills
    log << "Printing information for Student named Kenneth Sills";
    std::cout << "Printing information for Student named Kenneth Sills" << std::endl;
    func::apply<std::weak_ptr<admin::Student>>(
        [](std::weak_ptr<admin::Student>& student_ref) -> void {
            auto student = *student_ref.lock();
            auto birthday = student.birth_date();

            std::cout << "[Student]" << std::endl;
            std::cout << "Name      : " << student.first_name() << " " << 
                                          student.last_name() << std::endl;
            std::cout << "UID       : " << student.id().as_string() << std::endl;
            std::cout << "Birth Day : " << std::to_string(birthday.year()) << "/" <<
                                          std::to_string(birthday.month()) << "/" <<
                                          std::to_string(birthday.day()) << std::endl;
            std::cout << "Gender    : " << ((student.gender() == admin::Gender::Male) ? 
                                              "Male" : "Female") << 
                                          std::endl;
            std::cout << "Department: " << student.department().as_string() << std::endl;
            std::cout << "Active?   : " << ((student.active()) ? "True" : "False") << 
                                          std::endl << std::endl;
        }, func::filter<std::weak_ptr<admin::Student>>(
            [](const std::weak_ptr<admin::Student>& student_ref) -> bool {
                auto student = *student_ref.lock();
                if (student.first_name() == "Kenneth" && student.last_name() == "Sills") {
                    return true;
                }
                return false;
            }, adminsys.students()));

    // Print information for a Teacher - Largo Rubio
    log << "Printing information for Teacher named Largo Rhubio";
    std::cout << "Printing information for Teacher named Largo Rhubio" << std::endl;
    func::apply<std::weak_ptr<admin::Teacher>>(
        [](std::weak_ptr<admin::Teacher>& teacher_ref) -> void {
            auto teacher = *teacher_ref.lock();
            auto birthday = teacher.birth_date();

            std::cout << "[Teacher]" << std::endl;
            std::cout << "Name      : " << teacher.first_name() << " " << 
                                          teacher.last_name() << std::endl;
            std::cout << "UID       : " << teacher.id().as_string() << std::endl;
            std::cout << "Birth Day : " << std::to_string(birthday.year()) << "/" <<
                                          std::to_string(birthday.month()) << "/" <<
                                          std::to_string(birthday.day()) << std::endl;
            std::cout << "Gender    : " << ((teacher.gender() == admin::Gender::Male) ? 
                                              "Male" : "Female") << 
                                          std::endl;
            std::cout << "Department: " << teacher.department().as_string() << std::endl;
            std::cout << "Active?   : " << ((teacher.active()) ? "True" : "False") << 
                                          std::endl << std::endl;
        }, func::filter<std::weak_ptr<admin::Teacher>>(
            [](const std::weak_ptr<admin::Teacher>& teacher_ref) -> bool {
                auto teacher = *teacher_ref.lock();
                if (teacher.first_name() == "Largo" && teacher.last_name() == "Rhubio") {
                    return true;
                }
                return false;
            }, adminsys.teachers()));

    // Print Information for all course sections for courses named Engineering_Calculus
    // Expand each person to include ID & Name.
    log << "Printing information of all courses of Engineering_Calculus";
    std::cout << "Printing information of all courses of Engineering_Calculus" << std::endl;
    func::apply<std::weak_ptr<admin::Course_Section>>(
        // Print data.
        [&](std::weak_ptr<admin::Course_Section>& course) {
            auto section = *course.lock();
            
            std::cout << "[Course]" << std::endl;
            std::cout << "SID       : " << section.id().as_string() << std::endl;
            std::cout << "Definition: " << section.definition().as_string() << std::endl;
            std::cout << "Seats     : " << std::to_string(section.total_seats()) << std::endl;
            std::cout << "Active?   : " << ((section.is_active()) ? "True" : "False")<< std::endl;
            std::cout << "Teachers: " << std::endl;
            func::apply<admin::UID>(
                [&](admin::UID& teacher) {
                    auto teacher_ref = *adminsys.teacher(teacher).access().lock();
                    std::cout << "  " << teacher_ref.id().as_string();
                    std::cout << " - " << teacher_ref.first_name() << " " <<
                                              teacher_ref.last_name() << std::endl;
                }, section.teachers());
            
            std::cout << "Assistants: " << std::endl;
            func::apply<admin::UID>(
                [&](admin::UID& student) {
                    auto student_ref = *adminsys.student(student).access().lock();
                    std::cout << "  " << student_ref.id().as_string();
                    std::cout << " - " << student_ref.first_name() << " " <<
                                              student_ref.last_name() << std::endl;
                }, section.assistants());

            std::cout << "Students: " << std::endl;
            func::apply<admin::UID>(
                [&](admin::UID& student) {
                    auto student_ref = *adminsys.student(student).access().lock();
                    std::cout << "  " << student_ref.id().as_string();
                    std::cout << " - " << student_ref.first_name() << " " <<
                                              student_ref.last_name() << std::endl;
                }, section.students()); 
            std::cout << std::endl;

        }, func::remap<std::weak_ptr<admin::Course_Definition>, std::weak_ptr<admin::Course_Section>>(
            // Get all sections for this definition.
            [&](const std::weak_ptr<admin::Course_Definition>& course_def) {
                return
                func::filter<std::weak_ptr<admin::Course_Section>>(
                    // Filter to sections of this definition.
                    [&](const std::weak_ptr<admin::Course_Section>& course_ref) {
                        if (course_ref.lock()->definition() == course_def.lock()->id()) {
                            return true;
                        }
                        return false;
                    }, adminsys.courses());

            }, func::filter<std::weak_ptr<admin::Course_Definition>>(
                // Filter to definitions with what name we want.
                [](const std::weak_ptr<admin::Course_Definition>& course_def) {
                    auto definition = *course_def.lock();
                    if (definition.name() == "Engineering_Calculus") {
                        return true;
                    }
                    return false;
                }, adminsys.course_defs())));

    // Print Course Names a Student is enrolled in
    log << "Printing data for all courses Stacy's Mom is enrolled in.";
    std::cout << "Printing data for all courses Stacy's Mom is enrolled in." << std::endl;
    func::apply<std::weak_ptr<admin::Student>>(
        [&](std::weak_ptr<admin::Student>& student_ref) {
            auto student = *student_ref.lock();

            std::cout << "[Student - " << student.id().as_string() << "]" << std::endl;
            std::cout << "Current Courses:" << std::endl;
            // Print section details
            func::apply<admin::Course_Section>(
                [&](admin::Course_Section& course){
                    auto definition = *adminsys.course_def(course.definition()).access().lock();
                    std::cout << "  [Section - " << course.id().as_string() << "]" << std::endl;
                    std::cout << "  Name       : " << definition.name() << std::endl;
                    std::cout << "  Department : " << definition.department().as_string() << 
                                                      std::endl;
                    std::cout << "  Seats      : " << std::to_string(course.total_seats()) << 
                                                      std::endl;
                    std::cout << "  GPA        : " << std::to_string(course.gpa(student.id()).access()) << 
                                                      std::endl;
                    std::cout << "  Retired?   : " << ((definition.retired()) ? "True" : "False") << 
                                                      std::endl;
                    std::cout << "  Active?    : " << ((course.is_active()) ? "True" : "False") << 
                                                      std::endl << std::endl;

                }, func::map<admin::SID, admin::Course_Section>(
                    [&](const admin::SID& course_ref) {
                        return *adminsys.course(course_ref).access().lock();
                    }, student.enrolled_courses()));
            

        }, func::filter<std::weak_ptr<admin::Student>>(
            [](const std::weak_ptr<admin::Student>& student_ref) {
                auto student = *student_ref.lock();
                if (student.first_name() == "Stacy" && student.last_name() == "Mom") {
                    return true;
                }
                return false;
            }, adminsys.students()));
    
    // Department
    log << "Describing Department Ballet and it's consituent People & Research'";
    std::cout << "Describing Department Ballet and it's constituent People & Research" << std::endl;
    func::apply<std::weak_ptr<admin::Department>>(
        [&](std::weak_ptr<admin::Department>& department_ref) {
            auto department = *department_ref.lock();
            std::cout << "[Department - " << department.id().as_string() << "]" << std::endl;
            std::cout << "Students:" << std::endl;
            func::apply<admin::UID>(
                [&](admin::UID& student_id){
                    auto student = *adminsys.student(student_id).access().lock();
                    std::cout << "  " << student.id().as_string() << 
                                 " - " << student.first_name() << 
                                 " " << student.last_name() << std::endl;
                }, department.students());

            std::cout << "Faculty:" << std::endl;
            func::apply<admin::UID>(
                [&](admin::UID& teacher_id){
                    auto teacher = *adminsys.teacher(teacher_id).access().lock();
                    std::cout << "  " << teacher.id().as_string() << 
                                 " - " << teacher.first_name() << 
                                 " " << teacher.last_name() << std::endl;
                }, department.teachers());

            std::cout << "Research:" << std::endl;
            func::apply<admin::Research_Project>(
                [](admin::Research_Project& project) {
                    std::cout << "  " << project.id().as_string() <<
                                 " - " << project.name() << " is ";
                    switch (project.status()) {
                    case admin::Research_Status::Ongoing:
                        std::cout << "Ongoing"; 
                        break;
                    case admin::Research_Status::Complete:
                        std::cout << "Complete";
                        break;
                    case admin::Research_Status::Cancelled:
                        std::cout << "Cancelled";
                        break;
                    }
                    std::cout << std::endl;
                    
                }, func::map<admin::RID, admin::Research_Project>(
                    [&](const admin::RID project_id) {
                        return *adminsys.research(project_id).access().lock();
                    }, department.research()));
            std::cout << std::endl;

        }, func::filter<std::weak_ptr<admin::Department>>(
            [](const std::weak_ptr<admin::Department>& department_ref) {
                if (department_ref.lock()->name() == "Ballet") {
                    return true;
                }
                return false;
            }, adminsys.departments()));

    log << "Saving contents to database files.";
    if (!adminsys.save()) {
        log << Log_Level::Error << "Failed to save database! Contents may be corrupt!";
        exit(EXIT_FAILURE);
    }
}
