#include "util/logger.hpp"

#include <string>
#include <iostream>
#include <iomanip>

namespace util
{

Logger::Logger(std::string filename)
    : _current_level(Log_Level::Info)
{
    std::cout << "Opening " + filename << std::endl;
    _handle = new std::fstream(filename, std::fstream::app);
    if (!_handle->is_open()) {
        std::cout << "Can't open logging file. Panic!" << std::endl;
        throw new std::exception();
    }

    *_handle << std::endl << std::string(60, '*') << std::endl;
    auto current_time = std::time(nullptr);
    *_handle << "Logger Fresh Boot for " << filename << " at ";
    *_handle << std::put_time(std::localtime(&current_time), "%F %T") << std::endl;
    *_handle << std::string(60, '*') << std::endl << std::endl;
}

Logger::~Logger()
{
    _handle->flush();
    _handle->close();
    delete _handle;
}

auto Logger::set_info_post(std::string post) -> void
{
    _info_post = post;
}

auto Logger::set_warning_post(std::string post) -> void
{
    _warning_post = post;
}

auto Logger::set_error_post(std::string post) -> void
{
    _error_post = post;
}

auto Logger::log(Log_Level level, std::string message) -> void
{
    auto current_time = std::time(nullptr);
    *_handle << std::put_time(std::localtime(&current_time), "%F %T");
    *_handle << " : [";

    switch (level) {
    case Log_Level::Info:
        *_handle << _info_post;
        break;
    case Log_Level::Warning:
        *_handle << _warning_post;
        break;
    case Log_Level::Error:
        *_handle << _error_post;
        break;
    }
    *_handle << "] : ";
    *_handle << message;
    *_handle << "\n";
    _handle->flush();
}

auto Logger::flush() -> void
{
    _handle->flush();
}

auto Logger::operator<<(std::string message) -> Logger&
{
    log(_current_level, message);
    return *this;
}

auto Logger::operator<<(Log_Level level) -> Logger&
{
    _current_level = level;
    return *this;
}

}