#include "admin/teacher.hpp"

#include "admin/admin_db.hpp"

namespace admin
{

Teacher::Teacher(Registered_Person person, Teacher_Kind employment_kind)
        : Registered_Person(person), _kind(employment_kind),
          _current_courses(), _completed_courses(), _current_research(), _completed_research()
{
}

Teacher::Teacher(Registered_Person person, Teacher_Kind employment_kind,
                 std::vector<SID> current_courses, std::vector<SID> completed_courses,
                 std::vector<RID> current_research, std::vector<RID> completed_research)
            : Registered_Person(person), _kind(employment_kind),
              _current_courses(current_courses), _completed_courses(completed_courses),
              _current_research(current_research), _completed_research(completed_research)
{
}

auto Teacher::from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Teacher>
{
    adminsys->log("Attempting to parse Teacher from SDF_Node.");

    Registered_Person* base_ptr = nullptr; // See below for details
    bool base_found = false;
    Teacher_Kind kind = Teacher_Kind::Professor;
    bool kind_found = false;
    std::vector<SID> current_courses;
    bool current_courses_found = false;
    std::vector<SID> completed_courses;
    bool completed_courses_found = false;
    std::vector<RID> current_research;
    bool current_research_found = false;
    std::vector<RID> completed_research;
    bool completed_research_found = false;

    for (const auto& node : branch) {
        if (node.name() == "Registered_Person") {
            adminsys->log("Found Registered_Person.");
            if (node.kind() == sdf::SDF_Node_Type::Value) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.'");
                return util::Maybe<Teacher>();
            } else {
                auto child = Registered_Person::from_sdf(adminsys, node.branch().access());
                if (child.exists()) {
                    base_ptr = new Registered_Person(child.access());
                    base_found = true;
                } else {
                    adminsys->log(Log_Level::Error, "But it's ill-formed.'");
                    return util::Maybe<Teacher>();
                }
            }
        }
        else if (node.name() == "kind") {
            adminsys->log("Found kind.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.'");
                return util::Maybe<Teacher>();
            } else {
                auto node_string = node.as_string().access();
                if (node_string == "lecturer") {
                    kind = Teacher_Kind::Lecturer;
                    kind_found = true;
                } 
                else if (node_string == "adjunct") {
                    kind = Teacher_Kind::Adjunct;
                    kind_found = true;
                }
                else if (node_string == "professor") {
                    kind = Teacher_Kind::Professor;
                    kind_found = true;
                }
                else {
                    adminsys->log(Log_Level::Error, "But it's ill-formed.'");
                    return util::Maybe<Teacher>();
                }
            }
        }
        else if (node.name() == "current_courses") {
            adminsys->log("Found current_courses.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Teacher>();
            } else {
                current_courses = parse_id_list<SID>(node.as_string().access());
                current_courses_found = true;
            }
        }
        else if (node.name() == "completed_courses") {
            adminsys->log("Found completed_courses.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Teacher>();
            } else {
                completed_courses = parse_id_list<SID>(node.as_string().access());
                completed_courses_found = true;
            }
        }
        else if (node.name() == "current_research") {
            adminsys->log("Found current_research.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Teacher>();
            } else {
                current_research = parse_id_list<RID>(node.as_string().access());
                current_research_found = true;
            }
        }
        else if (node.name() == "completed_research") {
            adminsys->log("Found completed_research.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Teacher>();
            } else {
                completed_research = parse_id_list<RID>(node.as_string().access());
                completed_research_found = true;
            }
        }
        else {
            adminsys->log(Log_Level::Warning, "Unknown entry " + node.name() + ". Skipping.");
        }
    }

    // Since a default assignment operator would be ill-formed (const adminsys member)
    // I create a copy of the base into a ptr, then copy that into a stack object.
    // Makes two copies, one of which is on the heap, but that's fine for now.
    Registered_Person base{*base_ptr};
    delete base_ptr;

    // TODO
    if (!base_found || !kind_found || !current_courses_found || !completed_courses_found ||
            !current_research_found || !completed_research_found) {
        adminsys->log(Log_Level::Error, "Missing necessary entry for Teacher.");
        return util::Maybe<Teacher>();
    }
    adminsys->log("Successfully parsed Teacher.");
    return util::Maybe<Teacher>(Teacher(base, kind, current_courses, completed_courses, 
                                        current_research, completed_research));
}

auto Teacher::to_sdf(const Teacher& teacher) -> sdf::SDF_Node
{
    teacher._adminsys->log(teacher._id, "Generating Node from Teacher...");
    sdf::SDF_Branch result;

    result.push_back(Registered_Person::to_sdf(teacher));
    switch (teacher._kind) {
    case Teacher_Kind::Lecturer:
        result.emplace_back("kind", "lecturer");
        break;
    case Teacher_Kind::Adjunct:
        result.emplace_back("kind", "adjunct");
        break;
    case Teacher_Kind::Professor:
        result.emplace_back("kind", "professor");
        break;
    }
    result.emplace_back("current_courses", write_id_list(teacher._current_courses));
    result.emplace_back("completed_courses", write_id_list(teacher._completed_courses));
    result.emplace_back("current_research", write_id_list(teacher._current_research));
    result.emplace_back("completed_research", write_id_list(teacher._completed_research));

    teacher._adminsys->log(teacher._id, "Successfully generated Node from Teacher.");
    return sdf::SDF_Node("Teacher", result);
}

auto Teacher::kind() const -> Teacher_Kind
{
    return _kind;
}

auto Teacher::change_kind(Teacher_Kind employment_kind) -> void
{
    std::string log_output;
    switch (employment_kind) {
    case Teacher_Kind::Lecturer:
        log_output = "Lecturer";
        break;
    case Teacher_Kind::Adjunct:
        log_output = "Adjunct";
        break;
    case Teacher_Kind::Professor:
        log_output = "Professor";
        break;
    }

    _adminsys->log(_id, "Changing teacher kind to " + log_output);
    _kind = employment_kind;
    if (!can_research()) {
        _adminsys->log(_id, "New kind does not allow research. Purging list...");
        for (const auto& id : _current_research) {
            quit_research(id);
        }
    }
    _adminsys->log(_id, "Successfully changed teacher kind.");
}

auto Teacher::add_course(SID section) -> bool
{
    _adminsys->log(_id, "Attempting to register for course " + section.as_string() + "...");
    auto course_ref = _adminsys->course(section);
    if (course_ref.exists()) {
        return course_ref.access().lock()->assign_teacher(_id);
    }
    _adminsys->log(_id, Log_Level::Error, "Failed - No such course exists.");
    return false;
}

auto Teacher::quit_course(SID section) -> void
{
    _adminsys->log(_id, "Leaving course section " + section.as_string());
    auto course_ref = _adminsys->course(section);
    if (course_ref.exists()) {
        course_ref.access().lock()->remove_teacher(_id);
        return;
    }
    _adminsys->log(_id, Log_Level::Error, "Failed - No such course exists.");
}

auto Teacher::finalize_course(SID section) -> void
{
    _adminsys->log(_id, "Finalizing course section " + section.as_string() + "...");

    bool permissible = false;
    for (const auto id : _current_courses) {
        if (id == section) {
            permissible = true;
        }
    }
    if (!permissible) {
        _adminsys->log(_id, Log_Level::Error, "Failed - We don't have permission to do this.");
    }

    auto course_ref = _adminsys->course(section);
    if (course_ref.exists()) {
        course_ref.access().lock()->finalize_course();
    } else {
        _adminsys->log(_id, Log_Level::Error, "Failed - No such section found.");
    }
}

auto Teacher::add_research(RID project) -> bool
{
    _adminsys->log(_id, "Attempting to register for research " + project.as_string() + "...");
    auto research_ref = _adminsys->research(project);
    if (research_ref.exists()) {
        return research_ref.access().lock()->assign_teacher(_id);
    }
    _adminsys->log(_id, Log_Level::Error, "Failed - No such research exists.");
    return false;
}

auto Teacher::quit_research(RID project) -> void
{
    _adminsys->log(_id, "Leaving research project " + project.as_string() + "...");
    auto research_ref = _adminsys->research(project);
    if (research_ref.exists()) {
        research_ref.access().lock()->remove_teacher(_id);
        return;
    }
    _adminsys->log(_id, Log_Level::Error, "Failed - No such research exists.");
}

auto Teacher::finalize_research(RID project) -> void
{
    _adminsys->log(_id, "Finalizing research project " + project.as_string() + "...");

    bool permissible = false;
    for (const auto id : _current_research) {
        if (id == project) {
            permissible = true;
        }
    }
    if (!permissible) {
        _adminsys->log(_id, Log_Level::Error, "Failed - We don't have permission to do this.");
    }

    auto research_ref = _adminsys->research(project);
    if (research_ref.exists()) {
        research_ref.access().lock()->complete();
    } else {
        _adminsys->log(_id, Log_Level::Error, "Failed - No such project found.");
    }
}

auto Teacher::can_research() const -> bool
{
    switch (_kind) {
    case Teacher_Kind::Lecturer:
        return false;
        break;
    case Teacher_Kind::Adjunct:
        return true;
        break;
    case Teacher_Kind::Professor:
        return true;
        break;
    }
    return false;
}

auto Teacher::can_research(RID research) const -> bool
{
    if (!can_research()) 
        { return false; }
    auto project_ref = _adminsys->research(research);
    if (project_ref.exists()) {
        return project_ref.access().lock()->can_research(_id);
    }
    return false;
}

auto Teacher::current_courses() const -> std::vector<SID>
{
    return _current_courses;
}

auto Teacher::completed_courses() const -> std::vector<SID>
{
    return _completed_courses;
}

auto Teacher::current_research() const -> std::vector<RID>
{
    return _current_research;
}

auto Teacher::completed_research() const -> std::vector<RID>
{
    return _completed_research;
}

auto Teacher::is_teaching(SID section) const -> bool
{
    for (const auto& id : _current_courses) {
        if (id == section) {
            return true;
        }
    }
    return false;
}

auto Teacher::is_teaching(CID course) const -> bool
{
    for (const auto& id : _current_courses) {
        auto section_ref = _adminsys->course(id);
        if (section_ref.exists()) {
            auto definition = section_ref.access().lock()->definition();
            if (definition == course) {
                return true;
            } else {
                auto course_ref = _adminsys->course_def(definition);
                if (course_ref.exists()) {
                    if (course_ref.access().lock()->is_equivalent(course)) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

auto Teacher::has_taught(SID section) const -> bool
{
    for (const auto& id : _completed_courses) {
        if (id == section) {
            return true;
        }
    }
    return false;
}

auto Teacher::has_taught(CID course) const -> bool
{
    for (const auto& id : _completed_courses) {
        // Get instance
        auto section_ref = _adminsys->course(id);
        if (section_ref.exists()) {
            auto definition = section_ref.access().lock()->definition();
            if (definition == course) { // This course is exactly right
                return true;
            } else {                    // Check equivalencies
                auto course_ref = _adminsys->course_def(definition);
                if (course_ref.exists()) {
                    if (course_ref.access().lock()->is_equivalent(course)) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

auto Teacher::is_researching(RID project) const -> bool
{
    for (const auto& id : _current_research) {
        if (id == project) {
            return true;
        }
    }
    return false;
}

auto Teacher::has_researched(RID project) const -> bool
{
    for (const auto& id : _completed_research) {
        if (id == project) {
            return true;
        }
    }
    return false;
}

auto Teacher::deactivate() -> void
{
    _adminsys->log(_id, "Deactivating person...");
    Registered_Person::deactivate();

    _adminsys->log(_id, "Withdrawing from research projects...");
    for (auto& research : _current_research) {
        quit_research(research);
    }

    _adminsys->log(_id, "Withdrawing from enrolled courses...");
    for (auto& course : _current_courses) {
        quit_course(course);
    }

    _adminsys->log(_id, "Purge complete. Successfully deactivated.");
} 

auto Teacher::assign_course(SID section) -> void
{
    _adminsys->log(_id, "Assigning teacher to section " + section.as_string() + "...");
    for (const auto& id : _current_courses) {
        if (id == section) {
            _adminsys->log(_id, Log_Level::Warning, "Already assigned, skipping.");
            return;
        }
    }
     _current_courses.push_back(section);
}

auto Teacher::remove_course(SID section) -> void
{
    _adminsys->log(_id, "Removing section " + section.as_string());
    for (size_t i = 0; i < _current_courses.size(); i++) {
        if (_current_courses[i] == section) {
            _current_courses.erase(_current_courses.begin() + i);
            break;
        }
    }
    for (size_t i = 0; i < _completed_courses.size(); i++) {
        if (_completed_courses[i] == section) {
            _completed_courses.erase(_completed_courses.begin() + i);
            break;
        }
    }
}

auto Teacher::assign_research(RID project) -> void
{
    _adminsys->log(_id, "Assigning teacher to research " + project.as_string() + "...");
    for (const auto& id : _current_research) {
        if (id == project) {
            _adminsys->log(_id, Log_Level::Warning, "Already assigned, skipping.");
            return;
        }
    }
     _current_research.push_back(project);
}

auto Teacher::remove_research(RID project) -> void
{
    _adminsys->log(_id, "Removing project " + project.as_string());
    for (size_t i = 0; i < _current_research.size(); i++) {
        if (_current_research[i] == project) {
            _current_research.erase(_current_research.begin() + i);
            break;
        }
    }
    for (size_t i = 0; i < _completed_research.size(); i++) {
        if (_completed_research[i] == project) {
            _completed_research.erase(_completed_research.begin() + i);
            break;
        }
    }
}

auto Teacher::archive_course(SID section) -> void
{
    _adminsys->log(_id, "Archiving course section " + section.as_string());
    for (size_t i = 0; i < _current_courses.size(); ++i) {
        if (_current_courses[i] == section) {
            _current_courses.erase(_current_courses.begin() + i);
            break;
        }
    }
    _completed_courses.push_back(section);
}

auto Teacher::archive_research(RID project) -> void
{
    _adminsys->log(_id, "Archiving course section " + project.as_string());
    for (size_t i = 0; i < _current_research.size(); ++i) {
        if (_current_research[i] == project) {
            _current_research.erase(_current_research.begin() + i);
            break;
        }
    }
    _completed_research.push_back(project);
}


}