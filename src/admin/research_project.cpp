#include "admin/research_project.hpp"

#include "admin/admin_db.hpp"

namespace admin
{

Research_Project::Research_Project(Admin_DB* const adminsys, RID id, DID department, std::string name)
        : _adminsys(adminsys), _id(id), _department(department), _name(name),
          _teachers(), _assistants(), _status(Research_Status::Ongoing)
{
}
Research_Project::Research_Project(Admin_DB* const adminsys, RID id, DID department, std::string name,
            std::vector<UID> teachers, std::vector<UID> assistants,
            Research_Status status)
        : _adminsys(adminsys), _id(id), _department(department), _name(name), 
          _teachers(teachers), _assistants(assistants), _status(status)
{
}

auto Research_Project::from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Research_Project>
{
    adminsys->log("Attempting read of Research_Project from SDF_Node.");

    RID id;
    bool id_found = false;
    DID department;
    bool department_found = false;
    std::string name;
    bool name_found = false;
    std::vector<UID> teachers;
    bool teachers_found = false;
    std::vector<UID> assistants;
    bool assistants_found = false;
    Research_Status status = Research_Status::Complete;
    bool status_found = false;

    for (const auto& node : branch) {
        if (node.name() == "id") {
            auto node_int = node.as_int();
            if (!node_int.exists()) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Research_Project>();
            }
            id = RID(node_int.access());
            id_found = true;
        }
        else if (node.name() == "department") {
            auto node_int = node.as_int();
            if (!node_int.exists()) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Research_Project>();
            }
            department = DID(node_int.access());
            department_found = true;
        }
        else if (node.name() == "name") {
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Research_Project>();
            }
            name = node.as_string().access();
            name_found = true;
        }
        else if (node.name() == "teachers") {
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Research_Project>();
            }
            teachers = parse_id_list<UID>(node.as_string().access());
            teachers_found = true;
        }
        else if (node.name() == "assistants") {
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Research_Project>();
            }
            assistants = parse_id_list<UID>(node.as_string().access());
            assistants_found = true;
        }
        else if (node.name() == "status") {
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                return util::Maybe<Research_Project>();
            }
            auto node_string = node.as_string().access();
            if (node_string == "ongoing") {
                status = Research_Status::Ongoing;
            }
            else if (node_string == "complete") {
                status = Research_Status::Complete;
            }
            else if (node_string == "cancelled") {
                status = Research_Status::Cancelled;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.'");
                return util::Maybe<Research_Project>();
            }
            status_found = true;
        } else {
            adminsys->log(Log_Level::Warning, "Unknown entry " + node.name() + ". Skipping.");
        }
    }

    if (!id_found || !department_found || !name_found || 
        !teachers_found || !assistants_found || !status_found) {
        adminsys->log(Log_Level::Error, "Missing necessary entry for Research_Project.");
        return util::Maybe<Research_Project>();
    }
    adminsys->log("Successfully parsed Research_Project.");
    return util::Maybe<Research_Project>(true, 
                                         Research_Project(adminsys, id, department, name,
                                                          teachers, assistants, status));
}
auto Research_Project::to_sdf(const Research_Project& research) -> sdf::SDF_Node
{
    research._adminsys->log("Writing research " + research._id.as_string() + " to SDF_Node.");

    sdf::SDF_Branch result;

    result.emplace_back("id", std::to_string(research._id.raw()));
    result.emplace_back("department", std::to_string(research._department.raw()));
    result.emplace_back("name", research._name);
    result.emplace_back("teachers", write_id_list<UID>(research._teachers));
    result.emplace_back("assistants", write_id_list<UID>(research._assistants));
    
    switch (research._status) {
    case Research_Status::Ongoing:
        result.emplace_back("status", "ongoing");
        break;
    case Research_Status::Complete:
        result.emplace_back("status", "complete");
        break;
    case Research_Status::Cancelled:
        result.emplace_back("status", "cancelled");
        break;
    }

    research._adminsys->log("Write for research " + research._id.as_string() + " success.");
    return sdf::SDF_Node("Research_Project", result);
}

auto Research_Project::id() const -> RID
{
    return _id;
}
auto Research_Project::department() const -> DID
{
    return _department;
}
auto Research_Project::name() const -> std::string
{
    return _name;
}
auto Research_Project::completed() const -> bool
{
    return (_status == Research_Status::Complete);
}
auto Research_Project::cancelled() const -> bool
{
    return (_status == Research_Status::Cancelled);
}

auto Research_Project::status() const -> Research_Status
{
    return _status;
}

auto Research_Project::teachers() const -> std::vector<UID>
{
    return _teachers;
}
auto Research_Project::assistants() const -> std::vector<UID>
{
    return _assistants;
}

auto Research_Project::can_research(UID person) const -> bool
{
    // If person is student
    auto student_ref = _adminsys->student(person);
    if (student_ref.exists()) {
        return student_ref.access().lock()->can_assist();
    }

    // If person is teacher
    auto teacher_ref = _adminsys->teacher(person);
    if (teacher_ref.exists()) {
        return teacher_ref.access().lock()->can_research();
    }

    return false;
}
auto Research_Project::is_assigned(UID person) const -> bool
{
    // Check if person is teacher
    for (const auto& id : _teachers) {
        if (id == person) {
            return true;
        }
    }

    // Check if person is student
    for (const auto& id : _assistants) {
        if (id == person) {
            return true;
        }
    }

    return false; // No one found.
}

auto Research_Project::assign_teacher(UID teacher) -> bool
{
    _adminsys->log(_id, "Attempting to assign teacher " + teacher.as_string());

    if (_status != Research_Status::Ongoing) {
        _adminsys->log(_id, Log_Level::Error, "Failed - Research project has been closed.");
        return false;
    }

    // Do we already have this teacher assigned?
    for (const auto& id : _teachers) {
        if (id == teacher) {
            _adminsys->log(_id, Log_Level::Warning, "Teacher already assigned. Skipping.");
            return true;
        }
    }

    // Get actual teacher instance.
    auto teacher_ref = _adminsys->teacher(teacher);
    if (teacher_ref.exists()) {
        // But first make sure they're elligible
        if (!can_research(teacher)) {
            _adminsys->log(_id, Log_Level::Error, "Teacher inelligible for research.");
            return false;
        }
        teacher_ref.access().lock()->assign_research(_id);
        _teachers.push_back(teacher);
        _adminsys->log(_id, "Successfully assigned teacher " + teacher.as_string());
        return true;
    } else {
        _adminsys->log(_id, Log_Level::Error, "No such teacher " + teacher.as_string());
        return false;
    }
}
auto Research_Project::remove_teacher(UID teacher) -> void
{
    _adminsys->log(_id, "Removing teacher "  + teacher.as_string());

    // Get actual teacher instance.
    auto teacher_ref = _adminsys->teacher(teacher);
    if (teacher_ref.exists()) {

        // Remove any instance of the teacher from our list, and request teacher to remove this.
        for (size_t i = 0; i < _teachers.size(); ++i) {
            if (_teachers[i] == teacher) {
                teacher_ref.access().lock()->remove_research(_id);
                _teachers.erase(_teachers.begin() + i);
                _adminsys->log(_id, "Successfully removed Teacher.");
                return;
            }
        }
    }
    _adminsys->log(_id, Log_Level::Error, "No such teacher " + teacher.as_string());
}
auto Research_Project::assign_assistant(UID student) -> bool
{
    _adminsys->log(_id, "Attempting to assign teacher " + student.as_string());
    if (_status != Research_Status::Ongoing) {
        _adminsys->log(_id, Log_Level::Error, "Failed - Research project has been closed.");
        return false;
    }

    for (const auto& id : _assistants) {
        if (id == student) {
            _adminsys->log(_id, Log_Level::Warning, "Student already assigning. Skipping.");
            return true;
        }
    }

    // Get actual student instance.
    auto assistant_ref = _adminsys->student(student);
    if (assistant_ref.exists()) {    
        // But first make sure they're elligible
        if (!can_research(student)) {
            _adminsys->log(_id, Log_Level::Error, "Student illegible to assist with research.");
            return false;
        }

        assistant_ref.access().lock()->assign_as_assistant(_id);
        _assistants.push_back(student);
        _adminsys->log(_id, "Successfully assigned student.");
        return true;
    } else {
        _adminsys->log(_id, Log_Level::Error, "No such student.");
        return false;
    }
}
auto Research_Project::remove_assistant(UID student) -> void
{
    _adminsys->log(_id, "Removing student "  + student.as_string());

    // Get actual student instance.
    auto assistant_ref = _adminsys->student(student);
    if (assistant_ref.exists()) {
        // Remove any instance of the assistant from our list, and request assistant to remove this.
        for (size_t i = 0; i < _assistants.size(); ++i) {
            if (_assistants[i] == student) {
                assistant_ref.access().lock()->remove_from_assisting(_id);
                _assistants.erase(_assistants.begin() + i);
                _adminsys->log(_id, "Successfully removed student.");
                return;
            }
        }
    }
    _adminsys->log(_id, Log_Level::Error, "No such student.");
}

auto Research_Project::complete() -> void
{
    _adminsys->log(_id, "Finalizing research project as complete.");

    if (_status != Research_Status::Ongoing) {
        _adminsys->log(_id, Log_Level::Warning, "Already finalized. Skipping");
        return;
    }

    _adminsys->log(_id, "Finalizing research with assistants.");
    for (const auto& assistant : _assistants) {
        auto assistant_ref = _adminsys->student(assistant);
        if (assistant_ref.exists()) {
            assistant_ref.access().lock()->finalize_assisting_research(_id);
        }
    }

    _adminsys->log(_id, "Finalizing research with teachers.");
    for (const auto& teacher : _teachers) {
        auto teacher_ref = _adminsys->teacher(teacher);
        if (teacher_ref.exists()) {
            teacher_ref.access().lock()->archive_research(_id);
        }
    }

    _status = Research_Status::Complete;
    _adminsys->log(_id, "Successfully finalized research.");
}
auto Research_Project::cancel() -> void
{
    _adminsys->log(_id, "Finalizing research project as cancelled.");

    if (_status != Research_Status::Ongoing) {
        _adminsys->log(_id, Log_Level::Warning, "Already finalized. Skipping.");
        return;
    }

    _adminsys->log(_id, "Finalizing research with assistants.");
    for (const auto& assistant : _assistants) {
        auto assistant_ref = _adminsys->student(assistant);
        if (assistant_ref.exists()) {
            assistant_ref.access().lock()->finalize_assisting_research(_id);
        }
    }

    _adminsys->log(_id, "Finalizing research with teachers.");
    for (const auto& teacher : _teachers) {
        auto teacher_ref = _adminsys->teacher(teacher);
        if (teacher_ref.exists()) {
            teacher_ref.access().lock()->archive_research(_id);
        }
    }

    _status = Research_Status::Cancelled;
    _adminsys->log(_id, "Successfully finalized research.");
}

auto Research_Project::change_department(DID new_department) -> void
{
    _adminsys->log(_id, "Changing department to " + new_department.as_string());
    _department = new_department;
}

}