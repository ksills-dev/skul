#include "admin/admin_db.hpp"

#include "util/functional.hpp"

#include <type_traits>

namespace admin 
{

Admin_DB::Admin_DB(std::string dirpath)
    : _dirpath(dirpath + "/"), _logger(_dirpath + "log/admin.log"),
      _uid_factory(), _did_factory(), _rid_factory(), _cid_factory(), _sid_factory(),
      _students(), _teachers(), _departments(), _research(), _course_defs(), _course_secs()
{
    log("Admin Database successfully booted.");
}


auto Admin_DB::load() -> bool
{
    log("Loading Database from " + _dirpath);
    log("Initializing Parser.");
    sdf::SDF_Parser parser;

    auto courses_input_file =     _dirpath + "courses.txt";
    auto departments_input_file = _dirpath + "departments.txt";
    auto research_input_file =    _dirpath + "research.txt";
    auto students_input_file =    _dirpath + "students.txt";
    auto teachers_input_file =    _dirpath + "teachers.txt";

    log("Reading " + courses_input_file);
    if (parser.load_file(courses_input_file)) {
        for (const auto& node : parser.table()) {
            if (node.kind() == sdf::SDF_Node_Type::Value) {
                log(Log_Level::Warning, "Invalid node type for this record, skipping " + 
                                        node.name() + "...");
                continue;
            }
            if (node.name() == "Course_Definition") {
                log("Found Course Definition...");
                auto parsed_def = Course_Definition::from_sdf(this, node.branch().access());
                if (parsed_def.exists()) {
                    log("Generating entry for definition...");
                    _course_defs[parsed_def.access().id()] = 
                        std::shared_ptr<Course_Definition>(
                                new Course_Definition(parsed_def.access()));
                } else {
                    log(Log_Level::Error,"But it's ill-formed. Skipping.");
                }
            }
            else if (node.name() == "Course_Section") {
                log("Found Course Section...");
                auto parsed_section = Course_Section::from_sdf(this, node.branch().access());
                if (parsed_section.exists()) {
                    log("Generating entry for section...");
                    _course_secs[parsed_section.access().id()] = 
                        std::shared_ptr<Course_Section>(
                            new Course_Section(parsed_section.access()));
                } else {
                    log(Log_Level::Error,"But it's ill-formed. Skipping.");
                }
            } else {
                log(Log_Level::Warning, "Invalid node name for this record, skipping " 
                                        + node.name() + "...");
            }
        }
        log("Successfully read " + courses_input_file);
    } else {
        log(Log_Level::Error,"Failure!");
        return false;
    }

    log("Reading " + departments_input_file);
    if (parser.load_file(departments_input_file)) {
        for (const auto& node : parser.table()) {
            if (node.kind() == sdf::SDF_Node_Type::Value) {
                log(Log_Level::Warning, "Invalid node type for this record, skipping " +
                                        node.name() + "...");
                continue;
            }
            if (node.name() == "Department") {
                log("Found Department...");
                auto parsed_department = Department::from_sdf(this, node.branch().access());
                if (parsed_department.exists()) {
                    log("Generating entry for department...");
                    _departments[parsed_department.access().id()] = 
                        std::shared_ptr<Department>(
                            new Department(parsed_department.access()));
                        continue;
                } else {
                    log(Log_Level::Error,"But it's ill-formed. Skipping.");
                }
            } else {
                log(Log_Level::Warning, "Invalid node name for this record, skipping " + 
                                        node.name() + "...");
            }
        }
        log("Successfully read " + departments_input_file);
    } else {
        log(Log_Level::Error,"Failure!");
        return false;
    }

    log("Reading " + research_input_file);
    if (parser.load_file(research_input_file)) {
        for (const auto& node : parser.table()) {
            if (node.kind() == sdf::SDF_Node_Type::Value) {
                log(Log_Level::Warning, "Invalid node type for this record, skipping " + 
                                        node.name() + "...");
                continue;
            }
            if (node.name() == "Research_Project") {
                log("Found Research Project...");
                auto parsed_research = Research_Project::from_sdf(this, node.branch().access());
                if (parsed_research.exists()) {
                    log("Generating entry for research project...");
                    _research[parsed_research.access().id()] = 
                        std::shared_ptr<Research_Project>(
                            new Research_Project(parsed_research.access()));
                } else {
                    log(Log_Level::Error,"But it's ill-formed. Skipping.");
                }
            } else {
                log(Log_Level::Warning, "Invalid node name for this record, skipping " + 
                                        node.name() + "...");
            }
        }
        log("Successfully read " + research_input_file);
    } else {
        log(Log_Level::Error,"Failure!");
        return false;
    }

    log("Reading " + students_input_file);
    if (parser.load_file(students_input_file)) {
        for (const auto& node : parser.table()) {
            if (node.kind() == sdf::SDF_Node_Type::Value) {
                log(Log_Level::Warning, "Invalid node type for this record, skipping " + 
                                        node.name() + "...");
                continue;
            }
            if (node.name() == "Student") {
                log("Found Student...");
                auto parsed_student = Student::from_sdf(this, node.branch().access());
                if (parsed_student.exists()) {
                    log("Generating entry for student...");
                    _students[parsed_student.access().id()] = 
                        std::shared_ptr<Student>(
                            new Student(parsed_student.access()));
                } else {
                    log(Log_Level::Error,"But it's ill-formed. Skipping.");
                }
            } else {
                log(Log_Level::Warning,"Invalid node name for this record, skipping " + 
                                        node.name() + "...");
            }
        }
        log("Successfully read " + students_input_file);
    } else {
        log(Log_Level::Error,"Failure!");
        return false;
    }

    log("Reading " + teachers_input_file);
    if (parser.load_file(teachers_input_file)) {
        for (const auto& node : parser.table()) {
            if (node.kind() == sdf::SDF_Node_Type::Value) {
                log(Log_Level::Warning, "Invalid node type for this record, skipping " + 
                                         node.name() + "...");
                continue;
            }
            if (node.name() == "Teacher") {
                log("Found Teacher...");
                auto parsed_teacher = Teacher::from_sdf(this, node.branch().access());
                if (parsed_teacher.exists()) {
                    log("Generating entry for teacher...");
                    _teachers[parsed_teacher.access().id()] = 
                        std::shared_ptr<Teacher>(
                            new Teacher(parsed_teacher.access()));
                } else {
                    log(Log_Level::Error,"But it's ill-formed. Skipping.");
                }
            } else {
                log(Log_Level::Warning, "Invalid node name for this record, skipping " +
                                        node.name() + "...");
            }
        }
        log("Successfully read " + teachers_input_file);
    } else {
        log(Log_Level::Error,"Failure!");
        return false;
    }

    log("Generating Utilized ID Lists...");
    log("For UIDs:");
    {    
        std::vector<UID> utilized_uids; 
        for (const auto& student : _students) {
            utilized_uids.push_back(student.first);
        }
        for (const auto& teacher : _teachers) {
            utilized_uids.push_back(teacher.first);
        }
        _uid_factory = UID_Factory(utilized_uids);
    }
    log("For CIDs:");
    {
        std::vector<CID> utilized_cids;
        for (const auto& course_def : _course_defs) {
            utilized_cids.push_back(course_def.first);
        }
        _cid_factory = CID_Factory(utilized_cids);
    }
    log("For SIDs:");
    {  
        std::vector<SID> utilized_sids;
        for (const auto& course_sec : _course_secs) {
            utilized_sids.push_back(course_sec.first);
        }
        _sid_factory = SID_Factory(utilized_sids);
    } 
    log("For RIDs:");
    {
        std::vector<RID> utilized_rids;
        for (const auto& research : _research) {
            utilized_rids.push_back(research.first);
        }
        _rid_factory = RID_Factory(utilized_rids);
    }
    log("For DIDs:");
    {
        std::vector<DID> utilized_dids;
        for (const auto& department : _departments) {
            utilized_dids.push_back(department.first);
        }
        _did_factory = DID_Factory(utilized_dids);
    }
    log("Successfully generated ID lists.");
    
    log("Successfully loaded database.");
    return true;
}

auto Admin_DB::save() -> bool
{
    log("Saving database to " + _dirpath);
    log("Initializing parser");
    sdf::SDF_Parser parser;

    auto courses_output_file =     _dirpath + "courses.txt";
    auto departments_output_file = _dirpath + "departments.txt";
    auto research_output_file =    _dirpath + "research.txt";
    auto students_output_file =    _dirpath + "students.txt";
    auto teachers_output_file =    _dirpath + "teachers.txt";

    bool success = true;
    {
        log("Generating output table for students.");
        sdf::SDF_Branch output;
        for (const auto& student : _students) {
            output.push_back(Student::to_sdf(*student.second));
        }
        log("Successfully generated output table.");
        log("Writing table to parser.");
        parser.set_table(output);
        log("Setting parser target and writing to file.");
        if (!parser.save_file(students_output_file)) {
            log(Log_Level::Error, "Failed to write " + students_output_file);
            success = false;
        }
    }
    {
        log("Generating output table for teachers.");
        sdf::SDF_Branch output;
        for (const auto& teacher : _teachers) {
            output.push_back(Teacher::to_sdf(*teacher.second));
        }
        log("Successfully generated output table.");
        log("Writing table to parser.");
        parser.set_table(output);
        log("Setting parser target and writing to file.");
        if (!parser.save_file(teachers_output_file)) {
            log(Log_Level::Error, "Failed to write " + teachers_output_file);
            success = false;
        }
    }
    {
        log("Generating output table for courses.");
        sdf::SDF_Branch output;
        for (const auto& course_def : _course_defs) {
            output.push_back(Course_Definition::to_sdf(*course_def.second));
        }
        for (const auto& course_sec : _course_secs) {
            output.push_back(Course_Section::to_sdf(*course_sec.second));
        }
        log("Successfully generated output table.");
        log("Writing table to parser.");
        parser.set_table(output);
        log("Setting parser target and writing to file.");
        if (!parser.save_file(courses_output_file)) {
            log(Log_Level::Error, "Failed to write " + courses_output_file);
            success = false;
        }
    }
    {
        log("Generating output table for research.");
        sdf::SDF_Branch output;
        for (const auto& research : _research) {
            output.push_back(Research_Project::to_sdf(*research.second));
        }
        log("Successfully generated output table.");
        log("Writing table to parser.");
        parser.set_table(output);
        log("Setting parser target and writing to file.");
        if (!parser.save_file(research_output_file)) {
            log(Log_Level::Error, "Failed to write " + research_output_file);
            success = false;
        }
    }
    {
        log("Generating output table for departments.");
        sdf::SDF_Branch output;
        for (const auto& department : _departments) {
            output.push_back(Department::to_sdf(*department.second));
        }
        log("Successfully generated output table.");
        log("Writing table to parser.");
        parser.set_table(output);
        log("Setting parser target and writing to file.");
        if (!parser.save_file(departments_output_file)) {
            log(Log_Level::Error, "Failed to write " + departments_output_file);
            success = false;
        }
    }

    if (success) {
        log("Successfully saved database.");
    } else {
        log(Log_Level::Error, "Failed saving database.");
    }
    return success;
}

auto Admin_DB::log(util::Log_Level level, std::string message) const -> void
{
    _logger.log(level, message);
}

auto Admin_DB::log(std::string message) const -> void
{
    log(Log_Level::Info, message);
}


auto Admin_DB::enroll_student(Person person, DID department) -> Maybe<UID>
{
    log("Enrolling new student " + person.first_name() + " " + person.last_name() + 
        " in department " + department.as_string() + "...");
    if (_departments.count(department) > 0) {
        log("Generating UID for student.");
        auto new_id = _uid_factory.generate_id();

        log("Mapping new student.");
        _students.emplace(new_id, 
            std::shared_ptr<Student>(
                new Student{
                    Registered_Person{this, person, new_id, department}, 
                    Grade_Level::Freshman}));

        log("Enrolling new student " + new_id.as_string() + 
            " in department" + department.as_string());
        _departments[department]->enroll_student(new_id);

        return Maybe<UID>(new_id);
    }
    log(Log_Level::Error, "Failed - No such department.");
    return Maybe<UID>();
}

auto Admin_DB::unenroll_student(UID student) -> void
{
    log("Unenrolling student " + student.as_string() + "...");
    if (_students.count(student) > 0) {
        _students[student]->deactivate();
    }
}

auto Admin_DB::student_map() const -> std::unordered_map<UID, std::weak_ptr<Student>, UID::hash>
{
    log(std::string("Student map requested, generating map..."));
    std::unordered_map<UID, std::weak_ptr<Student>, UID::hash> result;
    for (const auto& student : _students) {
        result.emplace(student.first, std::weak_ptr<Student>(student.second));
    }
    log(std::string("Map constructed. Returning."));
    return result;
}

auto Admin_DB::students() const -> std::vector<std::weak_ptr<Student>>
{
    log(std::string("Student vector requested, generating vector..."));
    std::vector<std::weak_ptr<Student>> result;
    for (const auto& student : _students) {
        result.emplace_back(std::weak_ptr<Student>(student.second));
    }
    log(std::string("Vector constructed. Returning."));
    return result;
}

auto Admin_DB::student(UID student) const -> Maybe<std::weak_ptr<Student>>
{
    if (_students.count(student) > 0) {
        return Maybe<std::weak_ptr<Student>>(_students.at(student));
    } else {
        return Maybe<std::weak_ptr<Student>>();
    }
}


auto Admin_DB::hire_teacher(Person person, DID department, Teacher_Kind kind) -> Maybe<UID>
{
    log("Hiring new teacher " + person.first_name() + " " + person.last_name() + 
        " in department " + department.as_string() + "...");
    if (_departments.count(department) > 0) {
        log("Generating UID for teacher.");
        auto new_id = _uid_factory.generate_id();

        _teachers.emplace(new_id, 
            std::shared_ptr<Teacher>(
                new Teacher{
                    Registered_Person{this, person, new_id, department}, 
                    kind}));

        log("Assigning new teacher " + new_id.as_string() + 
            " to department" + department.as_string());
        _departments[department]->assign_teacher(new_id);

        return Maybe<UID>(new_id);
    }
    log(Log_Level::Error, "Failed - No such department.");
    return Maybe<UID>();
}

auto Admin_DB::fire_teacher(UID teacher) -> void
{
    log("Unenrolling teacher " + teacher.as_string() + "...");
    if (_teachers.count(teacher) > 0) {
        _teachers[teacher]->deactivate();
    }
}

auto Admin_DB::teacher_map() const -> std::unordered_map<UID, std::weak_ptr<Teacher>, UID::hash>
{
    log(std::string("Teacher map requested, generating map..."));
    std::unordered_map<UID, std::weak_ptr<Teacher>, UID::hash> result;
    for (const auto& teacher : _teachers) {
        result.emplace(teacher.first, std::weak_ptr<Teacher>(teacher.second));
    }
    log(std::string("Map constructed. Returning."));
    return result;
}

auto Admin_DB::teachers() const -> std::vector<std::weak_ptr<Teacher>>
{
    log(std::string("Teacher vector requested, generating vector..."));
    std::vector<std::weak_ptr<Teacher>> result;
    for (const auto& teacher : _teachers) {
        result.emplace_back(std::weak_ptr<Teacher>(teacher.second));
    }
    log(std::string("Vector constructed. Returning."));
    return result;
}

auto Admin_DB::teacher(UID teacher) const -> Maybe<std::weak_ptr<Teacher>>
{
    if (_teachers.count(teacher) > 0) {
        return Maybe<std::weak_ptr<Teacher>>(_teachers.at(teacher));
    } else {
        return Maybe<std::weak_ptr<Teacher>>();
    }
}


auto Admin_DB::start_research(std::string name, DID department) -> Maybe<RID>
{
    log("Starting new research project " + name + " in department " + 
        department.as_string() + "...");
    if (_departments.count(department) > 0) {
        log("Generating RID for research.");
        auto new_id = _rid_factory.generate_id();

        _research.emplace(new_id, 
            std::shared_ptr<Research_Project>(
                new Research_Project{this, new_id, department, name}));

        log("Assigning new research " + new_id.as_string() + 
            " to department" + department.as_string());
        _departments[department]->add_research(new_id);
        return Maybe<RID>(new_id);
    }
    log(Log_Level::Error, "Failed - No such department.");
    return Maybe<RID>();
}

auto Admin_DB::cancel_research(RID project) -> void
{
    log("Cancelling research project " + project.as_string() + "...");
    if (_research.count(project) > 0) {
        _research[project]->cancel();
    }
}

auto Admin_DB::research_map() const -> std::unordered_map<RID, std::weak_ptr<Research_Project>, RID::hash>
{
    log(std::string("Research Project map requested, generating map..."));
    std::unordered_map<RID, std::weak_ptr<Research_Project>, RID::hash> result;
    for (const auto& research : _research) {
        result.emplace(research.first, std::weak_ptr<Research_Project>(research.second));
    }
    log(std::string("Map constructed. Returning."));
    return result;
}

auto Admin_DB::research() const -> std::vector<std::weak_ptr<Research_Project>>
{
    log(std::string("Research Project vector requested, generating vector..."));
    std::vector<std::weak_ptr<Research_Project>> result;
    for (const auto& research : _research) {
        result.emplace_back(std::weak_ptr<Research_Project>(research.second));
    }
    log(std::string("Vector constructed. Returning."));
    return result;
}

auto Admin_DB::research(RID project) const -> Maybe<std::weak_ptr<Research_Project>>
{
    if (_research.count(project) > 0) {
        return Maybe<std::weak_ptr<Research_Project>>(_research.at(project));
    } else {
        return Maybe<std::weak_ptr<Research_Project>>();
    }
}


auto Admin_DB::define_course(std::string name, std::string number, DID department, int credits) -> Maybe<CID>
{
    return define_course(name, number, department, credits,
                         std::vector<CID>(), std::vector<CID>(),
                         std::vector<CID>());
}

auto Admin_DB::define_course(std::string name, std::string number, DID department, int credits,
                    std::vector<CID> prereqs, std::vector<CID> coreqs,
                    std::vector<CID> equivalencies) -> Maybe<CID>
{
    log("Defining new course " + name + "...");
    if (_departments.count(department) > 0) {
        log("Finding course number conflicts.");
        for (const auto& course : _departments[department]->courses()) {
            if (_course_defs.count(course) > 0) {
                if (_course_defs[course]->number() == number) {
                    log(Log_Level::Error, "Failed - department already contains course number " + number);
                    return Maybe<CID>();
                }
            }
        }

        log("Generating CID for course definition.");
        auto new_id = _cid_factory.generate_id();
        _course_defs.emplace(new_id, 
            std::shared_ptr<Course_Definition>(
                new Course_Definition(this, new_id, department, name, number, credits,
                                      prereqs, coreqs, equivalencies, false)));
        
        log("Assigning new course " + new_id.as_string() + 
            " to department" + department.as_string());
        _departments[department]->add_course(new_id);
        return Maybe<CID>(new_id);

    } else {
        log(Log_Level::Error, "Failed - no such department.");
        return Maybe<CID>();
    }
}

auto Admin_DB::retire_course(CID course_def) -> void
{
    log("Retiring course definition " + course_def.as_string());
    if (_course_defs.count(course_def) > 0) {
        _course_defs[course_def]->retire();
    }
    log("Failed - no such course exists.");
}

auto Admin_DB::course_def_map() const -> std::unordered_map<CID, std::weak_ptr<Course_Definition>,
                                                            CID::hash>
{
    log(std::string("Course Definition map requested, generating map..."));
    std::unordered_map<CID, std::weak_ptr<Course_Definition>, CID::hash> result;
    for (const auto& course_def : _course_defs) {
        result.emplace(course_def.first, std::weak_ptr<Course_Definition>(course_def.second));
    }
    log(std::string("Map constructed. Returning."));
    return result;
}

auto Admin_DB::course_defs() const -> std::vector<std::weak_ptr<Course_Definition>>
{
    log(std::string("Course Definition vector requested, generating vector..."));
    std::vector<std::weak_ptr<Course_Definition>> result;
    for (const auto& course_def : _course_defs) {
        result.emplace_back(std::weak_ptr<Course_Definition>(course_def.second));
    }
    log(std::string("Vector constructed. Returning."));
    return result;
}

auto Admin_DB::course_def(CID course) const -> Maybe<std::weak_ptr<Course_Definition>>
{
    if (_course_defs.count(course) > 0) {
        return Maybe<std::weak_ptr<Course_Definition>>(_course_defs.at(course));
    } else {
        return Maybe<std::weak_ptr<Course_Definition>>();
    }
}


auto Admin_DB::create_course(CID course_def, int seats) -> Maybe<SID>
{
    log("Creating course section for definition " + course_def.as_string());
    if (_course_defs.count(course_def) > 0) {
        if (_course_defs[course_def]->retired()) {
            log(Log_Level::Error, "Failed - course definition has been retired.");
        }

        log("Generating SID for course section.");
        auto new_id = _sid_factory.generate_id();
        _course_secs.emplace(new_id, 
            std::shared_ptr<Course_Section>(
                new Course_Section(this, new_id, course_def, seats)));
        return Maybe<SID>(new_id);

    } else {
        log(Log_Level::Error, "Failed - no such course definition exists.");
        return Maybe<SID>();
    }
}

auto Admin_DB::cancel_course(SID course_section) -> void
{
    log("Cancelling course section " + course_section.as_string());
    if (_course_secs.count(course_section) > 0) {
        auto course_ref = _course_secs[course_section];

        log("Purging students from course...");
        func::apply<UID>( 
            [&](UID& student) {
                course_ref->remove_student(student);
            }, 
            course_ref->students());
        log("Purging assistants from course...");
        func::apply<UID>( 
            [&](UID& assistant) {
                course_ref->remove_assistant(assistant);
            }, 
            course_ref->assistants());
        log("Purging teachers from course...");
        func::apply<UID>( 
            [&](UID& teacher) {
                course_ref->remove_teacher(teacher);
            }, 
            course_ref->teachers());

        log("Removing section record...");
        _course_secs.erase(course_section);
        log("Freeing course section ID.");
        _sid_factory.free_id(course_section);
        log("Successfully cancelled course");
    } else {
        log(Log_Level::Error, "Failed - no such section exists.");
    }
}

auto Admin_DB::course_map() const -> std::unordered_map<SID, std::weak_ptr<Course_Section>,
                                                        SID::hash>
{
    log(std::string("Course Definition map requested, generating map..."));
    std::unordered_map<SID, std::weak_ptr<Course_Section>, SID::hash> result;
    for (const auto& course_sec : _course_secs) {
        result.emplace(course_sec.first, std::weak_ptr<Course_Section>(course_sec.second));
    }
    log(std::string("Map constructed. Returning."));
    return result;
}

auto Admin_DB::courses() const -> std::vector<std::weak_ptr<Course_Section>>
{
    log(std::string("Course Section vector requested, generating vector..."));
    std::vector<std::weak_ptr<Course_Section>> result;
    for (const auto& course_sec : _course_secs) {
        result.emplace_back(std::weak_ptr<Course_Section>(course_sec.second));
    }
    log(std::string("Vector constructed. Returning."));
    return result;
}

auto Admin_DB::course(SID section) const -> Maybe<std::weak_ptr<Course_Section>>
{
    if (_course_secs.count(section) > 0) {
        return Maybe<std::weak_ptr<Course_Section>>(_course_secs.at(section));
    } else {
        return Maybe<std::weak_ptr<Course_Section>>();
    }
}


auto Admin_DB::create_department(std::string name, std::string prefix) -> Maybe<DID>
{
    log("Creating new Department for " + name + " (" + prefix + ")...");
    for (const auto& department : _departments) {
        if ((department.second->name() == name || department.second->prefix() == prefix) 
           && !(department.second->archived())) {
            log(Log_Level::Error, "Failed - conflicting name or prefix with existing department");
            return Maybe<DID>();
        }
    }
    log("Generating department ID.");
    const auto new_id = _did_factory.generate_id();
    log("Placing department into map.");
    _departments.emplace(new_id, 
        std::shared_ptr<Department>(
            new Department(this, new_id, name, prefix)));
    log("Successfully created department.");
    return Maybe<DID>(new_id);
}

auto Admin_DB::archive_department(DID department) -> void
{
    log("Archiving department "  + department.as_string());
    if (_departments.count(department) > 0) {
        _departments[department]->archive();
    } else {
        log(Log_Level::Error, "Failed - no such department.");
    }
}

auto Admin_DB::department_map() const -> std::unordered_map<DID, std::weak_ptr<Department>,
                                                            DID::hash>
{
    log("Course Definition map requested, generating map...");
    std::unordered_map<DID, std::weak_ptr<Department>, DID::hash> result;
    for (const auto& department : _departments) {
        result.emplace(department.first, std::weak_ptr<Department>(department.second));
    }
    log("Map constructed. Returning.");
    return result;
}

auto Admin_DB::departments() const -> std::vector<std::weak_ptr<Department>>
{
    log("Department vector requested, generating vector...");
    std::vector<std::weak_ptr<Department>> result;
    for (const auto& department : _departments) {
        result.emplace_back(std::weak_ptr<Department>(department.second));
    }
    log("Vector constructed. Returning.");
    return result;
}

auto Admin_DB::department(DID department) const -> Maybe<std::weak_ptr<Department>>
{
    if (_departments.count(department) > 0) {
        return Maybe<std::weak_ptr<Department>>(_departments.at(department));
    } else {
        return Maybe<std::weak_ptr<Department>>();
    }
}

}
