#include "admin/department.hpp"
 
#include "admin/admin_db.hpp"

namespace admin
{

Department::Department(Admin_DB* const adminsys, DID id, std::string name, std::string prefix)
        : _adminsys(adminsys), _id(id), _name(name), _prefix(prefix), _archived(false),
          _teachers(), _students(), _courses(), _research()
{
}

Department::Department(Admin_DB* const adminsys, DID id, std::string name, std::string prefix,
            std::vector<UID> students, std::vector<UID> teachers,
            std::vector<CID> courses, std::vector<RID> research, bool archived)
        : _adminsys(adminsys), _id(id), _name(name), _prefix(prefix), _archived(archived),
          _teachers(teachers), _students(students), _courses(courses), _research(research)
{
}
            

auto Department::from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Department>
{
    adminsys->log("Attempting to parse Department from SDF_Node");

    DID id;
    bool id_found = false;
    std::string name;
    bool name_found = false;
    std::string prefix;
    bool prefix_found = false;
    bool archived = false;
    bool archived_found = false;
    std::vector<UID> teachers;
    bool teachers_found = false;
    std::vector<UID> students;
    bool students_found = false;
    std::vector<CID> courses;
    bool courses_found = false;
    std::vector<RID> research;
    bool research_found = false;

    for (const auto& node : branch) {
        if (node.name() == "id") {
            adminsys->log("Found id.");
            auto node_int = node.as_int();
            if (!node_int.exists()) {
                adminsys->log("But it's ill-formed.");
                return util::Maybe<Department>();
            }
            id = DID(node_int.access());
            id_found = true;
        }
        else if (node.name() == "name") {
            adminsys->log("Found name.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log("But it's ill-formed.");
                return util::Maybe<Department>();
            }
            name = node.as_string().access();
            name_found = true;
        }
        else if (node.name() == "prefix") {
            adminsys->log("Found prefix.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log("But it's ill-formed.");
                return util::Maybe<Department>();
            }
            prefix = node.as_string().access();
            if (prefix.length() != 3) { // A prefix must be 3 letters.
                return util::Maybe<Department>();
            } 
            prefix_found = true;
        }
        else if (node.name() == "archived") {
            adminsys->log("Found archived.");
            auto node_bool = node.as_bool();
            if (!node_bool.exists()) {
                adminsys->log("But it's ill-formed.");
                return util::Maybe<Department>();
            }
            archived = node_bool.access();
            archived_found = true;
        }
        else if (node.name() == "teachers") {
            adminsys->log("Found teachers.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log("But it's ill-formed.");
                return util::Maybe<Department>();
            }
            teachers = parse_id_list<UID>(node.as_string().access());
            teachers_found = true;
        }
        else if (node.name() == "students") {
            adminsys->log("Found students.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log("But it's ill-formed.");
                return util::Maybe<Department>();
            }
            students = parse_id_list<UID>(node.as_string().access());
            students_found = true;
        }
        else if (node.name() == "courses") {
            adminsys->log("Found courses.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log("But it's ill-formed.");
                return util::Maybe<Department>();
            }
            courses = parse_id_list<CID>(node.as_string().access());
            courses_found = true;
        }
        else if (node.name() == "research") {
            adminsys->log("Found research.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log("But it's ill-formed.");
                return util::Maybe<Department>();
            }
            research = parse_id_list<RID>(node.as_string().access());
            research_found = true;
        } else {
            adminsys->log(Log_Level::Warning, "Unknown entry " + node.name() + ". Skipping.");
        }
    }

    if (!id_found || !name_found || !prefix_found || !archived_found || 
        !teachers_found || !students_found || !courses_found || !research_found) {
            adminsys->log("Missing necessary entry for Department."); 
            return util::Maybe<Department>(); 
    }

    adminsys->log("Successfully parsed Department.");
    return util::Maybe<Department>(true, Department(adminsys, id, name, prefix, students, 
                                                    teachers, courses, research, archived));
}

auto Department::to_sdf(const Department& department) -> sdf::SDF_Node
{
    department._adminsys->log("Writing department " + department._id.as_string() + " to SDF_Node.");
    sdf::SDF_Branch results;

    results.emplace_back("id", department._id.as_string());
    results.emplace_back("name", department._name);
    results.emplace_back("prefix", department._prefix);
    results.emplace_back("archived", (department._archived) ? "true" : "false");
    results.emplace_back("teachers", write_id_list<UID>(department._teachers));
    results.emplace_back("students", write_id_list<UID>(department._students));
    results.emplace_back("courses", write_id_list<CID>(department._courses));
    results.emplace_back("research", write_id_list<RID>(department._research));

    department._adminsys->log("Write for department" + department._id.as_string() + " success.");
    return sdf::SDF_Node("Department", results);
}


auto Department::id() const -> DID
{
    return _id;
}

auto Department::name() const -> std::string
{
    return _name;
}

auto Department::prefix() const -> std::string
{
    return _prefix;
}

auto Department::archived() const -> bool
{
    return _archived;
}

auto Department::archive() -> void
{
    _adminsys->log(_id, "Archiving department...");
    _archived = true;
}

auto Department::teachers() const -> std::vector<UID>
{
    return _teachers;
}

auto Department::students() const -> std::vector<UID>
{
    return _students;
}

auto Department::courses() const -> std::vector<CID>
{
    return _courses;
}

auto Department::research() const -> std::vector<RID>
{
    return _research;
}

auto Department::enroll_student(UID student) -> bool
{
    _adminsys->log(_id, "Enrolling student " + student.as_string());

    if (_archived) {
        _adminsys->log(_id, Log_Level::Error, "Failed - Department closed.");
        return false;
    }

    // Ensure not already enrolled
    for (const auto& id : _students) {
        if (id == student) {
            _adminsys->log(_id, Log_Level::Warning, "Student already enrolled. Skipping.");
            return true;
        }
    }
    
    // Get currently enrolled department and remove student from list.
    auto student_ref = _adminsys->student(student);
    if (student_ref.exists()) {
        _adminsys->log(_id, "Removing student from previous department records.");
        auto department_ref = _adminsys->department(student_ref.access().lock()->department());
        if (department_ref.exists()) {
            auto student_list = department_ref.access().lock()->_students;
            for (size_t i = 0; i < student_list.size(); ++i) {
                if (student == student_list[i]) {
                    student_list.erase(student_list.begin() + i);;
                    break;
                }
            }
        } else {
            _adminsys->log(_id, Log_Level::Warning, "Previous department does not exist.");
        }
        student_ref.access().lock()->change_department(_id);
    } else {
        _adminsys->log(_id, Log_Level::Error, "No such student exists.");
        return false;
    }

    // Push student into list
    _students.push_back(student);
    _adminsys->log(_id, "Successfully enrolled student.");
    return true;
}

auto Department::remove_student(UID student, DID new_department) -> bool
{
    _adminsys->log(_id, "Removing student " + student.as_string());

    _adminsys->log(_id, "Enrolling student in new department " + new_department.as_string());
    auto new_department_ref = _adminsys->department(new_department);
    if (new_department_ref.exists()) {
        if (!new_department_ref.access().lock()->enroll_student(student)) {
            _adminsys->log(_id, Log_Level::Error, "Failed to enroll student in new department.");
            return false;
        }
    } else {
        _adminsys->log(_id, Log_Level::Error, "New department does not exist.");
        return false;
    }

    _adminsys->log(_id, "Successfully changing student department.");
    return true;
}

auto Department::assign_teacher(UID teacher) -> bool
{
    _adminsys->log(_id, "Assigning teacher " + teacher.as_string() + " to department.");

    if (_archived) {
        _adminsys->log(_id, Log_Level::Error, "Failed - Department closed.");
        return false;
    }

    // Ensure not already enrolled
    for (const auto& id : _teachers) {
        if (id == teacher) {
            _adminsys->log(_id, Log_Level::Warning, "Teacher already assigned. Skipping");
            return true;
        }
    }
    
    // Get currently enrolled department and remove student from list.
    auto teacher_ref = _adminsys->teacher(teacher);
    if (teacher_ref.exists()) {
        _adminsys->log(_id, "Removing teacher from previous department records.");
        auto department_ref = _adminsys->department(teacher_ref.access().lock()->department());
        if (department_ref.exists()) {
            auto teacher_list = department_ref.access().lock()->_teachers;
            for (size_t i = 0; i < teacher_list.size(); ++i) {
                if (teacher == teacher_list[i]) {
                    teacher_list.erase(teacher_list.begin() + i);
                    break;
                }
            }
        } else {
            _adminsys->log(_id, Log_Level::Warning, "Previous department does not exist.");
        }
        teacher_ref.access().lock()->change_department(_id);
    } else {
        _adminsys->log(_id, Log_Level::Error, "No such teacher exists.");
        return false;
    }

    // Push teacher into list
    _teachers.push_back(teacher);
    _adminsys->log(_id, "Successfully assigned teacher.");
    return true;
}

auto Department::remove_teacher(UID teacher, DID new_department) -> bool
{
    _adminsys->log(_id, "Removing teacher " + teacher.as_string());

    _adminsys->log(_id, "Assigning teacher to new department " + new_department.as_string());
    auto new_department_ref = _adminsys->department(new_department);
    if (new_department_ref.exists()) {
        if (!new_department_ref.access().lock()->assign_teacher(teacher)) {
            _adminsys->log(_id, Log_Level::Error, "Failed to assign teacher to new department");
            return false;
        }
    } else {
        _adminsys->log(_id, Log_Level::Error, "No such department exists.");
        return false;
    }

    _adminsys->log("Successfully removed teacher.");
    return true;
}

auto Department::add_course(CID course) -> bool
{
    _adminsys->log(_id, "Assigning course " + course.as_string() + " to department.");

    if (_archived) {
        _adminsys->log(_id, Log_Level::Error, "Failed - Department closed.");
        return false;
    }

    // Ensure not already enrolled
    for (const auto& id : _courses) {
        if (id == course) {
            _adminsys->log(_id, Log_Level::Warning, "Course already assigned. Skipping");
            return false;
        }
    }
    
    // Get currently enrolled department and remove course from list.
    auto course_ref = _adminsys->course_def(course);
    if (course_ref.exists()) {
        _adminsys->log(_id, "Checking for course number conflicts.");
        for (const auto& id : _courses) {
            auto existing_course_ref = _adminsys->course_def(id);
            if (existing_course_ref.exists()) {
                if (existing_course_ref.access().lock()->number() == 
                        course_ref.access().lock()->number()) {
                    _adminsys->log(_id, Log_Level::Error, 
                        "Course number conflict with existing course definition.");
                    return false;
                }
            }
        }

        _adminsys->log(_id, "Removing course from previous department records.");
        auto department_ref = _adminsys->department(course_ref.access().lock()->department());
        if (department_ref.exists()) {
            auto course_list = department_ref.access().lock()->_courses;
            for (size_t i = 0; i < course_list.size(); ++i) {
                if (course == course_list[i]) {
                    course_list.erase(course_list.begin() + i);
                    break;
                }
            }
        } else {
            _adminsys->log(_id, Log_Level::Warning, "Previous department does not exist.");
        }
        course_ref.access().lock()->change_department(_id);
    } else {
        _adminsys->log(_id, Log_Level::Error, "No such course exists.");
        return false;
    }

    // Push student into list
    _courses.push_back(course);
    _adminsys->log("Successfully assigned course.");
    return true;
}

auto Department::remove_course(CID course, DID new_department) -> bool
{
    _adminsys->log(_id, "Removing course " + course.as_string());

    if (_archived) {
        _adminsys->log(_id, Log_Level::Error, "Failed - Department closed.");
        return false;
    }

    _adminsys->log(_id, "Assigning course to new department " + new_department.as_string());
    auto new_department_ref = _adminsys->department(new_department);
    if (new_department_ref.exists()) {
        if (!new_department_ref.access().lock()->add_course(course)) {
            _adminsys->log(_id, Log_Level::Error, "Failed to assign course to new department");
            return false;
        }
    } else {
        _adminsys->log(_id, Log_Level::Error, "No such department exists.");
        return false;
    }

    _adminsys->log("Successfully removed course.");
    return true;
}

auto Department::add_research(RID project) -> bool
{
    _adminsys->log(_id, "Assigning research " + project.as_string() + " to department.");

    if (_archived) {
        _adminsys->log(_id, Log_Level::Error, "Failed - Department closed.");
        return false;
    }

    // Ensure not already assigned
    for (const auto& id : _research) {
        if (id == project) {
            _adminsys->log(_id, Log_Level::Warning, "Research already assigned. Skipping");
            return false;
        }
    }
    
    // Get currently assigned department and remove project from list.
    auto project_ref = _adminsys->research(project);
    if (project_ref.exists()) {
        _adminsys->log(_id, "Removing research from previous department records.");
        auto department_ref = _adminsys->department(project_ref.access().lock()->department());
        if (department_ref.exists()) {
            auto project_list = department_ref.access().lock()->_research;
            for (size_t i = 0; i < project_list.size(); ++i) {
                if (project == project_list[i]) {
                    project_list.erase(project_list.begin() + i);
                    break;
                }
            }
        } else {
            _adminsys->log(_id, Log_Level::Warning, "Previous department does not exist.");
        }
        project_ref.access().lock()->change_department(_id);
    } else {
        _adminsys->log(_id, Log_Level::Error, "No such research project exists.");
        return false;
    }

    // Push project into list
    _research.push_back(project);
    _adminsys->log("Successfully assigned research project.");
    return true;
}

auto Department::remove_research(RID project, DID new_department) -> bool
{
    _adminsys->log(_id, "Removing research project " + project.as_string());

    if (_archived) {
        _adminsys->log(_id, Log_Level::Error, "Failed - Department closed.");
        return false;
    }

    _adminsys->log(_id, "Assigning research to new department " + new_department.as_string());
    auto new_department_ref = _adminsys->department(new_department);
    if (new_department_ref.exists()) {
        if (!new_department_ref.access().lock()->add_research(project)) {
            _adminsys->log(_id, Log_Level::Error, "Failed to assign research to new department");
            return false;
        }
    } else {
        _adminsys->log(_id, Log_Level::Error, "No such department exists.");
        return false;
    }

    _adminsys->log("Successfully removed research project.");
    return true;
}


}
