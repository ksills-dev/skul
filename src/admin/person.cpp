#include "admin/person.hpp"

#include "admin/admin_db.hpp"

namespace admin
{

auto Person::from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Person>
{
    adminsys->log("Attempting to parse Person.");

    Date date;
    bool date_found = false;
    std::string first_name;
    bool first_name_found = false;
    std::string last_name;
    bool last_name_found = false;
    Gender gender = Gender::Male;
    bool gender_found = false;

    for (const auto& node : branch) {
        if (node.name() == "Date" && node.tag() == "date_of_birth") {
            adminsys->log("Found Date = date_of_birth");
            if (node.kind() == sdf::SDF_Node_Type::Value) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Person>();
            }
            auto child = Date::from_sdf(adminsys, node.branch().access());
            if (child.exists()) {
                date = child.access();
            } else {
                adminsys->log("But it's ill-formed.");
                return util::Maybe<Person>();
            }
            date_found = true;
        }
        else if (node.name() == "first_name") {
            adminsys->log("Found first_name");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Person>();
            }
            first_name = node.as_string().access();
            first_name_found = true;
        }
        else if (node.name() == "last_name") {
            adminsys->log("Found last_name");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Person>();
            }
            last_name = node.as_string().access();
            last_name_found = true;
        }
        else if (node.name() == "gender") {
            adminsys->log("Found gender.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Person>();
            }

            auto value = node.as_string().access();
            if (value == "male") {
                gender = Gender::Male;
            } else if (value == "female") {
                gender = Gender::Female;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Person>();
            }
            gender_found = true;
        } else {
            adminsys->log(Log_Level::Warning, "Found unknown entry " + node.name() + ". Skipping.");
        }
    }

    if (!date_found || !first_name_found || !last_name_found || !gender_found) {
        adminsys->log(Log_Level::Error, "Missing necessary entry for Person."); 
        return util::Maybe<Person>(); 
    };
    adminsys->log("Person parsed succesfully.");
    return util::Maybe<Person>(true, Person{first_name, last_name, date, gender});
}

auto Person::to_sdf(Admin_DB* const adminsys, const Person& person) -> sdf::SDF_Node
{
    adminsys->log("Generating SDF_Node from Person.");
    sdf::SDF_Branch result;

    auto date = Date::to_sdf(adminsys, person.birth_date());
    date.tag("date_of_birth");
    result.push_back(date);
    result.emplace_back("first_name", person.first_name());
    result.emplace_back("last_name", person.last_name());
    result.emplace_back("gender", (person.gender() == Gender::Male) ? "male" : "female");

    adminsys->log("Successfully generated SDF_Node from Person.");
    return sdf::SDF_Node("Person", result);
}

}