#include "admin/assignment.hpp"

#include "admin/admin_db.hpp"

namespace admin
{

auto Assignment_Grade::from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) 
        -> util::Maybe<Assignment_Grade>
{
    adminsys->log("Attempting to parse Assignment_Grade from SDF_Node.");

    Assignment_Grade grade;
    bool graded_found = false;
    bool earned_score_found = false;
    bool max_score_found = false; 

    for (const auto& node : branch) {
        if (node.name() == "graded") {
            adminsys->log("Found graded.");
            auto node_bool = node.as_bool();
            if (node_bool.exists()) {
                grade.graded = node_bool.access();
                graded_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Assignment_Grade>();
            }
        }
        else if (node.name() == "earned_score") {
            adminsys->log("Found earned_score.");
            auto node_float = node.as_float();
            if (node_float.exists()) {
                grade.earned_score = node_float.access();
                earned_score_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Assignment_Grade>();
            }
        } 
        else if (node.name() == "max_score") {
            adminsys->log("Found max_score.");
            auto node_float = node.as_float();
            if (node_float.exists()) {
                grade.max_score = node_float.access();
                max_score_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Assignment_Grade>();
            }
        } 
        else {
            adminsys->log(Log_Level::Warning, "Unknown entry " + node.name() + ". Skipping.");
        }
    }

    if (!graded_found || !earned_score_found || !max_score_found) {
        adminsys->log(Log_Level::Error, "Missing necessary entries for Assignment Grade.");
        return util::Maybe<Assignment_Grade>();
    }
    adminsys->log("Successfully parsed Assignment_Grade.");
    return util::Maybe<Assignment_Grade>(grade);
}

auto Assignment_Grade::to_sdf(Admin_DB* const adminsys, const Assignment_Grade& grade) -> sdf::SDF_Node
{
    adminsys->log("Generating Node from Assignment_Grade...");
    sdf::SDF_Branch result;

    result.emplace_back("graded", (grade.graded) ? "true" : "false" );
    result.emplace_back("earned_score", std::to_string(grade.earned_score));
    result.emplace_back("max_score", std::to_string(grade.max_score));

    adminsys->log("Successfully generated Node from Assignment_Grade.");
    return sdf::SDF_Node("Assignment_Grade", result);
}

auto Assignment::from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Assignment>
{
    adminsys->log("Attempting to parse Assignment from SDF_Node.");

    Assignment assignment; 
    bool id_found = false;
    bool name_found = false;
    bool max_score_found = false;
    bool weight_found = false;

    for (const auto& node : branch) {
        if (node.name() == "id") {
            adminsys->log("Found id.");
            auto node_int = node.as_int();
            if (node_int.exists()) {
                assignment.id = AID(node_int.access());
                id_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Assignment>();
            }
        }
        else if (node.name() == "name") {
            adminsys->log("Found name.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Assignment>();
            } else {
                assignment.name = node.as_string().access();
                name_found = true;
            }
        }
        else if (node.name() == "max_score") {
            adminsys->log("Found max_score.");
            auto node_float = node.as_float();
            if (node_float.exists()) {
                assignment.max_score = node_float.access();
                max_score_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Assignment>();
            }
        } 
        else if (node.name() == "weight") {
            adminsys->log("Found weight_score.");
            auto node_float = node.as_float();
            if (node_float.exists()) {
                assignment.weight = node_float.access();
                weight_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Assignment>();
            }
        } 
        else {
            adminsys->log(Log_Level::Warning, "Unknown entry " + node.name() + ". Skipping.");
        }
    }

    if (!id_found || !name_found || !max_score_found || !weight_found) {
        adminsys->log(Log_Level::Error, "Missing necessary entries for Assignment.");
        return util::Maybe<Assignment>();
    }
    adminsys->log("Successfully parsed Assignment.");
    return util::Maybe<Assignment>(assignment);
}

auto Assignment::to_sdf(Admin_DB* const adminsys, const Assignment& assignment) -> sdf::SDF_Node
{
    adminsys->log("Generating Node from Assignment...");
    sdf::SDF_Branch result;

    result.emplace_back("id", assignment.id.as_string());
    result.emplace_back("name", assignment.name);
    result.emplace_back("max_score", std::to_string(assignment.max_score));
    result.emplace_back("weight", std::to_string(assignment.weight));

    adminsys->log("Successfully generated Node from Assignment.");
    return sdf::SDF_Node("Assignment", result);
}

}