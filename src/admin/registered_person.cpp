#include "admin/registered_person.hpp"

#include "admin/admin_db.hpp"

namespace admin
{

auto Registered_Person::from_sdf(Admin_DB* const  adminsys, 
                                 const sdf::SDF_Branch& branch) -> util::Maybe<Registered_Person>
{
    adminsys->log("Attempting to parse Registered_Person.");

    Person base;
    bool base_found = false;
    UID id;
    bool id_found = false;
    DID department;
    bool department_found = false;
    bool active = false; 
    bool active_found = false;
    
    for (const auto& node : branch) {
        if (node.name() == "Person") {
            adminsys->log("Found Person.");
            auto person = Person::from_sdf(adminsys, node.branch().access());
            if (person.exists()) {
                base = person.access();
                base_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Registered_Person>();
            }
        }
        else if (node.name() == "id") {
            adminsys->log("Found id.");
            auto response = node.as_int();
            if (response.exists()) {
                id = UID(response.access());
                id_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Registered_Person>();
            }
        }
        else if (node.name() == "department") {
            adminsys->log("Found department.");
            auto response = node.as_int();
            if (response.exists()) {
                department = DID(response.access());
                department_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Registered_Person>();
            }
        } 
        else if (node.name() == "active") {
            adminsys->log("Found active.");
            auto response = node.as_bool(); 
            if (response.exists()) {
                active = response.access();
                active_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Registered_Person>();
            }
        }
    }

    if (!base_found || !id_found || !department_found || !active_found) {
        adminsys->log(Log_Level::Error, "Missing necessary entry for Registered_Person."); 
        return util::Maybe<Registered_Person>(); 
    };
    adminsys->log("Successfully parsed Registered_Person.");
    return util::Maybe<Registered_Person>(true, Registered_Person{adminsys, base, id, department, active});
}

auto Registered_Person::to_sdf(const Registered_Person& person) -> sdf::SDF_Node
{
    person._adminsys->log(person._id, "Generating SDF_Node from Registered_Person.");
    sdf::SDF_Branch result;

    result.push_back(Person::to_sdf(person._adminsys, person));
    result.emplace_back("id", std::to_string(person.id().raw()));
    result.emplace_back("department", std::to_string(person.department().raw()));
    result.emplace_back("active", (person.active()) ? "true" : "false");

    person._adminsys->log(person._id, "Successfully Generated SDF_Node from Registered_Person.");
    return sdf::SDF_Node("Registered_Person", result);
}

auto Registered_Person::id() const -> UID
{
    return _id;
}

auto Registered_Person::active() const -> bool
{
    return _active;
}

auto Registered_Person::department() const -> DID
{
    return _department;
}

auto Registered_Person::activate() -> void
{
    _active = true;
}

auto Registered_Person::deactivate() -> void
{
    _active = false;
}

auto Registered_Person::change_department(DID department) -> void
{
    _adminsys->log(_id, "Changing department to " + department.as_string());
    _department = department;
}

}