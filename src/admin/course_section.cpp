#include "admin/course_section.hpp"

#include "admin/admin_db.hpp"

namespace admin
{
    
Course_Section::Course_Section(Admin_DB* const adminsys, SID id, CID definition, unsigned int seats)
        : _adminsys(adminsys), _id(id), _definition(definition), _active(true), _seats(seats),
          _teachers(), _students(), _assistants(), _assignments(), _grade_book(), _aid_factory()
          
{
}

Course_Section::Course_Section(Admin_DB* const adminsys, SID id, CID definition, unsigned int seats,
                std::vector<UID> teachers, std::vector<UID> students, std::vector<UID> assistants,
                std::vector<Assignment> assignments, Grade_Book grade_book,
                bool active)
        : _adminsys(adminsys), _id(id), _definition(definition), _active(active), _seats(seats),
          _teachers(teachers), _students(students), _assistants(assistants), _assignments(),
          _grade_book(grade_book)
{
    _adminsys->log(_id, "Generating ID Utilization List for Factory.");
    std::vector<AID> used_ids;
    for (const auto& assignment : assignments) {
        if (_assignments.count(assignment.id) > 0) {
            used_ids.push_back(assignment.id);
        }
        _assignments[assignment.id] = assignment;
    }
    _adminsys->log(_id, "Reinitializing Factory with proper sparsity list.");
    _aid_factory = AID_Factory(used_ids);
}


auto Course_Section::from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Course_Section>
{
    adminsys->log("Attempting to parse Course_Section from SDF_Node.");

    Course_Section course(adminsys);
    bool id_found = false;
    bool definition_found = false;
    bool active_found = false;
    bool seats_found = false;
    bool teachers_found = false;
    bool students_found = false;
    bool assistants_found = false;
    bool assignments_found = false;
    bool grade_book_found = false;

    for (const auto& node : branch) {
        if (node.name() == "id") {
            adminsys->log("Found id.");
            auto node_int = node.as_int();
            if (node_int.exists()) {
                course._id = SID(node_int.access());
                id_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Course_Section>();
            }
        }
        else if (node.name() == "definition") {
            adminsys->log("Found definition.");
            auto node_int = node.as_int();
            if (node_int.exists()) {
                course._definition = CID(node_int.access());
                definition_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Course_Section>();
            }
        }
        else if (node.name() == "active") {
            adminsys->log("Found active.");
            auto node_bool = node.as_bool();
            if (node_bool.exists()) {
                course._active = node_bool.access();
                active_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Course_Section>();
            }
        }
        else if (node.name() == "seats") {
            adminsys->log("Found seats.");
            auto node_int = node.as_int();
            if (node_int.exists()) {
                course._seats = node_int.access();
                seats_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Course_Section>();
            }
        }
        else if (node.name() == "teachers") {
            adminsys->log("Found teachers.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Course_Section>();
            } else {
                course._teachers = parse_id_list<UID>(node.as_string().access());
                teachers_found = true;
            }
        }
        else if (node.name() == "students") {
            adminsys->log("Found students.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Course_Section>();
            } else {
                course._students = parse_id_list<UID>(node.as_string().access());
                students_found = true;
            }
        }
        else if (node.name() == "assistants") {
            adminsys->log("Found assistants.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Course_Section>();
            } else {
                course._assistants = parse_id_list<UID>(node.as_string().access());
                assistants_found = true;
            }
        }
        else if (node.name() == "Assignments") {
            adminsys->log("Found Assignments.");
            if (node.kind() == sdf::SDF_Node_Type::Value) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Course_Section>();
            } else {
                for (const auto& subnode : node.branch().access()) {
                    if (subnode.name() == "Assignment") {
                        adminsys->log("Found Assignment.");
                        if (subnode.kind() == sdf::SDF_Node_Type::Value) {
                            adminsys->log(Log_Level::Error, "But it's ill-formed.");
                            return util::Maybe<Course_Section>();
                        } else {
                            auto assignment = Assignment::from_sdf(adminsys, subnode.branch().access());
                            if (assignment.exists()) {
                                course._assignments[assignment.access().id] = assignment.access();
                            } else {
                                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                                return util::Maybe<Course_Section>();
                            }
                        }
                    } else {
                        adminsys->log(Log_Level::Error, "But it's ill-formed.");
                        return util::Maybe<Course_Section>();
                    }
                }
                assignments_found = true;
            }
        }
        else if (node.name() == "Grade_Book") {
            adminsys->log("Found Grade_Book.");
            if (node.kind() == sdf::SDF_Node_Type::Value) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Course_Section>();
            } else {
                for (const auto& subnode : node.branch().access()) {
                    if (subnode.name() == "Assignment_Grade") {
                        adminsys->log("Found Assignment_Grade.");
                        if (subnode.kind() == sdf::SDF_Node_Type::Value) {
                            adminsys->log(Log_Level::Error, "But it's ill-formed.");
                            adminsys->log(subnode.as_string().access());
                            return util::Maybe<Course_Section>();
                        } else {
                            auto grade = Assignment_Grade::from_sdf(adminsys, subnode.branch().access());
                            if (grade.exists()) {
                                auto keys = parse_id_list<UID>(subnode.tag()); // I parse a UID list, but one is an AID!
                                if (keys.size() != 2) {
                                    adminsys->log(Log_Level::Error, "But the key is ill-formed.");
                                    return util::Maybe<Course_Section>();
                                } else {
                                    auto student = UID(keys[0].raw());      // And so we explicitly convert it them.
                                    auto assignment = AID(keys[1].raw());
                                    if (course._grade_book.count(student) == 0) {
                                        course._grade_book.emplace(student, std::unordered_map<AID, Assignment_Grade, AID::hash>());
                                    }
                                    course._grade_book[student][assignment] = grade.access();
                                }
                            } else {
                                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                                return util::Maybe<Course_Section>();
                            }
                        }
                    } else {
                        adminsys->log(Log_Level::Error, "But it's contents are weird " + subnode.name());
                        return util::Maybe<Course_Section>();
                    }
                }
                grade_book_found = true;
            }
        }
        else {
            adminsys->log(Log_Level::Warning, "Unknown entry " + node.name() + ". Skipping.");
        }
    }

    if (!id_found || !definition_found || !active_found || !seats_found || 
            !teachers_found || !students_found || !assistants_found ||
            !assignments_found || !grade_book_found) {
        adminsys->log(Log_Level::Error, "Missing necessary entry for Course_Section.");
        return util::Maybe<Course_Section>();
    }
    adminsys->log("Successfully parsed Course_Section.");
    return util::Maybe<Course_Section>(course);
}

auto Course_Section::to_sdf(const Course_Section& section) -> sdf::SDF_Node
{
    section._adminsys->log(section._id, "Generating Node from Course Section...");
    sdf::SDF_Branch result;

    result.emplace_back("id", section._id.as_string());
    result.emplace_back("definition", section._definition.as_string());
    result.emplace_back("active", (section._active) ? "true" : "false");
    result.emplace_back("seats", std::to_string(section._seats));
    result.emplace_back("teachers", write_id_list(section._teachers));
    result.emplace_back("assistants", write_id_list(section._assistants));
    result.emplace_back("students", write_id_list(section._students));


    sdf::SDF_Branch assignment_branch;
    for (const auto& assignment : section._assignments) {
        assignment_branch.push_back(Assignment::to_sdf(section._adminsys, assignment.second));
    }
    result.emplace_back("Assignments", assignment_branch);

    sdf::SDF_Branch grade_book_branch;
    for (const auto& student : section._grade_book) {
        for (const auto& grade : student.second) {
            auto node = Assignment_Grade::to_sdf(section._adminsys, grade.second);
            node.tag(student.first.as_string() + "," + grade.first.as_string());
            grade_book_branch.push_back(node);
        }
    }
    result.emplace_back("Grade_Book", grade_book_branch);

    section._adminsys->log(section._id, "Successfully generated Node from Course Section.");
    return sdf::SDF_Node("Course_Section", result);
}


auto Course_Section::id() const -> SID
{
    return _id;
}

auto Course_Section::definition() const -> CID
{
    return _definition;
}


auto Course_Section::set_seats(const int number) -> bool
{
    _adminsys->log(_id, "Changing seat count to " + std::to_string(number) + "...");
    if (static_cast<int>(_students.size()) > number) {
        _adminsys->log(_id, Log_Level::Error, "Too many students already enrolled.");
        return false;
    }
    _seats = number;
    return true;
}

auto Course_Section::total_seats() const -> unsigned int
{
    return _seats;
}

auto Course_Section::free_seats() const -> unsigned int
{
    return (_seats - _students.size());
}

auto Course_Section::student_count() const -> unsigned int
{
    return _students.size();
}


auto Course_Section::is_active() const -> bool
{
    return _active;
}

auto Course_Section::finalize_course() -> void
{
    _adminsys->log(_id, "Finalizing course section...");
    _adminsys->log(_id, "Archiving with Students...");
    for (const auto& student : _students) {
        auto student_ref = _adminsys->student(student);
        if (student_ref.exists()) {
            student_ref.access().lock()->finalize_course(_id);
        }
    }

    _adminsys->log(_id, "Archiving with Teachers...");
    for (const auto& teacher : _teachers) {
        auto teacher_ref = _adminsys->teacher(teacher);
        if (teacher_ref.exists()) {
            teacher_ref.access().lock()->archive_course(_id);
        }
    }

    _adminsys->log(_id, "Archiving with Assistants...");
    for (const auto& assistant : _assistants) {
        auto assistant_ref = _adminsys->student(assistant);
        if (assistant_ref.exists()) {
            assistant_ref.access().lock()->finalize_course(_id);
        }
    }

    _adminsys->log(_id, "Successfully finalized course.");
    _active = false;
}


auto Course_Section::can_teach(UID teacher) const -> bool
{
    auto teacher_ref = _adminsys->teacher(teacher);
    if (teacher_ref.exists()) {
        return true;
    } else {
        return false;
    }
}

auto Course_Section::can_assist(UID student) const -> bool
{
    auto student_ref = _adminsys->student(student);
    if (student_ref.exists()) {
        return student_ref.access().lock()->can_assist();
    } else {
        return false;
    }
}

auto Course_Section::can_enroll(UID student) const -> bool
{
    auto student_ref = _adminsys->student(student);
    if (student_ref.exists()) {
        auto def_ref = _adminsys->course_def(_definition);
        if (def_ref.exists()) {
            return def_ref.access().lock()->can_enroll(student);
        } else {
            return false;
        }
    } else {
        return false;
    }
}


auto Course_Section::assign_teacher(UID teacher) -> bool
{
    _adminsys->log(_id, "Attempting to assign teacher " + teacher.as_string() + "...");

    if (!_active) {
        _adminsys->log(_id, Log_Level::Error, "Failed - Course closed.");
        return false;
    }

    for (const auto& id : _teachers) {
        if (id == teacher) {
            _adminsys->log(_id, Log_Level::Warning, "Teacher already assigned. Skipping");
            return true;
        }
    }

    if (can_teach(teacher)) {
        _adminsys->log(_id, "Adding course section to teacher...");
        _adminsys->teacher(teacher).access().lock()->assign_course(_id);
        _teachers.push_back(teacher);
        _adminsys->log(_id, "Successfully assigned teacher to course.");
        return true;
    } else {
        _adminsys->log(_id, Log_Level::Error, "Failed - teacher inelligible for this course.");
        return false;
    }
}

auto Course_Section::remove_teacher(UID teacher) -> void
{
    _adminsys->log(_id, "Removing teacher " + teacher.as_string() + " from course...");
    for (size_t i = 0; i < _teachers.size(); ++i) {
        if (_teachers[i] == teacher) {
            _adminsys->log(_id, "Removing section from teacher record.");
            _adminsys->teacher(teacher).access().lock()->remove_course(_id);
            _teachers.erase(_teachers.begin() + i);
            break;
        }
    }
}

auto Course_Section::assign_assistant(UID student) -> bool
{
    _adminsys->log(_id, "Attempting to assign student " + student.as_string() + " as assistant...");

    if (!_active) {
        _adminsys->log(_id, Log_Level::Error, "Failed - Course closed.");
        return false;
    }

    for (const auto& id : _assistants) {
        if (id == student) {
            _adminsys->log(_id, Log_Level::Warning, "Assistant already assigned. Skipping");
            return true;
        }
    }

    if (can_assist(student)) {
        _adminsys->log(_id, "Adding course section to assistant...");
        _adminsys->student(student).access().lock()->assign_as_assistant(_id);
        _assistants.push_back(student);
        _adminsys->log(_id, "Successfully assigned assistant to course.");
        return true;
    } else {
        _adminsys->log(_id, Log_Level::Error, "Failed - student inelligible to assist this course.");
        return false;
    }
}

auto Course_Section::remove_assistant(UID student) -> void
{
    _adminsys->log(_id, "Removing assistant " + student.as_string() + " from course...");
    for (size_t i = 0; i < _students.size(); ++i) {
        if (_assistants[i] == student) {
            _adminsys->log(_id, "Removing section from student record.");
            _adminsys->student(student).access().lock()->remove_from_assisting(_id);
            _assistants.erase(_assistants.begin() + i);
            break;
        }
    }
}

auto Course_Section::enroll_student(UID student) -> bool
{
    _adminsys->log(_id, "Attempting to enroll student " + student.as_string() + "...");

    if (!_active) {
        _adminsys->log(_id, Log_Level::Error, "Failed - Course closed.");
        return false;
    }

    if (_students.size() == _seats) {
        _adminsys->log(_id, "Failed - Course already full.");
    }

    for (const auto& id : _students) {
        if (id == student) {
            _adminsys->log(_id, Log_Level::Warning, "Student already assigned. Skipping");
            return true;
        }
    }

    if (can_enroll(student)) {
        _adminsys->log(_id, "Adding course section to student...");
        _adminsys->student(student).access().lock()->add_course(_id);
        _students.push_back(student);
        _adminsys->log(_id, "Generating gradebook for student");
        _grade_book.emplace(student, std::unordered_map<AID, Assignment_Grade, AID::hash>());
        for (const auto& assignment : _assignments) {
            _grade_book[student].emplace(assignment.first, 
                                         Assignment_Grade{false, 0, assignment.second.max_score});
        }
        _adminsys->log(_id, "Successfully enrolled student.");
        return true;
    } else {
        _adminsys->log(_id, Log_Level::Error, "Failed - student inelligible for this course.");
        return false;
    }
}

auto Course_Section::remove_student(UID student) -> bool
{
    _adminsys->log(_id, "Removing student " + student.as_string() + " from course...");
    for (size_t i = 0; i < _students.size(); ++i) {
        if (_students[i] == student) {
            _adminsys->log(_id, "Removing section from student record.");
            auto student_ref = _adminsys->student(student);
            if (student_ref.exists()) {
                student_ref.access().lock()->remove_course(_id);
            } else {
                _adminsys->log(_id, "Failed - no such student exists");
                return false;
            }
            _adminsys->log(_id, "Removing student from grade book.");
            _grade_book.erase(student);
            _adminsys->log(_id, "Removing student from record.");
            _students.erase(_students.begin() + i);
            break;
        }
    }
    return true;
}


auto Course_Section::teachers() const -> std::vector<UID>
{
    return _teachers;
}

auto Course_Section::assistants() const -> std::vector<UID>
{
    return _assistants;
}

auto Course_Section::students() const -> std::vector<UID>
{
    return _students;
}

auto Course_Section::is_teaching(UID teacher) const -> bool
{
    for (const auto& id : _teachers) {
        if (id == teacher) {
            return true;
        }
    }
    return false;
}

auto Course_Section::is_assisting(UID student) const -> bool
{
    for (const auto& id : _assistants) {
        if (id == student) {
            return true;
        }
    }
    return false;
}

auto Course_Section::is_enrolled(UID student) const -> bool
{
    for (const auto& id : _students) {
        if (id == student) {
            return true;
        }
    }
    return false;
}


auto Course_Section::assignments() const -> std::vector<Assignment>
{
    std::vector<Assignment> result;
    for (const auto& assignment : _assignments) {
        result.push_back(assignment.second);
    }
    return result;
}

auto Course_Section::assignment(AID assignment) const -> util::Maybe<Assignment>
{
    if (_assignments.count(assignment)) {
        return util::Maybe<Assignment>(_assignments.at(assignment));
    } else {
        return util::Maybe<Assignment>();
    }
}

auto Course_Section::create_assignment(std::string name, float max_score, float weight) 
        -> util::Maybe<AID>
{
    _adminsys->log(_id, "Creating new assignment definition...");

    if (!_active) {
        _adminsys->log(_id, Log_Level::Error, "Failed - Course closed.");
        return util::Maybe<AID>();
    }

    _adminsys->log(_id, "Generating new Assignment ID.");
    auto id = _aid_factory.generate_id();
    _adminsys->log(_id, "Mapping new assignment.");
    _assignments[id] = Assignment{id, name, max_score, weight};

    _adminsys->log(_id, "Creating placeholder grades...");
    for (const auto& student : _students) {
        _grade_book[student][id] = Assignment_Grade{false, 0, max_score};
    }

    _adminsys->log(_id, "Assignment successfully created with id " + id.as_string());
    return util::Maybe<AID>(id);
}

auto Course_Section::remove_assignment(AID assignment) -> void
{
    _adminsys->log(_id, "Removing assignment " + assignment.as_string() + "...");

    if (!_active) {
        _adminsys->log(_id, Log_Level::Error, "Failed - Course closed.");
        return;
    }

    _adminsys->log(_id, "Purging associated assignment grades...");
    for (const auto& student : _students) {
        _grade_book[student].erase(assignment);
    }

    _adminsys->log(_id, "Removing assignment definition...");
    _assignments.erase(assignment);

    _adminsys->log(_id, "Successfully removed assignment");
}


auto Course_Section::grade_book() const -> Grade_Book
{
    return _grade_book;
}

auto Course_Section::gpa(UID student) const -> util::Maybe<float>
{
    _adminsys->log(_id, "Calculating GPA for student " + student.as_string());

    float earned = 0.0;
    float max    = 0.0;
    for (const auto& id : _students) {
        if (id == student) {
            for (const auto& grade : _grade_book.at(student)) {
                if (grade.second.graded) {
                    earned += grade.second.earned_score * _assignments.at(grade.first).weight;
                    max    += grade.second.max_score    * _assignments.at(grade.first).weight; 
                }
            }

            if (max <= 0.0) {
                _adminsys->log(_id, Log_Level::Error, "Invalid grade!");
                return util::Maybe<float>();
            }

            float gpa;
            float percent = earned / max;
            if (percent > 0.94) {
                gpa = 4.00;
            } else if (percent > 0.90) {
                gpa = 3.67;
            } else if (percent > 0.87) {
                gpa = 3.33;
            } else if (percent > 0.84) {
                gpa = 3.00;
            } else if (percent > 0.80) {
                gpa = 2.67;
            } else if (percent > 0.77) {
                gpa = 2.33;
            } else if (percent > 0.74) {
                gpa = 2.00;
            } else if (percent > 0.70) {
                gpa = 1.67;
            } else if (percent > 0.67) {
                gpa = 1.33;
            } else if (percent > 0.64) {
                gpa = 1.00;
            } else if (percent > 0.60) {
                gpa = 0.67;
            } else {
                gpa = 0.0;
            }
            return util::Maybe<float>(gpa);
        }
    }
    _adminsys->log(_id, Log_Level::Error, "Could not find student.");
    return util::Maybe<float>();
}

auto Course_Section::grade_assignment(UID student, AID assignment, float score) -> bool
{
    _adminsys->log(_id, "Setting score to " + std::to_string(score) + " on assignment " + 
                        assignment.as_string() + " for student " + student.as_string() + "...");

    if (!_active) {
        _adminsys->log(_id, Log_Level::Error, "Failed - Course closed.");
        return false;
    }

    if (is_enrolled(student) && _assignments.count(assignment) > 0) {
        _grade_book[student][assignment].graded = true;
        _grade_book[student][assignment].earned_score = score;
        return true;
    } else {
        _adminsys->log(_id, Log_Level::Error, "Failed - no such student or assignment in course.");
        return false;
    }
}

auto Course_Section::assignment_grade(UID student, AID assignment) const -> util::Maybe<Assignment_Grade>
{
    if (_grade_book.count(student) > 0) {
        if (_grade_book.at(student).count(assignment) > 0) {
            return util::Maybe<Assignment_Grade>(true, _grade_book.at(student).at(assignment));
        }
    }
    return util::Maybe<Assignment_Grade>();
}


}