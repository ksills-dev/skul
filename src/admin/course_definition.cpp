#include "admin/course_definition.hpp"

#include "admin/admin_db.hpp"

namespace admin
{
    
Course_Definition::Course_Definition(Admin_DB* const adminsys, CID id, DID department,
                      std::string name, std::string number, unsigned int credits)
        : _adminsys(adminsys), _id(id), _department(department), _name(name),  
          _number(number), _credits(credits),
          _retired(false), _prereqs(), _coreqs(), _equivalencies()
{
}

Course_Definition::Course_Definition(Admin_DB* const adminsys, CID id, DID department,
                    std::string name, std::string number, unsigned int credits,
                    std::vector<CID> prereqs, std::vector<CID> coreqs, 
                    std::vector<CID> equivalencies, bool retired)
        : _adminsys(adminsys), _id(id), _department(department), _name(name), 
          _number(number), _credits(credits),
          _retired(retired), _prereqs(prereqs), _coreqs(coreqs), _equivalencies(equivalencies)
{
}

auto Course_Definition::from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Course_Definition>
{
    adminsys->log("Attempting to parse Course_Definition from SDF_Node.");

    Course_Definition course(adminsys);
    bool id_found = false;
    bool department_found = false;
    bool name_found = false;
    bool number_found = false;
    bool credits_found = false;
    bool retired_found = false;
    bool prereqs_found = false;
    bool coreqs_found = false;
    bool equivalencies_found = false;

    for (const auto& node : branch) {
        if (node.name() == "id") {
            adminsys->log("Found id.");
            auto node_int = node.as_int();
            if (node_int.exists()) {
                course._id = CID(node_int.access());
                id_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Course_Definition>();
            }
        }
        else if (node.name() == "department") {
            adminsys->log("Found department.");
            auto node_int = node.as_int();
            if (node_int.exists()) {
                course._department = DID(node_int.access());
                department_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Course_Definition>();
            }
        }
        else if (node.name() == "name") {
            adminsys->log("Found name.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Course_Definition>();
            } else {
                course._name = node.as_string().access();
                name_found = true;
            }
        }   
        else if (node.name() == "number") {
            adminsys->log("Found number.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Course_Definition>();
            } else {
                course._number = node.as_string().access();
                number_found = true;
            }
        }
        else if (node.name() == "credits") {
            adminsys->log("Found credits.");
            auto node_int = node.as_int();
            if (node_int.exists()) {
                course._credits = node_int.access();
                credits_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Course_Definition>();
            }
        }
        else if (node.name() == "retired") {
            adminsys->log("Found retired.");
            auto node_bool = node.as_bool();
            if (node_bool.exists()) {
                course._retired = node_bool.access();
                retired_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Course_Definition>();
            }
        }
        else if (node.name() == "prereqs") {
            adminsys->log("Found prereqs.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Course_Definition>();
            } else {
                course._prereqs = parse_id_list<CID>(node.as_string().access());
                prereqs_found = true;
            }
        }
        else if (node.name() == "coreqs") {
            adminsys->log("Found coreqs.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Course_Definition>();
            } else {
                course._coreqs = parse_id_list<CID>(node.as_string().access());
                coreqs_found = true;
            }
        }
        else if (node.name() == "equivalencies") {
            adminsys->log("Found equivalencies.");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Course_Definition>();
            } else {
                course._equivalencies = parse_id_list<CID>(node.as_string().access());
                equivalencies_found = true;
            }
        }
        else {
            adminsys->log(Log_Level::Warning, "Unknown entry " + node.name() + ". Skipping.");
        }
    }

    if (!id_found || !department_found || !name_found || !number_found || !credits_found ||
            !retired_found || !prereqs_found || !coreqs_found || !equivalencies_found) {
        adminsys->log(Log_Level::Error, "Missing necessary entry for Course_Definition.");
        return util::Maybe<Course_Definition>();
    }
    
    adminsys->log("Successfully parsed Course_Definition.");
    return util::Maybe<Course_Definition>(course);
}

auto Course_Definition::to_sdf(const Course_Definition& course) -> sdf::SDF_Node
{
    course._adminsys->log(course._id, "Generating Node for Course Definition...");
    sdf::SDF_Branch result;

    result.emplace_back("id", course._id.as_string());
    result.emplace_back("department", course._department.as_string());
    result.emplace_back("name", course._name);
    result.emplace_back("number", course._number);
    result.emplace_back("credits", std::to_string(course._credits));
    result.emplace_back("retired", (course._retired) ? "true" : "false");
    result.emplace_back("prereqs", write_id_list(course._prereqs));
    result.emplace_back("coreqs", write_id_list(course._coreqs));
    result.emplace_back("equivalencies", write_id_list(course._equivalencies));

    course._adminsys->log(course._id, "Successfully generated Node for Course Definition.");
    return sdf::SDF_Node("Course_Definition", result);
}


auto Course_Definition::id() const -> CID
{
    return _id;
}

auto Course_Definition::name() const -> std::string
{
    return _name;
}

auto Course_Definition::number() const -> std::string
{
    return _number;
}

auto Course_Definition::department() const -> DID
{
    return _department;
}

auto Course_Definition::prereqs() const -> std::vector<CID>
{
    return _prereqs;
}

auto Course_Definition::coreqs() const -> std::vector<CID>
{
    return _coreqs;
}

auto Course_Definition::equivalencies() const -> std::vector<CID>
{
    return _equivalencies;
}

auto Course_Definition::credits() const -> unsigned int
{
    return _credits;
}

auto Course_Definition::retired() const -> bool
{
    return _retired;
}

auto Course_Definition::is_equivalent(CID course_def) const -> bool
{
    for (const auto& equivalent : _equivalencies) {
        if (equivalent == course_def) {
            return true;
        }
    }
    return false;
}


auto Course_Definition::can_teach(UID teacher) const -> bool
{
    auto teacher_ref = _adminsys->teacher(teacher);
    if (teacher_ref.exists()) {
        return true;
    }
    return false;
}

auto Course_Definition::can_assist(UID student) const -> bool
{
    auto student_ref = _adminsys->student(student);
    if (student_ref.exists()) {
        auto student_instance = student_ref.access().lock();
        if (student_instance->can_assist()) {
            return true;
        }
    }
    return false;
}

auto Course_Definition::can_enroll(UID student) const -> bool
{
    auto student_ref = _adminsys->student(student);
    if (student_ref.exists()) {
        auto student_instance = student_ref.access().lock();

        // Prerequisites
        for (const auto& course : _prereqs) {
            if (!student_instance->has_taken(course)) {
                return false;
            }
        }

        // Coerequisites
        for (const auto& course : _coreqs) {
            if (!student_instance->has_taken(course) && // Gotta get that De Morgan's Law
                !student_instance->is_enrolled_in(course)) {
                return false;
            }
        }

        return true;
    } else {
        return false;
    }
}


auto Course_Definition::add_equivalency(CID course_def) -> void
{
    _adminsys->log(_id, "Adding course equivalency with " + course_def.as_string());

    auto def_ref = _adminsys->course_def(course_def);
    if (def_ref.exists()) {
        _adminsys->log(_id, "Successfully added equivalency.");
        _equivalencies.push_back(_id);
    } else {
        _adminsys->log(_id, Log_Level::Error, "Failed - no such equivalent course.");
    }
}

auto Course_Definition::remove_equivalency(CID course_def) -> void
{
    _adminsys->log(_id, "Removing course equivalency with " + course_def.as_string());

    auto def_ref = _adminsys->course_def(course_def);
    if (def_ref.exists()) {
        // Remove equivalency from this course
        for (size_t i = 0; i < _equivalencies.size(); ++i) {
            if (_equivalencies[i] == course_def) {
                _equivalencies.erase(_equivalencies.begin() + i);
                _adminsys->log(_id, "Successfully removed course equivalency.");
                break;
            }
        } 
    } else {
        _adminsys->log(_id, Log_Level::Error, "Failed - no such equivalent course.");
    }
}

auto Course_Definition::retire() -> void
{
    _adminsys->log(_id, "Retiring this course from service. Semper Fi.");
    _retired = true;
}

auto Course_Definition::change_department(DID new_department) -> void
{
    _adminsys->log(_id, "Changing department from " + _department.as_string()  + " to " + 
                        new_department.as_string());
    _department = new_department; 
}

}
