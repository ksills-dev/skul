#include "admin/student.hpp"

#include "admin/admin_db.hpp"

#include <regex>

namespace admin
{

Student::Student(const Registered_Person person, Grade_Level grade)
    : Registered_Person(person), _grade(grade),
      _current_research(), _completed_research(), 
      _current_ta_courses(), _completed_ta_courses(),
      _enrolled_courses(), _completed_courses() 
{
}

Student::Student(const Registered_Person person, Grade_Level grade,
        std::vector<RID> current_research, std::vector<RID> completed_research,
        std::vector<SID> current_ta_courses, std::vector<SID> completed_ta_courses,
        std::vector<SID> enrolled_courses, std::vector<SID> completed_courses)
    : Registered_Person(person), _grade(grade), 
      _current_research(current_research), _completed_research(completed_research),
      _current_ta_courses(current_ta_courses), _completed_ta_courses(completed_ta_courses),
      _enrolled_courses(enrolled_courses), _completed_courses(completed_courses)
{
}

// These are the first sdf conversion functions I wrote. They're not great. Forgive me.
auto Student::from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Student>
{
    adminsys->log("Attempting to parse Student.");

    Registered_Person* base_ptr = nullptr; // See below for details.
    bool base_found = false;
    Grade_Level grade = Grade_Level::Freshman;
    bool grade_found = false;
    std::vector<RID> current_research;
    bool current_research_found = false;
    std::vector<RID> completed_research;
    bool completed_research_found = false;
    std::vector<SID> current_ta_courses;
    bool current_ta_courses_found = false;
    std::vector<SID> completed_ta_courses;
    bool completed_ta_courses_found = false;
    std::vector<SID> current_courses;
    bool current_courses_found = false;
    std::vector<SID> completed_courses;
    bool completed_courses_found = false;
    

    for (const auto& node : branch) {
        if (node.name() == "Registered_Person") {
            adminsys->log("Found Registered_Person");
            if (node.kind() == sdf::SDF_Node_Type::Value) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.'");
                return util::Maybe<Student>();
            }
            auto child = Registered_Person::from_sdf(adminsys, node.branch().access());
            if (child.exists()) {
                base_ptr = new Registered_Person(child.access());
                base_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Student>();
            }
        }
        else if (node.name() == "grade") {
            adminsys->log("Found grade_level");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log("But it's ill-formed.");
                return util::Maybe<Student>();
            }
            auto node_string = node.as_string().access();
            if (node_string == "freshman") {
                grade = Grade_Level::Freshman;
            }
            else if (node_string == "sophmore") {
                grade = Grade_Level::Sophmore;
            }
            else if (node_string == "junior") {
                grade = Grade_Level::Junior;
            }
            else if (node_string == "senior") {
                grade = Grade_Level::Senior;
            }
            else if (node_string == "masters") {
                grade = Grade_Level::Masters;
            }
            else if (node_string == "doctorate") {
                grade = Grade_Level::Doctorate;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Student>();
            }
            grade_found = true;
        }
        else if (node.name() == "current_research") {
            adminsys->log("Found current_research");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Student>();
            }
            current_research = parse_id_list<RID>(node.as_string().access());
            current_research_found = true;
        }
        else if (node.name() == "completed_research") {
            adminsys->log("Found completed_research");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Student>();
            }
            completed_research = parse_id_list<RID>(node.as_string().access());
            completed_research_found = true;
        }
        else if (node.name() == "current_ta_courses") {
            adminsys->log("Found current_ta_courses");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Student>();
            }
            current_ta_courses = parse_id_list<SID>(node.as_string().access());
            current_ta_courses_found = true;
        }
        else if (node.name() == "completed_ta_courses") {
            adminsys->log("Found completed_ta_courses");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Student>();
            }
            completed_ta_courses = parse_id_list<SID>(node.as_string().access());
            completed_ta_courses_found = true;
        }
        else if (node.name() == "current_courses") {
            adminsys->log("Found current_courses");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed.");
                return util::Maybe<Student>();
            }
            current_courses = parse_id_list<SID>(node.as_string().access());
            current_courses_found = true;
        }
        else if (node.name() == "completed_courses") {
            adminsys->log("Found completed_courses");
            if (node.kind() == sdf::SDF_Node_Type::Branch) {
                adminsys->log(Log_Level::Error, "But it's ill-formed");
                return util::Maybe<Student>();
            }
            completed_courses = parse_id_list<SID>(node.as_string().access());
            completed_courses_found = true;
        } 
        else {
            adminsys->log(Log_Level::Warning, "Unknown entry " + node.name() + ". Skipping.");
        }
    }

    // Since a default assignment operator would be ill-formed (const adminsys member)
    // I create a copy of the base into a ptr, then copy that into a stack object.
    // Makes two copies, one of which is on the heap, but that's fine for now.
    Registered_Person base{*base_ptr};
    delete base_ptr;

    if      (!base_found || !grade_found || !current_research_found || !completed_research_found ||
             !current_ta_courses_found || !completed_ta_courses_found || !current_courses_found ||
             !completed_courses_found) {
        adminsys->log(Log_Level::Error, "Missing necessary entry for Student."); 
        return util::Maybe<Student>(); 
    }
    adminsys->log("Succesfully parsed Student.");
    return util::Maybe<Student>(true, Student(base, grade, current_research, completed_research, 
                                              current_ta_courses, completed_ta_courses, 
                                              current_courses, completed_courses));
}

auto Student::to_sdf(const Student& student) -> sdf::SDF_Node
{
    student._adminsys->log(student._id, "Generating SDF_Node from Student.");

    sdf::SDF_Branch result;
    auto registered_person_node = Registered_Person::to_sdf(student);
    result.push_back(registered_person_node);

    switch (student.grade_level()) {
    case Grade_Level::Freshman:
        result.emplace_back(std::string("grade"), std::string("freshman"));
        break;
    case Grade_Level::Sophmore:
        result.emplace_back(std::string("grade"), std::string("sophmore"));
        break;
    case Grade_Level::Junior:
        result.emplace_back(std::string("grade"), std::string("junior"));
        break;
    case Grade_Level::Senior:
        result.emplace_back(std::string("grade"), std::string("senior"));
        break;
    case Grade_Level::Masters:
        result.emplace_back(std::string("grade"), std::string("masters"));
        break;
    case Grade_Level::Doctorate:
        result.emplace_back(std::string("grade"), std::string("doctorate"));
        break;
    }

    result.emplace_back(std::string("current_research"), 
                        write_id_list<RID>(student._current_research));
    result.emplace_back(std::string("completed_research"), 
                        write_id_list<RID>(student._completed_research));
    result.emplace_back(std::string("current_ta_courses"), 
                        write_id_list<SID>(student._current_ta_courses));
    result.emplace_back(std::string("completed_ta_courses"), 
                        write_id_list<SID>(student._completed_ta_courses));
    result.emplace_back(std::string("current_courses"), 
                        write_id_list<SID>(student._enrolled_courses));
    result.emplace_back(std::string("completed_courses"), 
                        write_id_list<SID>(student._completed_courses));

    student._adminsys->log(student._id, "Successfully generated SDF_Node from Student.");
    return sdf::SDF_Node("Student", result);
}

auto Student::grade_level() const -> Grade_Level
{
    return _grade;
}

auto Student::is_assistant() const -> bool 
{
    return (_current_research.size() > 0 || _current_ta_courses.size() > 0);  
}

auto Student::is_teaching_assistant() const -> bool
{
    return (_current_ta_courses.size() > 0);
}

auto Student::is_research_assistant() const -> bool
{
    return (_current_research.size() > 0);
}

auto Student::change_grade_level(const Grade_Level grade) -> void
{
    auto log_message = std::string("Changing grade level of student to ");
    switch (_grade) {
    case Grade_Level::Freshman:
        log_message.append("Freshman");
        break;
    case Grade_Level::Sophmore:
        log_message.append("Sophmore");
        break;
    case Grade_Level::Junior:
        log_message.append("Junior");
        break;
    case Grade_Level::Senior:
        log_message.append("Senior");
        break;
    case Grade_Level::Masters:
        log_message.append("Masters");
        break;
    case Grade_Level::Doctorate:
        log_message.append("Doctorate");
        break;
    }
    _adminsys->log(_id, log_message);
    _grade = grade;
}

auto Student::enroll_course(SID section) -> bool
{
    _adminsys->log(_id, "Attempting to enroll in course section" + section.as_string());
    auto course = _adminsys->course(section);
    if (course.exists()) {
        return course.access().lock()->enroll_student(_id);
    } else {
        _adminsys->log(_id, Log_Level::Error, "No such course exists.");
        return false;
    }
}

auto Student::withdraw_course(SID section) -> bool
{
    _adminsys->log(_id, "Attempting to withdraw from " + section.as_string());
    auto course = _adminsys->course(section);
    if (course.exists()) {
        return course.access().lock()->remove_student(_id);
    } else {
        _adminsys->log(_id, Log_Level::Error, "No such course section exists.");
        return false;
    }
}

auto Student::enrolled_courses() const -> std::vector<SID>
{
    return _enrolled_courses;
}

auto Student::course_history() const -> std::vector<SID> 
{
    return _completed_courses;
}

auto Student::is_enrolled_in(SID section) const -> bool
{
    for (const auto& id : _enrolled_courses) {
        if (id == section) {
            return true;
        }
    }
    return false;
}

auto Student::has_taken(SID section) const -> bool
{
    for (const auto& id : _completed_courses) {
        if (id == section) {
            return true;
        }
    }
    return false;
}

auto Student::is_enrolled_in(CID course) const -> bool
{
    for (const auto& id : _enrolled_courses) {
        auto section_ref = _adminsys->course(id);
        if (section_ref.exists()) {
            auto definition = section_ref.access().lock()->definition();
            if (definition == course) {
                return true;
            } else {
                auto course_ref = _adminsys->course_def(definition);
                if (course_ref.exists()) {
                    if (course_ref.access().lock()->is_equivalent(course)) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

auto Student::has_taken(CID course) const -> bool
{
    for (const auto& id : _completed_courses) {
        // Get instance
        auto section_ref = _adminsys->course(id);
        if (section_ref.exists()) {
            auto definition = section_ref.access().lock()->definition();
            if (definition == course) { // This course is exactly right
                return true;
            } else {                    // Check equivalencies
                auto course_ref = _adminsys->course_def(definition);
                if (course_ref.exists()) {
                    if (course_ref.access().lock()->is_equivalent(course)) {
                        return true;
                    } 
                }
            }
        }
    }
    return false;
}

auto Student::assisting_courses() const -> std::vector<SID>
{
    return _current_ta_courses;
}

auto Student::past_assisting_courses() const -> std::vector<SID>
{
    return _completed_ta_courses;
}

auto Student::assisting_research() const -> std::vector<RID>
{
    return _current_research;
}

auto Student::past_assisting_research() const -> std::vector<RID>
{
    return _completed_research;
}

auto Student::is_assisting(SID section) const -> bool
{
    for (const auto& id : _current_ta_courses) {
        if (id == section) {
            return true;
        }
    }
    return false;
}

auto Student::is_assisting(RID project) const -> bool
{
    for (const auto& id : _current_research) {
        if (id == project) {
            return true;
        }
    }
    return false;
}

auto Student::can_assist() const -> bool
{
    // If this is so wrong, why does it feel so right?
    switch (_grade) {
    case Grade_Level::Freshman:
    case Grade_Level::Sophmore:
    case Grade_Level::Junior:
    case Grade_Level::Senior:
        return false;
        break;
    case Grade_Level::Masters:
    case Grade_Level::Doctorate:
        return true;
        break;
    }
    return false;
}

auto Student::can_assist(SID section) const -> bool
{
    if (!can_assist()) return false;

    auto course = _adminsys->course(section);
    if (course.exists()) {
        return course.access().lock()->can_assist(_id);
    } else {
        return false;
    }   
}

auto Student::can_assist(RID project) const -> bool
{
    if (!can_assist()) return false;

    auto project_ref = _adminsys->research(project);
    if (project_ref.exists()) {
        return project_ref.access().lock()->can_research(_id);
    } else {
        return false;
    }
}

auto Student::enroll_as_assistant(SID section) -> bool
{
    _adminsys->log("Attempting to enroll as assistant in course " + section.as_string());

    if (!can_assist(section)) {
        _adminsys->log(_id, Log_Level::Error, "Not elligible for assisting.");
        return false;
    }

    auto section_ref = _adminsys->course(section);
    if (section_ref.exists()) {
        return section_ref.access().lock()->assign_assistant(_id);
    } else {
        _adminsys->log(_id, Log_Level::Error, "No such course exists.");
        return false;
    }
}

auto Student::enroll_as_assistant(RID project) -> bool
{
    _adminsys->log("Attempting to enroll as assistant in research " + project.as_string());

    if (!can_assist(project)) {
        _adminsys->log(_id, Log_Level::Error, "Not elligible for assisting.");
        return false;
    }

    auto project_ref = _adminsys->research(project);
    if (project_ref.exists()) {
        return project_ref.access().lock()->assign_assistant(_id);
    } else {
        _adminsys->log(_id, Log_Level::Error, "No such course exists.");
        return false;
    }
}

auto Student::withdraw_from_assisting(SID section) -> bool
{
    _adminsys->log("Attempting to withdraw from assisting course " + section.as_string());

    if (!is_assisting(section)) {
        _adminsys->log(_id, Log_Level::Warning, "Already not assisting this course. Skipping");
        return true;
    }

    auto section_ref = _adminsys->course(section);
    if (section_ref.exists()) {
        section_ref.access().lock()->remove_assistant(_id);
        return true;
    } else {
        _adminsys->log(_id, Log_Level::Error, "No such course exists.");
        return false;
    }
}

auto Student::withdraw_from_assisting(RID project) -> bool
{
    _adminsys->log("Attempting to withdraw from assisting research " + project.as_string());

    if (!is_assisting(project)) {
        _adminsys->log(_id, Log_Level::Error, "Already not assisting research. Skipping");
        return false;
    }

    auto project_ref = _adminsys->research(project);
    if (project_ref.exists()) {
        project_ref.access().lock()->remove_assistant(_id);
        return true;
    } else {
        _adminsys->log(_id, Log_Level::Error, "No such research exists.");
        return false;
    }
}

auto Student::deactivate() -> void
{
    _adminsys->log(_id, "Deactivating person...");
    Registered_Person::deactivate();

    _adminsys->log(_id, "Withdrawing from research projects...");
    for (auto& research : _current_research) {
        withdraw_from_assisting(research);
    }

    _adminsys->log(_id, "Withdrawing from ta courses...");
    for (auto& course : _current_ta_courses) {
        withdraw_from_assisting(course);
    }

    _adminsys->log(_id, "Withdrawing from enrolled courses...");
    for (auto& course : _enrolled_courses) {
        withdraw_course(course);
    }

    _adminsys->log(_id, "Purge complete. Successfully deactivated.");
}

auto Student::add_course(SID section) -> void
{
    _enrolled_courses.push_back(section);
}

auto Student::assign_as_assistant(SID section) -> void
{
    _current_ta_courses.push_back(section);
}

auto Student::assign_as_assistant(RID project) -> void
{
    _current_research.push_back(project);
}

auto Student::remove_course(SID section) -> void
{
    for (size_t i = 0; i < _enrolled_courses.size(); ++i) {
        if (_enrolled_courses[i] == section) {
            _enrolled_courses.erase(_enrolled_courses.begin() + i);
            break;
        }
    }
}

auto Student::remove_from_assisting(SID section) -> void
{
    for (size_t i = 0; i < _current_ta_courses.size(); ++i) {
        if (_current_ta_courses[i] == section) {
            _current_ta_courses.erase(_current_ta_courses.begin() + i);
            break;
        }
    }
}

auto Student::remove_from_assisting(RID project) -> void
{
    for (size_t i = 0; i != _current_research.size(); ++i) {
        if (_current_research[i] == project) {
            _current_research.erase(_current_research.begin() + i);
            break;
        }
    }
}

auto Student::finalize_course(SID section) -> void
{
    remove_course(section);
    _completed_courses.push_back(section);
}

auto Student::finalize_assisting_course(SID section) -> void
{
    remove_from_assisting(section);
    _completed_ta_courses.push_back(section);
}

auto Student::finalize_assisting_research(RID project) -> void
{
    remove_from_assisting(project);
    _completed_research.push_back(project);
}

}