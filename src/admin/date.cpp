#include "admin/date.hpp"

#include "admin/admin_db.hpp"

#include <iostream>

namespace 
{

const auto january_length = 31;
const auto february_length = 28;
const auto february_leap_length = 29;
const auto march_length = 31;
const auto april_length = 30;
const auto may_length = 31;
const auto june_length = 30;
const auto july_length = 31;
const auto august_length = 31;
const auto september_length = 30;
const auto october_length = 31;
const auto november_length = 30;
const auto december_length = 31;

// Both of these rendered obselete by use of full (Day, Month, Year) format.
// <Removed 09-05-2018>
// const auto year_length = 365;
// const auto year_leap_length = 366;

auto leap_year(int year) -> bool
{
    return ((year % 4 == 0 && !(year % 100 != 0)) || (year % 400 == 0));
}

auto month_valid(int month) -> bool
{
    return ((month > 0) && (month <= 12));
}

auto month_length(int year, int month) -> int
{
    switch (month) {
    case 1:
        return january_length;
        break;
    case 2:
        return (leap_year(year)) ? february_length : february_leap_length;
        break;
    case 3:
        return march_length;
        break;
    case 4:
        return april_length;
        break;
    case 5:
        return may_length;
        break;
    case 6:
        return june_length;
        break;
    case 7:
        return july_length;
        break;
    case 8:
        return august_length;
        break;
    case 9:
        return september_length;
        break;
    case 10:
        return october_length;
        break;
    case 11:
        return november_length;
        break;
    case 12:
        return december_length;
        break;
    }
    return 0;
}

auto day_valid(int year, int month, int day) -> bool
{
    return ((day > 0) && (day <= month_length(year, month)));
}

}

namespace admin 
{

Date::Date(int year, int month, int day)
        : _year(year), _month(month), _day(day)
{
    if (!month_valid(month)) throw new Invalid_Date(year, month, day);
    if (!day_valid(year, month, day)) throw new Invalid_Date(year, month, day);
}

Date::Date(const Date& other)
    : _year(other.year()), _month(other.month()), _day(other.day())
{
}


auto Date::from_sdf(Admin_DB* const adminsys, const sdf::SDF_Branch& branch) -> util::Maybe<Date>
{
    adminsys->log("Attempting to parse Date from SDF_Node.");

    int year = 0;
    bool year_found = false;
    int month = 0;
    bool month_found = false;
    int day = 0;
    bool day_found = false;

    for (const auto& node : branch) {
        if (node.name() == "year") {
            adminsys->log("Found year.");
            auto response = node.as_int();
            if (response.exists()) {
                year = response.access();
                year_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.'");
                return util::Maybe<Date>();
            }
        }
        else if (node.name() == "month") {
            adminsys->log("Found month");
            auto response = node.as_int();
            if (response.exists()) {
                month = response.access();
                month_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.'");
                return util::Maybe<Date>();
            }
        }
        else if (node.name() == "day") {
            adminsys->log("Found day.");
            auto response = node.as_int();
            if (response.exists()) {
                day = response.access();
                day_found = true;
            } else {
                adminsys->log(Log_Level::Error, "But it's ill-formed.'");
                return util::Maybe<Date>();
            }
        }
        else {
            adminsys->log(Log_Level::Warning, "Unknown entry " + node.name() + ". Skipping.");
        }
    }

    if (!year_found || !month_found || !day_found) {
        adminsys->log(Log_Level::Error, "Missing necessary entry for Date."); 
        return util::Maybe<Date>(); 
    };
    adminsys->log("Successfully parsed Date.");
    return util::Maybe<Date>(Date(year, month, day));
}

auto Date::to_sdf(Admin_DB* const adminsys, const Date& date) -> sdf::SDF_Node
{
    adminsys->log("Generating SDF_Node from Date.");
    sdf::SDF_Branch result;

    result.emplace_back("year", std::to_string(date.year()));
    result.emplace_back("month", std::to_string(date.month()));
    result.emplace_back("day", std::to_string(date.day()));

    adminsys->log("Successfully generated SDF_Node from Date");
    return sdf::SDF_Node("Date", result);
}


auto Date::year(const int year) -> void
{
    _year = year;
}

auto Date::year() const -> int
{
    return _year;
}

auto Date::month(const int month) -> void
{
    _month = month;
}

auto Date::month() const -> int
{
    return _month;
}

auto Date::day(const int day) -> void
{
    _day = day;
}

auto Date::day() const -> int
{
    return _day;
}


// This is incredibly inefficient because I'm not using it in the project.
// There's obviously a better way to calculate this, and that should be used.
auto Date::distance(const Date& other) const -> int
{
    int result = 0;

    Date larger;
    Date smaller;
    if (*this > other) {
        larger = *this;
        smaller = other;
    } else {
        larger = other;
        smaller = *this;
    }

    while (smaller++ < larger) {
        result += 1;
    }

    return result;
}

auto Date::is_leap_year() const -> bool
{
    return leap_year(_year);
}


auto Date::operator ++() -> Date&
{
    if (!day_valid(_year, _month, ++_day)) {
         ++_month;
        _day = 1;
    }
    return *this;
}

auto Date::operator ++(int) -> Date
{
    Date original = *this;
    if (!day_valid(_year, _month, ++_day)) {
         ++_month;
        _day = 1;
    }
    return original;
}

auto Date::operator --() -> Date&
{
    if (!day_valid(_year, _month, --_day)) {
         --_month;
        _day = month_length(_year, _month);
    }
    return *this;
}

auto Date::operator --(int) -> Date
{
    Date original = *this;
    if (!day_valid(_year, _month, --_day)) {
         --_month;
        _day = month_length(_year, _month);;
    }
    return original;
}


auto Date::operator +(const int days) -> Date
{
    Date result = *this;
    result._day += days;
    while (!day_valid(result._year, result._month, result._day)) {
        result._day -= month_length(result._year, result._month);
        ++result._month;
        if (result._month > 12) {
            result._year = 1;
            result._month = 1;
        }
    }
    return result;
}

auto Date::operator -(const int days) -> Date
{
    Date result = *this;
    result._day -= days;
    while (!day_valid(result._year, result._month, result._day)) {
        --result._month;
        if (result._month < 1) {
            result._year = 1;
            result._month = 1;
        }
        result._day += month_length(result._year, result._month);
    }
    return result;
}

auto Date::operator +=(const int days) -> Date&
{
    _day += days;
    while (!day_valid(_year, _month, _day)) {
        _day -= month_length(_year, _month);
        ++_month;
        if (_month > 12) {
            _year = 1;
            _month = 1;
        }
    }
    return *this;
}

auto Date::operator -=(const int days) -> Date&
{
    _day -= days;
    while (!day_valid(_year, _month, _day)) {
        --_month;
        if (_month < 1) {
            _year = 12;
            _month = 12;
        }
        _day += month_length(_year, _month);
    }
    return *this;
}


auto Date::operator -(const Date& other) -> int
{
    return distance(other);
}


auto Date::operator >(const Date& other) const -> bool
{
    return ((_year > other._year) || 
            ((_year == other._year) && ((_month > other._month) ||
                                       ((_month == other._month && (_day > other._day))))));
}

auto Date::operator >=(const Date& other) const -> bool
{
    return ((_year > other._year) || 
            ((_year == other._year) && ((_month > other._month) ||
                                       ((_month == other._month && 
                                            ((_day > other._day) || _day == other._day))))));
}

auto Date::operator <(const Date& other) const -> bool
{
    return ((_year < other._year) || 
            ((_year == other._year) && ((_month < other._month) ||
                                       ((_month == other._month && (_day < other._day))))));
}

auto Date::operator <=(const Date& other) const -> bool
{
    return ((_year < other._year) || 
            ((_year == other._year) && ((_month < other._month) ||
                                       ((_month == other._month && 
                                            ((_day < other._day) || _day == other._day))))));
}


auto Date::operator ==(const Date& other) const -> bool
{
    return ((_year == other._year) && (_month == other._month) && (_day == other._day));
}

auto Date::operator !=(const Date& other) const -> bool
{
    return ((_year != other._year) || (_month != other._month) || (_day != other._day));
}


}