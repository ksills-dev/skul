#include "sdf/sdf_node.hpp"

namespace 
{

const auto int_regex = std::regex("\\s*(-?)(\\d+)\\s*");
const auto float_regex = std::regex("\\s*(-?)(\\d+)(?:.(\\d+))?\\s*");
const auto bool_regex = std::regex("\\s*((?:[Tt][Rr][Uu][Ee])|(?:[Ff][Aa][Ll][Ss][Ee]))\\s*");

/**
 * @brief convert the provided character letter to it's digit equivalent.
 *
 * Does not check validity and will return 0 if an improper character is provided.
 */
auto char_to_digit(char letter) -> int 
{
    auto digit = 0;
    switch (letter) {
    case '0':
        digit = 0;
        break;
    case '1':
        digit = 1;
        break;
    case '2':
        digit = 2;
        break;
    case '3':
        digit = 3;
        break;
    case '4':
        digit = 4;
        break;
    case '5':
        digit = 5;
        break;
    case '6':
        digit = 6;
        break;
    case '7':
        digit = 7;
        break;
    case '8':
        digit = 8;
        break;
    case '9':
        digit = 9;
        break;
    default: break;
    }
    return digit;
} 

}

namespace sdf
{

SDF_Node::SDF_Node(std::string name, std::string value)
        : _name(name), _tag(""), 
          _kind(SDF_Node_Type::Value), _value(value)
{}

SDF_Node::SDF_Node(std::string name, std::string tag, std::string value)
        : _name(name), _tag(tag), 
          _kind(SDF_Node_Type::Value), _value(value)
{}

SDF_Node::SDF_Node(std::string name, std::string tag, SDF_Branch branch)
        : _name(name), _tag(tag),
          _kind(SDF_Node_Type::Branch), _branch(branch)
{}

SDF_Node::SDF_Node(std::string name, SDF_Branch branch)
        : _name(name), _tag(""),
          _kind(SDF_Node_Type::Branch), _branch(branch)
{}

auto SDF_Node::name() const -> std::string
{
    return _name;
}

auto SDF_Node::tag() const -> std::string
{
    return _tag;
}

auto SDF_Node::tag(std::string tag) -> void
{
    _tag = tag;
}

auto SDF_Node::has_tag() const -> bool
{
    return (_tag.length() > 0);
}

auto SDF_Node::kind() const -> SDF_Node_Type
{
    return _kind;
}

auto SDF_Node::value() const -> util::Maybe<std::string>
{
    if (_kind == SDF_Node_Type::Branch) return util::Maybe<std::string>();
    return util::Maybe<std::string>(_value);
}

auto SDF_Node::as_string() const -> util::Maybe<std::string>
{
    if (_kind == SDF_Node_Type::Branch) return util::Maybe<std::string>();
    return util::Maybe<std::string>(_value);
}

auto SDF_Node::as_int() const -> util::Maybe<int>
{
    if (_kind == SDF_Node_Type::Branch) return util::Maybe<int>(); 
    std::smatch match;
    if (!std::regex_match(_value, match, int_regex)) return util::Maybe<int>();

    auto value = 0;
    auto multiplier = 1;
    for (auto i = match[2].length() - 1; i >= 0; --i) {
        value += multiplier * char_to_digit(match.str(2)[i]);
        multiplier *= 10;
    }

    if (match[1] == "-") { 
        return util::Maybe<int>(true, value * -1); 
    }
    return util::Maybe<int>(true, value); 
}

auto SDF_Node::as_float() const -> util::Maybe<float>
{
    if (_kind == SDF_Node_Type::Branch) return util::Maybe<float>();
    std::smatch match; 
    if (!std::regex_match(_value, match, float_regex)) return util::Maybe<float>();

    // Integer
    float value = 0.00;
    float integer_multiplier = 1;
    for (auto i = match[2].length() - 1; i >= 0; --i) {
        value += integer_multiplier * char_to_digit(match.str(2)[i]);
        integer_multiplier *= 10;
    }

    // Decimal
    float decimal_multiplier = 1/10.0;
    for (auto i = match[3].length() - 1; i >= 0; --i) {
        value += decimal_multiplier * char_to_digit(match.str(3)[i]);
        decimal_multiplier /= 10.0;
    }

    if (match[1] == "-") { 
        return util::Maybe<float>(value * -1);
    }
    return util::Maybe<float>(value); 
}

auto SDF_Node::as_bool() const -> util::Maybe<bool>
{   
    if (_kind == SDF_Node_Type::Branch) return util::Maybe<bool>(); 
    std::smatch match;
    if (!std::regex_match(_value, match, bool_regex)) return util::Maybe<bool>();
    if (match.str(1)[0] == 'T' || match.str(1)[0] == 't') 
        { return util::Maybe<bool>(true); }
    else 
        { return util::Maybe<bool>(false); }
}

auto SDF_Node::branch() const -> util::Maybe<SDF_Branch>
{
    if (_kind == SDF_Node_Type::Value) return util::Maybe<SDF_Branch>();
    return util::Maybe<SDF_Branch>(_branch);
}

auto SDF_Node::contains(std::string name) const -> bool
{
    if (_kind == SDF_Node_Type::Value) return 0;
    
    for (auto const& node : _branch) {
        if (node._name == name) {
            return true;
        }
    }
    return false;
}

auto SDF_Node::count(std::string name) const -> int
{
    if (_kind == SDF_Node_Type::Value) return 0;

    int count = 0;
    for (auto const& node : _branch) {
        if (node._name == name) {

            ++count;
        }
    }

    return count;
}

}