#include "sdf/sdf_parser.hpp"

#include <fstream>
#include <regex>

#include <iostream>

namespace 
{

// This regex is a masterpiece, isn't it?
const auto node_regex = 
    std::regex("<(\\w+)(?:\\s*=\\s*(\\S+?)\\s*)?>\\s*([\\S\\s]*?)\\s*<\\/\\1>");
}

namespace sdf 
{

SDF_Parser::SDF_Parser() : _log(_log_file)
{
}

auto SDF_Parser::load_file(std::string filename) -> bool
{
    _log << util::Log_Level::Info;
    _log << "Loading " + filename;
    _log << "Opening (+r)...";
    std::fstream file_handle(filename);
    if (!file_handle.is_open()) {
        _log << util::Log_Level::Error << "Could not open file!";
        return false;
    }
    
    _log << "Reading contents...";
    file_handle.seekg(0, std::ios::end);
    size_t size = file_handle.tellg();
    file_handle.seekg(0, std::ios::beg);

    std::string input = std::string(size, ' ');
    file_handle.read(&input[0], size); // Please don't look at this, it's an abomination.
                                       // It doesn't "technically" stay within defined 
                                       // behavior, but it works with the compiler we use!
    _log << "Closing handle...";
    file_handle.close();

    // Prune whitespace
    _log << "Pruning whitespace...";
    for (size_t i = 0; i < input.length(); ++i) {
        auto letter = input[i];
        switch (letter) {
        case ' ':
        case '\t':
        case '\n':
        case '\v':
        case '\f':
        case '\r':
            break;
        default:
            continue;
            break;
        }
        input.erase(input.begin() + i);
        --i;
    }

    _log << "Generating Node Graph...";
    _tree = construct_tree(input);
    _log << "Load successful!";
    return true;
}

auto SDF_Parser::save_file(std::string filename) -> bool
{
    _log << util::Log_Level::Info;
    _log << "Opening (+rw)...";
    std::fstream file_handle(filename, std::fstream::out);
    if (!file_handle.is_open()) {
        _log << util::Log_Level::Error << "Could not open file!";
        return false;
    }

    _log << "Writing Node Graph...";
    for (const auto& node : _tree) {
        file_handle << construct_node_output(node) << "\n\n";
    }

    file_handle.flush();
    file_handle.close();
    _log << "Save successful!";
    return true;
}

auto SDF_Parser::set_table(const SDF_Branch table) -> void
{
    _tree = table;
}

auto SDF_Parser::table() const -> SDF_Branch
{
    return _tree;
}

auto SDF_Parser::operator << (const SDF_Branch table) -> SDF_Parser&
{
    _tree.reserve(_tree.size() + table.size());
    for (const auto& element : table) {
        _tree.push_back(element);
    }
    return *this;
} 

auto SDF_Parser::construct_tree(std::string input) -> SDF_Branch
{
    SDF_Branch result{};
    std::smatch match;

    while (std::regex_search(input, match, node_regex)) {
        auto name = match.str(1);
        auto tag = match.str(2);
        auto value = match.str(3);

        result.push_back(construct_node(name, tag, value));

        input = match.suffix();
    }
    return result;
}

auto SDF_Parser::construct_node(std::string name, std::string tag, std::string value) -> SDF_Node
{
    std::smatch match;

    if (std::regex_search(value, match, node_regex)) {
        _log << "[Tree]";
        _log << "Name: " + name;
        _log << "Tag: " + tag;
        return SDF_Node{name, tag, construct_tree(value)};
    } else {
        _log << "[Node]";
        _log << "Name: " + name;
        _log << "Tag: " + tag;
        _log << "Value: " + value;
        return SDF_Node{name, tag, value};
    }
}

auto SDF_Parser::construct_node_output(SDF_Node node, int depth) -> std::string
{
    std::string output;
    output += "<" + node.name();
    if (node.tag().length() > 0) {
        output += " = ";
        output += node.tag();
    } 
    output += ">";
    if (node.kind() == SDF_Node_Type::Value) {
        output += " " + node.value().access() + " ";
    } else {
        for (auto const& child : node.branch().access()) {
            output += "\n";
            for (auto i = 0; i < depth; i++) {
                output += "\t";
            }
            output += construct_node_output(child, depth + 1);
        }
        output += "\n";
        for (auto i = 1; i < depth; i++) {
            output += "\t";
        }
    }

    output += "</" + node.name() + ">";
    return output;
}

}