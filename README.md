# **S**chool **Kul**lections (Skul)
The final project for USF's OOP Design course, a demonstration of my ability to build
non-trivial object heirarchies and make use of them in a basic system. The system includes:

* No external depedencies, leading to:
  * A custom fileformat for records and a R/W Parser for said format.
  * A simple to-file logger.
  * A simple unit-testing framework (currently unused).
  * Some utilities such as `Maybe` types and strict aliasing templated types.
* Reading in a database state from file and writing out to the same files.
* An Object model for faculty, students, courses, departments, and research in a
  logically consistent way.
* A rudimentary in-source AFR (Access-Request-Filter) model for database querying.

A (large) UML diagram and a pdf with some brief documentation are provided in the `docs`
folder. The design documents also go over known constraints / flaws in the design.

## Compiling
A complete Makefile is provided, so all that should be necessary is a pass through
`make`. If one wishes to use a compiler other than clang++, simply change the
`CXX := clang++` line in the Makefile.

Previous errors in the compilation process have been resolved and the program
should work with any C++11 compliant compiler and with only a single `make` pass.

The program is compiled utilizing `-Wall -Wextra -pedantic`, so it will complain about
all reasonable warnings and errors. As far as I can tell, I've resolved all of
these and it should compile silent and clean!

## Running
Just run the produced executable under `build/skul-systest`.

### Input
All input is received from the `Database` directory at the working directory of
execution. Specifically it will access `teachers.txt`, `courses.txt`,
`students.txt`, `research.txt`, and `departments.txt`. These files will need +rw
access. These files are all read in my standard SDF format (see below for details).

For ease of use an example database has been provided under `example-db`. Simply
copy this to the desired working directory and rename to `Database`.

### Output
All output is targetted toward the `Database` directory at the working directory.
Specifically it will write to `teachers.txt`, `courses.txt`, `students.txt`,
`research.txt`, and `departments.txt`. These files will need +rw access. These files are
all written in my standard SDF format (see below for details).

Our logger also requires successful write access to the `systest.log`, `parser.log`, and
`Database/log/admin.log` from the working directory.

To check for any issues with conducted operations, check those log files for posts
marked with "WARN" or "ERROR". Though the current state of the program has a
non-destructive bug somewhere, causing a couple of warnings for skipped subnodes.
However, all nodes are read and written successfully and this has no influence on
functionality, so it has not been fixed for time constraints.

## Debugging
If you ever find the need to debug the system, you will find three *extensive* log files
that will help you.

The first is `systest.log`, which details the logging data for the
overlying mainfile tests. The second is `parser.log` which explains, in (far too) explicit
detail, what the parser is doing. And finally you'll find `Database/log/admin.log`, which
details all mutating operations conducted within the database, as well as very detailed
explanations as to what is happening during parsing routines.

It will be easiest to Ctrl+F search for "WARN" or "ERROR" in such scenarios.

## Simple Database Format (SDF)
This is an incredibly simple data-oriented file type, designed to be parsable via a simple
regex: `<(\w+)(?:\s*=\s*(\S+?)\s*)?>\s*([\S\s]*?)\s*<\/\1>`.

Each node is delimited by angle brackets, with the node name in between. Optionally,
one may place an equal sign followed by a descriptive tag (great for things like keys or
separating like entries). The contents of a node may either be a value or a subnode.
The filetype is also able to check if a node's contents are an integer, decimal number,
or boolean.

This application has some specific handling of it's SDF files. Most notable, all
whitespace is stripped from the file at the beginning of parsing (just to minimize the
points of failure). So any Department, Course, or Research Project names will need to be
delimited not by spaces, but by underscores.

And second, ID lists are simply comma-delimited IDs inside a node. A trailing comma
is completely acceptable and, in fact, is how the file is written by the program.