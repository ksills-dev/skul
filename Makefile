# Makefile could use some work to make it prettier
# and easier to extend. But I got it working 
# and that's all I needed.

# Program output names
APPNAME_SYSTEST := skul-systest

# Put all the files here.
# If a new namespace (and therefore subdir) is added, 
# you'll need to designate the dir name, define a new
# object list, and add any necessary dependencies.
COMMON_FILES  := 
SDF_FILES     := sdf_parser.cpp sdf_node.cpp
ADMIN_FILES   := admin_db.cpp department.cpp course_definition.cpp course_section.cpp \
                  research_project.cpp teacher.cpp student.cpp registered_person.cpp \
                  person.cpp date.cpp assignment.cpp 
TESTER_FILES  := unit-tester.cpp
UTIL_FILES    := logger.cpp
SYSTEST_MAIN  := systest.cpp

SDF_DIR    := sdf
ADMIN_DIR  := admin
TESTER_DIR := tester
UTIL_DIR   := util

COMMON_OBJ  := ${COMMON_FILES:.cpp=.o}
SDF_OBJ     := ${SDF_FILES:.cpp=.o}
ADMIN_OBJ   := ${ADMIN_FILES:.cpp=.o}
TEST_OBJ    := ${TESTER_FILES:.cpp=.o}
UTIL_OBJ    := ${UTIL_FILES:.cpp=.o}
SYSTEST_OBJ := ${SYSTEST_MAIN:.cpp=.o}

INCLUDE_DIRS := include
SOURCE_DIR   := src 
BUILD_DIR    := build
OBJECT_DIR   := $(BUILD_DIR)/objects

CXX      := clang++
CXXFLAGS += -Wall -Wextra -Werror -pedantic -pedantic-errors \
            -I$(INCLUDE_DIRS) -std=c++11 -g

RM       := rm
RM_FLAGS := -r # Danger

MKDIR       := mkdir
MKDIR_FLAGS := -p

# Search path for sources and objects.
VPATH := $(SOURCE_DIR) $(BUILD_DIR) $(OBJECT_DIR)

.PHONY: all clean common admin_dir sdf_dir util_dir tester_dir admin sdf tester systest

default: systest

all: systest

common: #$(COMMON_OBJ)
	@if ! [ -e $(BUILD_DIR) ] ; then \
		echo "Creating build directory..." ; \
		$(MKDIR) $(MKDIR_FLAGS) $(BUILD_DIR) ; \
	fi
	@if ! [ -e $(OBJECT_DIR) ] ; then \
		echo "Creating object directory..." ; \
		$(MKDIR) $(MKDIR_FLAGS) $(OBJECT_DIR) ; \
	fi

admin_dir:
	@if ! [ -e $(OBJECT_DIR)/$(ADMIN_DIR) ] ; then \
		echo "Creating object/admin directory..." ; \
		$(MKDIR) $(MKDIR_FLAGS) $(OBJECT_DIR)/$(ADMIN_DIR) ; \
	fi

sdf_dir:
	@if ! [ -e $(OBJECT_DIR)/$(SDF_DIR) ] ; then \
		echo "Creating object/sdf directory..." ; \
		$(MKDIR) $(MKDIR_FLAGS) $(OBJECT_DIR)/$(SDF_DIR) ; \
	fi

util_dir:
	@if ! [ -e $(OBJECT_DIR)/$(UTIL_DIR) ] ; then \
		echo "Creating object/util directory..." ; \
		$(MKDIR) $(MKDIR_FLAGS) $(OBJECT_DIR)/$(UTIL_DIR) ; \
	fi

tester_dir:
	@if ! [ -e $(OBJECT_DIR)/$(TESTER_DIR) ] ; then \
		echo "Creating object/tester directory..." ; \
		$(MKDIR) $(MKDIR_FLAGS) $(OBJECT_DIR)/$(TESTER_DIR) ; \
	fi

admin: common admin_dir $(addprefix $(ADMIN_DIR)/, $(ADMIN_OBJ))

sdf: common sdf_dir $(addprefix $(SDF_DIR)/, $(SDF_OBJ))

util: common util_dir $(addprefix $(UTIL_DIR)/, $(UTIL_OBJ))

tester: common tester_dir $(addprefix $(TESTER_DIR)/, $(TESTER_OBJ))

systest: common sdf util admin tester $(addprefix $(BUILD_DIR)/, $(APPNAME_SYSTEST))

$(BUILD_DIR)/$(APPNAME_SYSTEST): $(SYSTEST_OBJ)
	$(CXX) $(addprefix $(OBJECT_DIR)/$(ADMIN_DIR)/, $(ADMIN_OBJ)) \
				 $(addprefix $(OBJECT_DIR)/$(SDF_DIR)/, $(SDF_OBJ)) \
				 $(addprefix $(OBJECT_DIR)/$(UTIL_DIR)/, $(UTIL_OBJ)) \
				 $(addprefix $(OBJECT_DIR)/$(TESTER_DIR)/, $(TESTER_OBJ)) \
				 $(addprefix $(OBJECT_DIR)/, $(SYSTEST_OBJ)) -o $@

# Common object compilation rule.
%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $(OBJECT_DIR)/$@

clean:
	$(RM) $(RM_FLAGS) $(OBJECT_DIR) $(BUILD_DIR)